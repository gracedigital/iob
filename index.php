<?php 
	
	$_SERVER['DOCUMENT_ROOT'] = getcwd();

	/*** error reporting ***/
	error_reporting(-1); // 0 = off
	
	ini_set('track_errors','true');
	ini_set("session.use_cookies", 1);
	ini_set("session.use_only_cookies", 1);
	ini_set("session.use_trans_sid", 1);
	
	/*** include the init.php file ***/
	include 'includes/bootstrap.php';

	/*** session ***/
	if(!isset($_SESSION)) session_start();

	/*** load the router ***/
	$registry->router = new router($registry);

	/*** set the controller path ***/
	$registry->router->setPath(CONTROLLERS);

	/*** load up the template ***/
	$registry->template = new template($registry);

	/*** load the controller ***/
	$registry->router->route();

?>

