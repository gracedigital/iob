var Folder = function () {

	// private variables
	var row = {};
	var note = {};
	var title = {};
	var action = {};
	var list_id = {};
	var message = {};
	var callback = {};

	// private constants
	var obsolete, email;

	// private methods
	function parameterize(route, input) {
		return $.param({route:route, params:input});		    
	}

	// public methods
 	return {

        //main function to initiate template pages
        reset: function () {
			row = {};
			note = {};
			title = {};
			action = {};
			list_id = {};
			message = {};
			callback = {};
			this.obsolete = false;
			this.email = 'ar@reciva.com';			
        },

        print: function () {
	    	console.log('FOLDER_PROPERTIES:');        		
	    	console.log("NOTE: " + this.note);
	    	console.log("EMAIL: " + this.email);
	    	console.log("TITLE: " + this.title);
	    	console.log("LIST_ID: " + this.list_id);
        },

       	serialize: function () {
	       	var output =  '{';
	       		output += '"note":"' + this.note + '", ';
	       		output += '"title":"' + this.title + '", ';
	       		output += '"email":"' + this.email + '"';
	       		output += '"list_id":"' + this.list_id + '", ';
	       		output += '"obsolete":"' + this.obsolete + '"';
	       		output += '}';
	       	return output;
       	},

		parameterizeFor: function (action) {
			var result;
			switch (action) {
				case 'channels':
					result = parameterize('channel/channels', { list_id: this.list_id, filter: 2, expand: true });
					break;				
				case 'delete':
					result = parameterize('folder/delete', { list_id: this.list_id });
					break;
				case 'update':
					result = parameterize('folder/update', { list_id: this.list_id, title: this.title, note: this.note, email: this.email });
					break;
			}
			return result;
		}, 

        prepareFor: function (action, row, data, inputs) {

        	this.reset();
            this.row = row;

            switch (action) {
            	case 'delete':
		            this.list_id = data[2].trim();
		            this.title = data[3].trim();
		            this.message = 'Are you sure you want to delete the lineup "<b>' + this.title + '</b>"?';
		            this.callback = 'javascript:deleteFolder()';                  
	               	break;
	            case 'update':
		            this.title = inputs[0].value.trim();
		            this.note = inputs[1].value.trim();
		            this.list_id = data[2].trim();            	
	            	break;
            }
        },

       	delete: function() {
		 	var params = this.parameterizeFor('delete');
		    ajaxRequest(params, function(response) {
		        return JSON.parse(response);
		    });        		
       	},

		update: function() {
		 	var params = this.parameterizeFor('update');
		    ajaxRequest(params, function(response) {
		        return JSON.parse(response);
		    }); 
        }        

	};        

}();