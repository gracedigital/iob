var Schedule = function () {

	var id = 0;
	var note = '';
	var html = '';
	var dirty = 0;
	var events = {};
	var groupId = 0;
	var groupName = '';
	var description = '';
	var updates = [];
	var additions = [];
	var deletions = [];
	var obsolete = false;
	var interval_type = 'W';

	// if event already exists don't add it again
	var inArray = function(needle, haystack) {
	    var length = haystack.length;
	    for(var i = 0; i < length; i++) {
	    	// console.log(haystack[i].event_id + '==' + needle);
	        if(haystack[i].event_id == needle) return true;
	    }
	    return false;
	}

	var inAdditions = function(dow, event) {
		var exists = false;
		additions[dow].forEach(function(entry) {
			if(_.isEqual(event, entry)) {
				exists = true;
				return;
			}
	    });   		
		return exists;
	}

 	return {

        //main function to initiate template pages
        clear: function () {
        	html = '';
			dirty = 0;
			events = {};
			updates = [];
			additions = [];
			deletions = [];
        },

        deleteEvent: function (id) {
            deletions.push(id);
            // console.log('delete these;')
            // console.log(deletions)
        }, 

        addEvent: function (dow, event) {
        	if(!additions[dow]) {
        		additions[dow] = [];
        	}

        	if(!inAdditions(dow, event)) {
        		additions[dow].push(event);
        	}
        },

        updateEvent: function (dow, event) {
        	if(!updates[dow]) {
        		updates[dow] = [];
        	}

        	if(!inArray(event.event_id, updates[dow])) {
        		updates[dow].push(event);
        	} 
        },

        print: function () {
	    	console.log('SCHEDULE_PROPERTIES:');        		
	    	console.log("ID: " + Schedule['id']);
	    	console.log("DESCRIPTION: " + Schedule['description']);        	
	    	console.log("OBSOLETE: " + obsolete);
	    	console.log("INTERVAL_TYPE: " + interval_type);
	    	console.log('');
	    	console.log('SCHEDULE_EVENTS:');   
	    	console.log('UPDATES:'); 
	    	console.log(updates);
	    	console.log('ADDITIONS:'); 	
	    	console.log(additions);
			console.log('DELETIONS:');        		
	    	console.log(deletions);
        },

       	serialize: function () {
	       	var output =  '{';
	       		output += '"id":"' + Schedule['id'] + '", ';
	       		output += '"radiogroup_id":"' + Schedule['groupId'] + '", ';
	       		output += '"note":"' + Schedule['note'] + '", ';
	       		output += '"description":"' + Schedule['description'] + '", ';
	       		output += '"obsolete":"' + obsolete + '", ';
	       		output += '"interval_type":"' + interval_type + '", ';
	       		output += '"updates":' + JSON.stringify(updates) + ', ';
	       		output += '"additions":' + JSON.stringify(additions) + ', ';
	       		output += '"deletions":' + JSON.stringify(deletions);
	       		output += '}';
	       	return output;
       	}
	};        

}();