var Channel = function () {

	// private variables
	var row = {};
	var note = {};
	var title = {};
	var action = {};
	var list_id = {};
	var message = {};
	var callback = {};
	var list_entry_id = {};

	// private constants
    var parent_id, folder, obsolete;

	// private methods
	function parameterize(route, input) {
		return $.param({route:route, params:input});		    
	}

	// public methods
 	return {

        reset: function () {
			row = {};
			note = {};
			title = {};
			action = {};
			list_id = {};
			message = {};
			callback = {};
			list_entry_id = {};
			this.parent_id = 0;
		    this.folder = false;
			this.obsolete = false;
        },

        print: function () {
	    	console.log('CHANNEL_PROPERTIES:');        		
	    	console.log("TITLE: " + this.title);
	    	console.log("LIST_ID: " + this.list_id);
	    	console.log("OBSOLETE: " + this.obsolete);
	    	console.log("PARENT_ID: " + this.parent_id);
	    	console.log("LIST_ENTRY_ID: " + this.list_entry_id);
        },

       	serialize: function () {
	       	var output =  '{';
	       		output += '"title":"' + this.title + '"';
	       		output += '"folder":"' + this.folder + '"';
	       		output += '"list_id":"' + this.list_id + '", ';
	       		output += '"obsolete":"' + this.obsolete + '"';
	       		output += '"parent_id":"' + this.parent_id + '"';
	       		output += '"list_entry_id":"' + this.list_entry_id + '", ';
	       		output += '}';
	       	return output;
       	},
       	
       	prepareFor: function (action, row, data, inputs) {

            this.reset();
            this.row = row;
            console.log('prepareFor: ' + action + ' @ ' + moment().format("hh:mm:ss a"));
            console.log(this);
            
            switch (action) {
            	case 'delete':
            	console.log('prepare delete');
	                this.list_id = data[0].trim();
	                this.list_entry_id = data[1].trim();
	                this.title = data[2].trim();
	                this.callback = 'javascript:deleteChannel()';
	                this.message = 'Are you sure you want to delete the channel "<b>' + this.title + '</b>"?';	
	               	break;
	            case 'update':
		            this.list_id = data[0].trim();
		            this.list_entry_id = data[1].trim();
		            this.title = inputs[0].value.trim();
		            this.stream_url = inputs[1].value.trim();	            	
	            	break;
            }
       	},

		parameterizeFor: function(action) {
			var result;
			switch (action) {
				case 'unlink':
					result = parameterize('channel/unlink', { list_id: this.list_id, list_entry_id: this.list_entry_id });
					break;
				case 'update':
					result = parameterize('channel/update', { list_id: this.list_id, list_entry_id: this.list_entry_id, parent_id: this.parent_id, title: this.title, stream_url: this.stream_url, folder: this.folder });
					break;
			}
			return result;
		}
	};        

}();