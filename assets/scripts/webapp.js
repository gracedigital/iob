var daysofweek = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];

function addMinutes(date, minutes) {
    return new Date(date.getTime() + minutes*60000);
}

function logMethod(name, action) {
  console.log(name + ' ' + action +  ' @ ' + moment().format("hh:mm:ss a"));
}

function clearLoading() {
    $('#loading-icon').hide();
    $('#submit-icon').show();
}

function clearStatus() {
    setTimeout(function(){
        $('#status-msg').text('');
        $('#status-bar').slideUp('slow');
    }, 4000);
}

function setStatus(message) {
    var at = moment().format("hh:mm:ss a"); 
    $('#status-bar').slideDown();
    $('#status-msg').text(at + ' - ' + message + '...');
}

function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

function convertToMilitaryTime(standardTime) {
    // console.log('convertToMilitaryTime: ' + standardTime)
    var ampm = standardTime.slice(-2);
    var parts = standardTime.slice(0, -2).split(":");
    var hours = parts[0];
    var minutes = parts[1];
    var militaryHours;

    if( ampm == "AM" ) {
        militaryHours = hours;
        // check for special case: midnight
        if( militaryHours == "12" ) { militaryHours = "00"; }
    } else {
        if( ampm == "PM" || ampm == "p.m." ) {
            // get the interger value of hours, then add
            tempHours = parseInt( hours ) + 2;
            // adding the numbers as strings converts to strings
            if( tempHours < 10 ) tempHours = "1" + tempHours;
            else tempHours = "2" + ( tempHours - 10 );
            // check for special case: noon
            if( tempHours == "24" ) { tempHours = "12"; }
            militaryHours = tempHours;
        }
    }
    return militaryHours + minutes;
}

function isEmpty(obj) {
    if (typeof obj == 'undefined' || obj === null || obj === '' || obj == 'undefined' || obj == '[]') return true;
    if (typeof obj == 'number' && isNaN(obj)) return true;
    if (obj instanceof Date && isNaN(Number(obj))) return true;
    return false;
}

function stopEvent(e){
    if(!e){ /* IE7, IE8, Chrome, Safari */ 
        e = window.event; 
    }
    if(e.preventDefault) { /* Chrome, Safari, Firefox */ 
        e.preventDefault(); 
    } 
    e.returnValue = false; /* IE7, IE8 */
}

// Load Main Content Div
var loadContent = function (route) {
  var container = $('#content'); 
  // add history for ajax
  History.pushState({rt:route}, null, "?rt="+route);  
  // get the content
  var params = $.param({ route: route, params: { obsolete:false } });
  $.ajax({data: params}).done(function (content) {
      container.empty().append(content);
      $.event.trigger({type: "contentLoaded"});
  });
  return false;           
}

// Load DataTable 
var loadTable = function (data) {
  oTable = $('#data_table').dataTable(
  {
    "aaSorting": [[0, "desc"]],
    "bDestroy": true,
    "bFilter"  : true,
    "aaData": data.aaData,      
    "aoColumnDefs": [{
      "bSortable": false,
      "aTargets": [0],
      "mRender": function (result, type, full) {
        return '<input type=\"checkbox\" class=\"checkboxes\" value=\"\" />';
      }
    }],
    "fnInitComplete": function(oSettings, json) {
      hideSpinner();
    }           
  });
}

// Hide Status Messages
var clearError = function () {
  $('#errorMessage').html('').parent().hide();
}

var clearSuccess = function () {
  $('#successMessage').html('').parent().hide();
}        

var clearWarning = function () {
  $('#warningMessage').html('').parent().hide();
}

var clearMessages = function () {
  clearError();
  clearSuccess();
  clearWarning();
}

// Show Status Messages
var showSuccess = function (message) {
  $('#successMessage').html('<strong>Success</strong>: ' + message).parent().show().pulsate({repeat: 2});
  // setTimeout(function() {
  //   clearSuccess();
  // }, 4000);
}

var showWarning = function (message) {
  $('#warningMessage').html('<strong>Warning</strong>: ' + message).parent().show().pulsate({repeat: 2});
}

var showError = function (message) {
  $('#errorMessage').html('<strong>Error</strong>: ' + message).parent().show(); 
}

var showSpinner = function () {
  $('#loading').spin('small','#fff');
}

var hideSpinner = function () {
   $('#loading').spin(false);
}


// Add toJSON method to form elements: Serialize form into JSON
jQuery.fn.toJSON = function () {
  
   var form = $(this) // It's your element
   var values = form.serialize(),
   attributes = {};

   values.replace(/([^&]+)=([^&]*)/g, function (match, name, value) {
       attributes[name] = value;
   });
   return attributes;
};

$(function() {
    // AJAX Setup 
    $.ajaxSetup({
        url: 'application/proxy.php',
        timeout: 30000
    });
});

$.widget( "ui.pcntspinner", $.ui.spinner, {
    _format: function( value ) { return value + '%'; },
    _parse: function(value) { return parseFloat(value); }
});