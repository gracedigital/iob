// Reset confirmation input on dialog close
$('body').on('hidden', '.modal', function () {
    $('input', '.modal').val('');
	$('select', '.modal').select2('data', null);
	$('.btn.green', '.modal').prop("disabled", "true");	    
});    	

// Enable 'OK' button in the move modal dialog
$('body').on('input paste drop', '#input-confirm-move', function() {
	$('.btn.green').prop("disabled", $(this).val().toUpperCase() != 'MOVE');
});

// Enable 'OK' button in the activate modal dialog
$('body').on('input paste drop', '#input-confirm-activate', function() {
	$('.btn.green').prop("disabled", $(this).val().search(/activate/i) < 0);
});

// Enable 'OK' button in the delete modal dialog
$('body').on('input paste drop', '#input-confirm-remove', function() {
	$('.btn.green').prop("disabled", $(this).val().toUpperCase() != 'DELETE');
});

// Enable 'OK' button in the delete modal dialog
$('body').on('input paste drop', '#input-confirm-unlink', function() {
	$('.btn.green').prop("disabled", $(this).val().toUpperCase() != 'UNLINK');
});

// Enable 'OK' button in the remove modal dialog
$('body').on('change', '#lst-entity-add', function() {
	$('.btn.green').prop("disabled", $('#lst-entity-add :selected').val().length == 0);
});

// Enable 'OK' button in the unregister modal dialog
$('body').on('input paste drop', '#input-confirm-code', function() {
	$('.btn.green').prop("disabled", $(this).val().length < 5);
});

// Intalize select2 options list
$("body").on('shown.bs.modal', "#modal-add", function () {
    var placeholder = "Choose one...";
    $("select").select2({
        placeholder: placeholder,
        allowClear: true,
        width: "355"
    });
});

// Set focus in remove modal
$("body").on('shown.bs.modal', "#modal-remove", function () {
	$('#input-confirm-remove').prop('autocomplete','on').focus();
});

// Handle enter key for remove dialog
$('body').on('keypress', '#modal-remove', function(e) {
    if(e.which == 13 && $('.btn.green').is(":enabled")) {
        $('#btn-entity-remove', $(this)).click();
    }
});

// Handle enter key for unlink dialog
$('body').on('keypress', '#modal-unlink', function(e) {
    if(e.which == 13 && $('.btn.green').is(":enabled")) {
        $('#btn-entity-unlink', $(this)).click();
    }
});

// Show add entity dialog
var showAddModal = function(data, message, callback) {
	$('#lst-entity-add', '.modal').empty().append("<option value=''></option>" + data);
	$('#modal-message', '.modal').empty().append(message);
	$('#btn-entity-add', '.modal').attr('onclick', callback);
    $('#modal-add').modal('show');
}

var hideAddModal = function() {
	$('#modal-add').modal('hide');
}

// Show remove entity dialog
var showRemoveModal = function(entity) {
	$('#btn-entity-remove', '.modal').attr('onclick', entity.callback);
    $('#modal-message', '.modal').empty().append(entity.message);
	$('#modal-remove').modal('show');
}

var hideRemoveModal = function() {
	$('#modal-remove').modal('hide');
}

// Show unlink entity dialog
var showUnlinkModal = function(entity) {
	$('#btn-entity-unlink', '.modal').attr('onclick', entity.callback);
    $('#modal-message', '.modal').empty().append(entity.message);
	$('#modal-unlink').modal('show');
}

var hideUnlinkModal = function() {
	$('#modal-unlink').modal('hide');
}

// Show activate entity dialog
var showActivateModal = function(entity) {
	$('#btn-entity-activate', '.modal').attr('onclick', entity.callback);
	$('#activate-action', '.modal').empty().append(entity.action);
    $('#activate-message', '.modal').empty().append(entity.message);
	$('#modal-activate').modal('show');
}

var hideActivateModal = function() {
	$('#modal-activate').modal('hide');
}

// Show remove entity dialog
var showUnregisterModal = function(action, description) {
    $('#entity-description', '.modal').empty().append(description);
	$('#modal-unregister').modal('show');
}

var hideUnregisterModal = function() {
	$('#modal-unregister').modal('hide');
}

// Show move entity dialog
var showMoveModal = function(entity) {
	$('#btn-entity-move', '.modal').attr('onclick', entity.callback);
    $('#move-message', '.modal').empty().append(entity.message);
	$('#modal-move').modal('show');
}

var hideMoveModal = function() {
	$('#modal-move').modal('hide');
}