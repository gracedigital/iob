var oEntity = {};

$(document).ready(function() { 

	// Fix to be able to hide page alert
	$('button.close').on('mousedown', function(e) {
		stopEvent(e);
	  	$(this).parent().hide();
	  	return false;
	});

	$('#lst-folders').on('change', function(e) {
		stopEvent(e);
		$('#btn-submit').removeAttr('disabled');
		return false;
	});
	
	$('#lst-folders').select2({
		width: 250,
        allowClear: true
	});

	$('.select2-container').click();
	$('.select2-drop').click();

	$('#lst-groups').select2({
		width: 250
	});

	$('#form-provision').on('keypress', function(e) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			$('#btn-submit').mousedown();		
			stopEvent(e);
			return false;
		}
	});

	$('#btn-submit').on('mousedown', function(e) {
		stopEvent(e);
		provisionRadio();
		return false;
	});

	$('#btn-cancel').on('mousedown', function(e) {
		clearMessages();
		resetForm();
		return false;
	});

	var provisionRadio = function () {

	    clearMessages();
   
	    if(!isValidForm()) {
	    	return false;
	    }

		var input = {};
		input.obsolete = false;
		input.reg_code = $('#reg_code').val().trim();
		input.name = $('#name').val().trim();
		input.list_id = $('#lst-folders').select2('val');
		input.radiogroup_id = $('#lst-groups').select2('data').id;

		var params = getParameters('radio/provision', input);
		
      	ajaxRequest(params, function (response) {
			var result = JSON.parse(response);
			if(result.error) {  
				showError(result.error.message);
			} else {
				showSuccess('The radio "<b>' + result.serial + '</b>" was successfully provisioned');
				resetForm();
			}
		});

	    return false;
	}

	var resetForm = function() {
		$('form').find('input[type=text], textarea').val('');
		$('form').find('select').select2('data', null);
	}

    // Validate
    var isValidForm = function() {
        try {
			if(isValidCode() && isValidName() && isValidFolder() && isValidGroup()) {
			  setStatus('The form is valid.');
			}
        } catch(error) {
            setStatus('The form is invalid!');
            showError(error);
            clearLoading();
            return false;            
        }
        return true;
    }  	

    var isValidCode = function() {
        oEntity.element = $('#reg_code');
        if(isEmpty(oEntity.element.val())) {
            oEntity.element.addClass('error-border').focus();
            throw 'You need to enter the registration key from the radio!';
        }
        oEntity.element.removeClass('error-border');
        return true;
    }  

    var isValidName = function() {
        oEntity.element = $('#name');
        if(isEmpty(oEntity.element.val())) {
            oEntity.element.addClass('error-border').focus();
            throw 'You need to enter a name for this radio!';
        }
        oEntity.element.removeClass('error-border');
        return true;
    }  

    var isValidFolder = function() {
        oEntity.element = $('#lst-folders');
        if(isEmpty(oEntity.element.select2('val') )) {
            oEntity.element.addClass('error-border').focus();
            throw 'You need to assign this radio to a lineup!';
        }
        oEntity.element.removeClass('error-border');
        return true;
    }  

    var isValidGroup = function() {
        oEntity.element = $('#lst-groups');
        if(isEmpty(oEntity.element.select2('data').text)) {
            oEntity.element.addClass('error-border').focus();
            throw 'You need to assign this radio to a group!';
        }
        oEntity.element.removeClass('error-border');
        return true;
    }

});