var oOuterTable = null;

$(document).ready(function() {

    OuterTable.init();
	clearMessages();

	// Hide page alert div
	$('button.close').on('click', function(e) {
	  $(this).parent().hide();
	  return false;
	});                  

    // Initialize the outer table
    oOuterTable = $('#data_table').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
        "sDom": '<"headwrap"lf<"clear_search">>rtip',
        "fnDrawCallback": function( oSettings ) {
            if($('.dataTables_filter input').val().length >0) {
                $('.clear_search').show();
                $('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
            }
        },
        "aoColumns": [ 
            /* Counter */   { "sWidth": "3.5%", "bSortable": false },
            /* Details */   { "sWidth": "07%", "bSortable": false },
            /* ID */        { "sWidth": "07%", "bSortable": true },
            /* Name */      { "sWidth": "30%", "bSortable": true },
            /* Desc */      { "sWidth": "*",   "bSortable": true },
            /* Edit */      { "sWidth": "05%", "bSortable": false },
            /* Delete */    { "sWidth": "03%", "bSortable": false },
            /* Empty */     { "sWidth": "01%", "bSortable": false }
        ]               
    });   


    // DataTables filter clear
    $('body').on('mousedown', '.clear_search', function(e) {

        stopEvent(e);
        $('.dataTables_filter input').val('');
        oOuterTable.fnFilterClear();
        $('.clear_search').hide();
        $('.dataTables_filter input').css('background','none');

        return false;
    });

    $('.dataTables_filter input').on('keyup', function(e) {

        stopEvent(e);

        if($(this).val().length == 0){
            $('.clear_search').hide();
            $('.dataTables_filter input').css('background','none');
        } else {
            $('.clear_search').show();
            $('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
        }

        return false;
    });



    // Load inner table
    var anOpen = [];
    var link, icon, nDetailsRow, oldRow, nTr;

    $('#data_table').on('mousedown', '.expand', function (e) {

        stopEvent(e);
        clearMessages();

        oldRow = nTr;
        oldLink = link;
        oldIcon = icon;

        link = $(this);
        icon = $(this).prev();  
        nDetailsRow = null;

        // COLLAPSE THE OLD ROW //
        if(oldRow) {    
            $('div.innerDetails',  oldRow[0]).slideUp(function () {
                oldIcon.removeClass('icon-spinner icon-mirrored icon-spin').removeClass('icon-minus');
                oldIcon.addClass('icon-plus'); 
                oldLink.text(' Expand');
                oOuterTable.fnClose(oldRow);
                anOpen.shift();
                $('.delete, .edit', '#data_table').show();
            }); 
        }

        nTr = $(this).closest('tr')[0];
        var i = $.inArray(nTr, anOpen);
        var aData = oOuterTable.fnGetData(nTr);

        Group.radiogroup_id = aData[2].trim();
        Group.description = aData[3].trim();

        // console.log('This is the selected group object:');
        // console.log(Group);

        // EXPAND THE NEW ROW //
        if(nTr !== oldRow || i === -1) {
            icon.removeClass('icon-plus');
            icon.addClass('icon-spinner icon-mirrored icon-spin'); 

            var params = Group.parameterizeFor('schedules');
            ajaxRequest(params, function(response) {

                nDetailsRow = oOuterTable.fnOpen(nTr, response, 'details');
                oInnerTable = $('#data_table_inner').dataTable({
                    "bDestroy": true,
                    "sDom": '',
                    "aoColumns": [ 
                        /* ScheduleID */    { "sWidth": "5%"},
                        /* Description */   { "sWidth": "*" }
                    ]                                   
                });     
                
                // var jqTds = $('>td', nRow);
                $('.delete, .edit', '#data_table').hide();
                $('div.innerDetails', nDetailsRow).slideDown(); 
                icon.removeClass('icon-spinner icon-mirrored icon-spin');
                icon.addClass('icon-minus');                        
                link.text(' Collapse');
                anOpen.push(nTr);
            });

        } else {
            $('div.innerDetails', $(oldRow).next()[0]).slideUp( function () {
                anOpen.shift();
                oOuterTable.fnClose(nTr);
            });
        }

    });


    // Select2 the datatable 'records per page' select element
    $('div.dataTables_length select').select2({width:60});                  


    /*************************
        LINK SCHEDULE EVENT
    **************************/
    $('#data_table').on('mousedown', '.link-schedule', function() {
        var params = getParameters('group/unassociated', { radiogroup_id: Group.radiogroup_id });

        ajaxRequest(params, function(response) {   
            var result = JSON.parse(response);
            if(result.error) {
                showError(result.error.message);
            } else {
                showAddModal(result.data, '<i class="icon-link"></i>&nbsp;Link a schedule to this group', 'javascript:linkSchedule()');
            }
            return false;
        });     
    });

});

/*************************
        ADD SCHEDULE
**************************/
function linkSchedule() {

    setStatus('Sending the data to the server');
    Group.schedule_id = $('#lst-entity-add').select2('data').id;
    Group.schedule_description = $('#lst-entity-add').select2('data').text;

    var params = getParameters('group/associate', {radiogroup_id: Group.radiogroup_id, schedule_id: Group.schedule_id});
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            showSuccess('Associated the "' + Group.schedule_description + '" schedule to the "' + Group.description + '" group!');
            console.log('1')
            $('#data_table_inner').dataTable().fnAddData([
                Group.schedule_id,
                Group.schedule_description] 
            );
            console.log('2')
            $('div.dataTables_length select').select2({width:60}); 
        }
    });
    
    hideAddModal();
    setStatus('Done sending the data to the server');
}

/*************************
      DELETE GROUP
**************************/
function deleteGroup() {
    
    console.log('deleteGroup triggered @ ' + moment().format("hh:mm:ss a"));
    setStatus('Deleting the ' + Group.description + '...');

    var params = Group.parameterizeFor('delete');
    ajaxRequest(params, function(response) {
        result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            $(Group.row).remove();
            showSuccess('The group "<b>' + Group.description + '</b>" (#'+ Group.radiogroup_id + ') was deleted');
        }        
    });    

    hideRemoveModal();
    setStatus('Finished processing the request');
}  