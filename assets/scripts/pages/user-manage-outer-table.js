var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                for (var i = 1, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }
                oTable.fnDraw();
            }

            function editNewRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[1].innerHTML = '<span class="m-wrap small">' + aData[1] + '</span>';
                jqTds[2].innerHTML = '<input type="text" class="m-wrap small">' + aData[2] + '</input>';
                jqTds[3].innerHTML = '<input type="password" class="m-wrap small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            var editRow = function(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[1].innerHTML = '<span class="m-wrap small">' + aData[1] + '</span>';
                jqTds[2].innerHTML = '<span class="m-wrap small">' + aData[2] + '</span>';
                jqTds[3].innerHTML = '<input type="password" class="m-wrap small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';
            }

            var saveUpdated = function(oTable, nRow) {

                var jqInputs = $('input', nRow);
                if(!validInputs(jqInputs)) {
                    return false;
                }

                var aData = oTable.fnGetData(nRow);
                var sRoute = 'user/update';

                var params = null
                if(_.isEmpty(jqInputs[0].value)) {
                    params = $.param({ route: sRoute, params: {subaccount_username:aData[2], note:jqInputs[1].value} });
                }

                if(_.isEmpty(jqInputs[1].value)) {
                    params = $.param({ route: sRoute, params: {subaccount_username:aData[2], subaccount_new_password:jqInputs[0].value, note:jqInputs[1].value}  });
                }

                if(_.isEmpty(params)) {
                    showError('You have not updated any of this users properties!')
                    return false;
                }

                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        showSuccess("The user record was saved!");
                    }
                    return false;
                });

                oTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 4, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                oTable.fnUpdate('<a class="Deletete" href="">Delete</a>', nRow, 6, false);
                oTable.fnDraw();
            }

            function saveNew(oTable, nRow) {

                var aData = oTable.fnGetData(nRow);
                var jqInputs = $('input', nRow);
                
                var params = getParameters('user/add', { subaccount_username:jqInputs[0].value, subaccount_password:jqInputs[1].value, note:jqInputs[2].value });
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        showSuccess('New user "' + jqInputs[0].value + '" was successfully added and give ID # ' + result.id);
                    }
                    return false;
                });

                oTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 4, false);
                oTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                oTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
                oTable.fnDraw();
            }

            function saveRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                if(aData[6].indexOf('data-mode') > 0) {
                    saveNew(oTable, nRow);
                } else {
                    saveUpdated(oTable, nRow);
                }
            }

            function validInputs(aInputs) {
                // Check Password
                if(aInputs[0].value) {
                    if(aInputs[0].value.length < 1) {
                        showError('Invalid Password.  Must be 1 or more characters in length');
                        return false;
                    }
                }
                
                // Check Notes
                if(aInputs[1].value) {
                    if(aInputs[1].value.length < 1) {
                        showError('Invalid Note.  Must be 1 or more characters in length');
                        return false;
                    }
                }
                return true;
            }

            var nEditing = null;

            // $('#btn-add-object').click(function (e) {
            //     stopEvent(e);
            //     var aiNew = oTable.fnAddData([null, '', '', '', '',
            //             '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
            //     ]);
            //     var nRow = oTable.fnGetNodes(aiNew[0]);
            //     editNewRow(oTable, nRow);
            //     nEditing = nRow;
            // });

            // $('#data_table_outer').on('mousedown', 'a.delete', function (e) {
            //     stopEvent(e);
            //     bootbox.dialog("<p>&nbsp;You do not have permission to complete this action.</p>", [
            //         {
            //             "label" : "OK",
            //             "icon"  : "icon-ok",
            //             "class" : "green",
            //             "callback": function() {
            //                 bootbox.hideAll();
            //             }
            //         }
            //     ]);
            // });

            $('#data_table_outer').on('mousedown', 'a.cancel', function (e) {
                stopEvent(e);
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oTable, nEditing);
                    nEditing = null;
                }
            });

            $('#data_table_outer').on('mousedown', 'a.edit', function (e) {
                stopEvent(e);
                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oTable, nEditing);
                    editRow(oTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();