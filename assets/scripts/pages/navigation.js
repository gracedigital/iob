var lastParent;
var lastChild;
var lastMenu;
var History = window.History; 
var	State = History.getState();

jQuery(document).ready(function() {		
	
	// Index.init();

    var getRouteFromURL = function() {
		var n = location.href.indexOf("=")+1;
		return location.href.substring(n);
    }

    var getLast = function() {
       	// am i a top level menu that is open?
        var last = $('.has-sub.open', $('.page-sidebar'));

        // am i a top level menu that is closed?
        if (last.size() == 0) {
            last = $('.has-sub.active', $('.page-sidebar'));
        }
        // am i a sub-menu item?
        if (last.size() == 0) {
        	last = $('.active', $('.page-sidebar'));
        }
        return last;
    }

	var handleContentHeight = function () {
		var content = $('.page-content');
        var sidebar = $('.page-sidebar');

        if (!content.attr("data-height")) {
            content.attr("data-height", content.height());
        }

        if (sidebar.height() > content.height()) {
            content.css("min-height", sidebar.height() + 20);
        } else {
            content.css("min-height", content.attr("data-height"));
        }
	}

	// deactivate parent menu
	var deactivateParent = function(element, classes) {
		// close
		element.removeClass(classes);
		$('.arrow', element).removeClass(classes);
		// collapse
		element.children('ul:first').slideUp(200, function () {
		    handleContentHeight();
		});
		$('.selected').remove(); 	     		
	}

	var activateParent = function(element) {
		// open
		$('.arrow', element).addClass('open');
		element.addClass("open active");
		// select
		$('a:first-child', element).append('<span class="selected"></span>');
		// expand
		element.children('ul:first').slideDown(200, function () {
            handleContentHeight();
        });		        		
	}
	
	// deactivate child menu
	var deactivateChild = function(element) {
		element.removeClass('active');
	}

	// activate child menu
	var activateChild = function(element) {
		element.addClass('active');
	}

	// Handle Navigation
	$(".page-sidebar ul li").click(function() {
		
		lastParent = $('li.active.top');
		lastChild = $('li[class="active"]');

		// Handle click on active parent
		if($(this).is(lastParent)) {
			
			if ($(this).text().trim() === 'Dashboard') {
				var route = $('a:first', $(this)).attr('href');
				loadContent(route);
				return false;
			}

			if($(this).hasClass('open')) {
    			deactivateParent($(this), 'open');	
			} else {
				activateParent($(this));			
			}
			return false;
		}

		var route = $('a:first', $(this)).attr('href');

		// Handle click on inactive parent
		if($(this).hasClass('top')) {
			deactivateChild(lastChild);
			deactivateParent(lastParent, 'open active');
			activateParent($(this));
			if(!$(this).hasClass('has-sub')) {
				// Top level item with no children
				loadContent(route);
			}
			return false;
		} else {
			// Handle child
			if($(this).not(lastChild)) {
				deactivateChild(lastChild);
				activateChild($(this));
			}

			lastMenu = $(this).find('span:first');
			if(lastMenu.find('.icon-spinner').length < 1) {
				lastMenu.append('<i class="icon-spinner icon-mirrored icon-spin"></i>');
			}

			loadContent(route);
			return false;
		}
	});

	$(this).on("contentLoaded", function() {
		setTimeout('removeSpinner()', 1000);
	});

});

function removeSpinner() {
	$('.page-sidebar').find('.icon-spinner').each(function() {
	  $(this).remove();
	});
}