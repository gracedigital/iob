/*** 
    Add addClone method to html table: 
    Clone a tr element within an html table 
 ***/
jQuery.fn.addClone = function() {

    return this.each(function () {

        var tab = $('.tab-pane.active');
        var dow = tab.attr('id').slice(-1);

        // check for too many clones
        // if($('table tbody tr', tab).length > 6) {
        //     console.log("ACTIVE TAB: "+dow);
        //     console.log(tab);            
        //     showWarning('You have added the maximum number of events for ' + daysofweek[dow] +'!');
        //     return false;
        //     console.log('still here');
        // }

        // get row for cloning
        var rowCount = $('table:visible > tbody > tr', tab).length - 1;
        var row = $('table:visible > tbody > tr:nth-child('+rowCount+')', tab);
        var parent = {};

        // use tbody or table parent
        if ($(row).parents('tbody').length>0) {
            parent = $(row).parents('tbody');
        } else {
            parent = $(row).parents('table');
        }

        // call destroy to revert the DOM changes
        $(row).find('select').select2("destroy");
        $(row).find('input[name=vol]').pcntspinner("destroy");

        // get clone
        var copy = $(row).clone();

        // restore original volume selector
        $('input[name=vol]', row).pcntspinner({ min: 0, max: 100, step: 1 });        

        // initalize cloned volume selector
        $('input[name=vol]', copy).pcntspinner({ min: 0, max: 100, step: 1 });        
        
        // set next time
        var timePicker = $('.timepicker-default', copy);
        var prevTime = timePicker.val();       
        var nextTime = Date.parse(prevTime).addMinutes(5).toString("hh:mm tt");
        timePicker.timepicker({ minuteStep:5 });
        timePicker.timepicker('setTime', nextTime).attr('value', nextTime);


        $(copy).addClass('sadey');
        $(copy).addClass('isclone');
        $(copy).removeAttr('id');
        $('.icon-unlock', copy).addClass('icon-table').removeClass('icon-unlock');

        // inject clone
        magicRow = ($('.isclone', tab).length === 0) ? row : $('.isclone:last', tab);
        copy.insertAfter(magicRow); 

        // remove last td and replace with remove html
        $('.sadey').children('td:last').remove();
        $('.sadey').append('<td><a class="btn mini red" style="margin-top:7%;" onclick="$(this).killClone()"><i class="icon-trash"></i> Delete Part</a></td>');

        // increment all names
        $('.isclone').each(function() {
          var element = $(this).find('input:first');
          var index = parseInt($(this)[0].sectionRowIndex);
          element.attr('value', 'Day Part ' + index);
        });

        // restore select2
        $('.tab-pane.active').find('select').select2({
            placeholder:'Choose a channel', 
            allowClear: true
        });
        
        // remove active tag
        $('.sadey').removeClass('sadey');

        return false;
    });
};

// Add killClone method to html table: Remove a tr element and re-index the remaining clones
jQuery.fn.killClone = function() {
    
    var row = $(this).parents('tr');
    var eventId = row.attr('id');
    if(eventId) {
        Schedule.deleteEvent(eventId);
    }

    $(row).remove();

    // re-index
    var id = 1;
    $('.isclone', '.tab-pane.active').each(function () {
        $(this).find('.daypart').each(function(index) {
            var tempId = $(this).attr('id');
            if (typeof tempId != 'undefined' && tempId!='') {
                tempId = tempId.split('_');
                $(this).attr('id',tempId[0]  + '_' +  id);
            }

            var tempName = $(this).attr('name');
            if (typeof tempName != 'undefined' && tempName!='') {
                tempName = tempName.split('_');
                $(this).attr('name',tempName[0]  + '_' +  id);
            }

            $(this).attr('placeholder', 'Day Part ' + id);
        });

        id++;
     });
};