var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {

            var restoreRow = function(oOuterTable, nRow) {
                console.log('restoreRow:cancelEditRow triggered @ ' + moment().format("hh:mm:ss a"));
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oOuterTable.fnUpdate(aData[i], nRow, i, false);
                }
                
                // oOuterTable.fnDraw();
                $('#th_folder_delete').empty().append('Delete');
                
                console.log('restoreRow:cancelEditRow triggered @ ' + moment().format("hh:mm:ss a"));
            }

            var editRow = function(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);              
                jqTds[3].innerHTML = '<input type="text" class="m-wrap large" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="m-wrap large" value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<span class="edit-folder" style="cursor:pointer;color:#0D638F;">Save</span>';
                jqTds[6].innerHTML = '<span class="cancel-folder" style="cursor:pointer;color:#0D638F;">Cancel</span>';
            }

            var saveRow = function(oOuterTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oOuterTable.fnGetData(nRow);

                // Set the folder properties for update
                Folder.prepareFor('update', nRow, aData, jqInputs);

                var params = Folder.parameterizeFor('update');
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {          
                        showError(result.error.message);
                    } else { 
                        oOuterTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                        oOuterTable.fnUpdate(jqInputs[1].value, nRow, 4, false);
                        oOuterTable.fnUpdate('<span class="edit-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt" style="width:100%;"></i></span>', nRow, 5, false);
                        oOuterTable.fnUpdate('<span class="delete-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-remove" style="width:100%;"></i></span>', nRow, 6, false);
                        showSuccess('The lineup "' + Folder.title + '" was updated!');
                    }
                });
            }

            var cancelEditRow = function(oOuterTable, nRow) {
                console.log('channel-manage-outer:cancelEditRow triggered @ ' + moment().format("hh:mm:ss a"));
                var jqInputs = $('input', nRow);
                oOuterTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oOuterTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oOuterTable.fnUpdate('<span class="edit-folder" style="cursor:pointer;color:#0D638F;">Edit</span>', nRow, 5, false);
                oOuterTable.fnDraw();
                console.log('channel-manage-outer:cancelEditRow exited @ ' + moment().format("hh:mm:ss a"));
            }

            var nEditing = null;


            /********************
                EDIT FOLDER
            ********************/
            $('#data_table').on('mousedown', '.edit-folder', function (e) {

                logMethod('channel-manage-outer:edit_folder', 'triggered');

                $('#th_folder_delete').empty().append('');

                /* Get the row as a parent of the link that was mousedowned on */
                var nRow = $(this).closest('tr')[0];
                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oOuterTable, nEditing);
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oOuterTable, nEditing);
                    $('#th_folder_delete').empty().append('Delete');
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                }

                console.log('channel-manage-outer:edit_folder exited @ ' + moment().format("hh:mm:ss a"));
            });

            /********************
                CANCEL EDIT
            ********************/
            $('#data_table').on('mousedown', '.cancel-folder', function (e) {
                console.log('channel-manage-outer:cancel_folder triggered @ ' + moment().format("hh:mm:ss a"));

                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oOuterTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oOuterTable, nEditing);
                    nEditing = null;
                }
                console.log('channel-manage-outer:cancel_folder exited @ ' + moment().format("hh:mm:ss a"));                
            });

            /********************
                DELETE FOLDER
            ********************/
            $('#data_table').on('mousedown', '.delete-folder', function(e) {    
                var nRow = $(this).closest('tr')[0];
                var aData = oOuterTable.fnGetData(nRow);
                Folder.prepareFor('delete', nRow, aData, null);
                showRemoveModal(Folder);
            });

            // Handle enter key to save an update
            $('#data_table').on('keypress', 'input[type="text"]', function(e) {
                if(e.which == 13) {
                    $('.edit-folder', $(this).closest('tr')).mousedown();
                }
            });       
        }

    };

}();