$(document).ready(function() {

    /*
        PAGE INITALIZATION
    */
    Schedule.clear();
    clearMessages();

    // initalize schedule selector
    $('#schedule_list').select2().focus();
    $('#schedule_list').pulsate({color:"#35aa46",repeat: 2});
    
    // Fix to be able to hide page alert
    $('button.close').on('mousedown', function(e) {
      $(this).parent().hide();
      return false;
    });

    /*
        PAGE EVENT HANDLERS
    */
    $('#schedule_list').on('change', function(e) {

        clearMessages();
        $('.tab-content').empty();        
        $('.schedule-watermark').hide();
        $('.nav-tabs').find('li').each(function() {
            $(this).removeClass('active');
        });

        Schedule.id = $(this).select2('data').id;
        Schedule.description = $(this).select2('data').text;  
        $('.nav-tabs').find('li:first').addClass('active');
        setStatus('Retreiving the schedule from the server');

        // get events for schedule & day of week
        var params = getParameters('schedule/edit_events', { schedule_id: Schedule.id, list_id: '-1' });
        ajaxRequest(params, function (response) {
            if(!response) {
                showWarning('No events defined for this schedule');
                return false;
            }

            $('.tab-content').empty().append(response);
            $(".tab-pane:first").addClass('active');
            $('.tab-content').each(function(index) {
                $('.select2', $(this)).each(function() {
                    if($(this).attr('data-channel')) {
                        $(this).select2().select2("val", $(this).attr('data-channel'));
                    } else {
                        $(this).select2();
                    }
                });
            });

            setStatus('Finished retreiving the data');
            $('#loading-icon').hide();
            $('#submit-icon').show();
            clearStatus();
        });        

        return false;
    });
    
    $('body').on('mousedown', '.icon-time', function(e) {
        var element = $(this).parent().parent().find('input:first');
        element.timepicker({
            minuteStep: 5,
            defaultTime: element.val()
        });
    });

    // flag row as dirty if controls are changed...
    $('body').on('change', '.select2.channel', function(e) {
        $(this).closest('tr').addClass('dirty');
    });

    // workaround for the select2 plugin (time)
    $('body').on('change', '.timepicker-default', function(e) {
        $(this).attr('value', $(this).val());
        $(this).closest('tr').addClass('dirty');
    });

    // workaround for number input (volume)
    $('body').on('change', ':input[name=vol]', function(e) {
        $(this).attr('value', $(this).val());
        $(this).closest('tr').addClass('dirty');
    });

    // show volume as percentage 
    $('body').on('change paste', 'input[type=number]', function(){
        $('.volume-percent', $(this).parent()).text((( $(this).val() * 5 ))+'%');        
    });

    // handle form cancellation
    $('#btn-edit-cancel').on('mousedown', function () {
        loadContent('schedule/edit');
    });
    
    // handle form submission
    $('#btn-edit-submit').on('mousedown', function () {

        clearMessages();
        clearFormErrors();

        if(!isValidSchedule()) {
            return false;
        }
        
        Schedule.dirty = 0;
    	var schedule = [];
        var scheduleUpdates = [];

        $('#status-bar').show();
        $('#submit-icon').hide();
        $('#loading-icon').show();

        // iterate each day-of-week
    	$('.tab-pane').each(function(index) {

            // iterate each event for the day
            $('table#day-parts > tbody > tr', $(this)).each(function(rowIndex) {

                var event = {};
                var eventId = $(this).attr('id');
                var standardTime = $(this).find('input[name="event_time"]').attr('value');

                event['sort'] = rowIndex;
                event['vol'] = $(this).find('input[name="vol"]').pcntspinner('value')
                event['stn'] = $(this).find('select[name="stn"]').select2('data').id;
                event['event_time'] = convertToMilitaryTime(standardTime).trim();

                if ($(this).hasClass('dirty') && !$(this).hasClass('isclone')) {
                    Schedule.dirty++; 
                }

                // add this event to list to be processed
                if(event['event_time']) {
                    if(eventId > 0) {
                        event['event_id'] = eventId;
                        Schedule.updateEvent(index, event);                            
                    } else {
                        Schedule.addEvent(index, event); 
                    } 
                }
            });

    	});

        // console.log(Schedule.print());
        // console.log(Schedule.serialize());
        setStatus('Sending the data to the server');         

        var params = getParameters('schedule/update_events', Schedule.serialize());
        ajaxPost(params, function(response) {
            var result = JSON.parse(response);
            if(result.error) {
                showError(result.error.message);
            } else {
                setStatus('Done sending the data to the server');
                showSuccess(getSuccessMessage(Schedule.description, result));

                setTimeout(function(){
                    loadContent('schedule/edit');
                }, 5000);                
            }

            $('#loading-icon').hide();
            $('#submit-icon').show();
            return false;
        });

    });

    
    /*
        PAGE FUNCTIONS
    */
    var isValidSchedule = function() {
        var dow;
        try {
            $('.tab-pane').each(function(index) {
                dow = $(this).attr('id').slice(-1);
                if(isValidScheduleTimes($(this), dow) && isValidScheduleChannels($(this), dow)) {
                    setStatus('The schedule is valid');
                }
            });
        } catch(error) {
            setStatus('The schedule is invalid');
            $('a[href="#tab_1_'+dow+'"]').trigger('click');
            showError(error);
            clearLoading();
            return false;            
        }
        return true;
    } 

    var isValidId = function () {
        if(isEmpty(Schedule.id)) {
            showError('You need to pick a schedule from the list before you can continue!');
            return false;
        }
        return true;
    }

    var isValidScheduleTimes = function (tab, dow) {

        setStatus('Validating times for ' + daysofweek[dow]);
        var pickers = $('.timepicker-default', tab);

        for (var index = 0; index < pickers.length; index++) {
            // check for empty event time
            var element = $(pickers[index]);
            if(isEmpty(element.val())) {    
                element.parent().parent().addClass('error').focus();
                throw 'You must choose a time for this event!'
            } 

            // check for same times
            if(index > 0) {
                var curTime = $(pickers[index]).val();
                var prevTime = $(pickers[index-1]).val();
                if(curTime == prevTime) {
                    element.parent().parent().addClass('error');
                    throw 'There is an invalid event time ' + daysofweek[dow] + '! Two consecutive events may not have the same start time.';
                }
            }
        }
        return true;
    }

    var isValidScheduleChannels = function(tab, dow) {
        
        setStatus('Validating channels for ' + daysofweek[dow]);
        var selects = $('.select2', tab);

        for (var index = 0; index < selects.length; index++) {
            var element = $(selects[index]);
            if(isEmpty(element.select2('data').text)) {
                element.addClass('alert alert-error');
                throw 'You need to select a channel for event "' + $('.daypart',element.closest('tr')).val() + '" on ' + daysofweek[dow];
            }
        }
        return true;
    }

    var clearFormErrors = function() {
        $('.select2-container').removeClass('alert alert-error');
    }

    var resetSchedule = function() {
        $('.tab-pane').each(function( index ) {
            $('#schedule_name').val('');
            // uncheck
            $(':checkbox').prop('checked', false);
            $.uniform.update(':checkbox');
            // reset controls - have to, because these controls suck
            $('.timepicker-default:first', $(this)).val('07:00 AM');
            $('.timepicker-default:last', $(this)).val('12:00 AM'); 
            $('select', $(this)).select2();
            $('div.select2-container:first', $(this)).remove();
        });
    }

    var clearLoading = function() {
        $('#loading-icon').hide();
        $('#submit-icon').show();
    }

    var clearStatus = function() {
        setTimeout(function(){
            $('#status-msg').text('');
            $('#status-bar').slideUp('slow');
        }, 5000);
    }

    var setStatus = function(message) {
        $('#status-bar').slideDown();
        $('#status-msg').text(message + '...');
    }

    var getSuccessMessage = function(description, result) {
        var msg = 'The schedule "<b>' + Schedule.description + '</b>" was modified.  There was (' + result.added + ') ' + getPlural(result.added) + ' added, (' + result.deleted + ') ' + getPlural(result.deleted) + ' deleted and (' + Schedule.dirty + ') total ' + getPlural(Schedule.dirty) + ' processed.';
        return msg;
    }

    var getPlural = function(count) {
        return (count == 1) ? 'event' : 'events';
    }

    var isEmpty = function(obj) {
        if (typeof obj == 'undefined' || obj === null || obj === '' || obj == 'undefined' || obj == '[]') return true;
        if (typeof obj == 'number' && isNaN(obj)) return true;
        if (obj instanceof Date && isNaN(Number(obj))) return true;
        return false;
    }

    var isEmptyObject = function(obj) {
        return Object.keys(obj).length === 0;
    }    

});

