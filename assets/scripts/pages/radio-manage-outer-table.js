var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oOuterTable.fnUpdate(aData[i], nRow, i, false);
                }
                
                $('#th_radio_delete').empty().append('Delete');
            }

            function editRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow); 
                jqTds[4].innerHTML = '<input type="text" class="m-wrap" maxlength="40" value="' + aData[4] + '">';         
                jqTds[6].innerHTML = '<span class="edit-radio" style="cursor:pointer;color:#0D638F;">Save</span>';
                jqTds[7].innerHTML = '<span class="cancel-edit" style="cursor:pointer;color:#0D638F;">Cancel</span>';

                $(jqTds[4]).find('input').pulsate({ color: "#E02222", repeat: 2 });
            }

            function saveRow(oOuterTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oOuterTable.fnGetData(nRow);

                // Set the radio properties for rename action
                Radio.prepareFor('rename', nRow, aData, jqInputs);

                var params = Radio.parameterizeFor('rename');
               
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        oOuterTable.fnUpdate(jqInputs[0].value, nRow, 4, false);
                        oOuterTable.fnUpdate('<span class="edit-radio" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt"></i></span>', nRow, 6, false);
                        oOuterTable.fnUpdate('<span class="delete-radio" style="cursor:pointer;color:#0D638F;"><i class="icon-remove"></i></span>', nRow, 7, false);
                        showSuccess('The radio serial # "' + Radio.serial + '" was renamed!');
                    }
                });
            }

            var nEditing = null;

            /********************
                EDIT RADIO
            ********************/ 
            $('#data_table').on('mousedown', '.edit-radio', function (e) {

                logMethod('radio-manage-outer:edit-radio', 'triggered');
                $('#th_radio_delete').empty().append('');

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).closest('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oOuterTable, nEditing);
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oOuterTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                }
            });

            /********************
                CANCEL EDIT
            ********************/ 
            $('#data_table').on('mousedown', '.cancel-edit', function (e) {
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oOuterTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oOuterTable, nEditing);
                    nEditing = null;
                }
            });

            /********************
                DELETE RADIO
            ********************/            
            $('#data_table').on('mousedown', '.delete-radio', function (e) { 
                var nRow = $(this).closest('tr')[0];
                var aData = oOuterTable.fnGetData(nRow);
                Radio.prepareFor('delete', nRow, aData, null);
                showRemoveModal(Radio);
            });

            // Handle enter key to save an update
            $('#data_table').on('keypress', 'input[type="text"]', function(e) {
                if(e.which == 13) {
                    $('.edit-radio', $(this).closest('tr')).mousedown();
                }
            });   
        }
    };
}();