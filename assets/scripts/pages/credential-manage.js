var oGroups = {};
var oEntity = {};
var oOuterTable = null;

$(document).ready(function() {		

	OuterTable.init();
	clearMessages();

	// Initialize the outer table
    oOuterTable = $('#data_table').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
        "aaSorting": [[ 1, "asc" ]],
		"sDom": '<"headwrap"lf<"clear_search">>rtip',
      	"fnDrawCallback": function( oSettings ) {
        	if($('.dataTables_filter input').val().length >0) {
          		$('.clear_search').show();
          		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
        	}
        },
		"aoColumns": [ 
		    /* Counter 	*/  { "sWidth": ".5%", "bSortable": false },
			/* User 	*/ 	{ "sWidth": "10%", "bSortable": true },
			/* Desc 	*/ 	{ "sWidth": "10%", "bSortable": true },
			/* Group 	*/	{ "sWidth": "10%", "bSortable": true },
			/* Serial 	*/  { "sWidth": "10%", "bSortable": true, },
			/* Edit 	*/ 	{ "sWidth": "01%", "bSortable": false },
			/* Delete 	*/ 	{ "sWidth": "01%", "bSortable": false },
			/* Empty 	*/ 	{ "sWidth": "01%", "bSortable": false },
			/* RGID 	*/ 	{ "bVisible": false }
		]					
    });	  

	// Hide page alert div
	$('button.close').on('mousedown', function(e) {
	  	$(this).parent().hide();
	  	return false;
	});

	// Select2 the datatable 'records per page' select element
    $('div.dataTables_length select').select2({width:60})

    // DataTables filter clear
	$('body').on('mousedown', '.clear_search', function(e) {
		stopEvent(e);
		$('.dataTables_filter input').val('');
		oOuterTable.fnFilterClear();
		$('.clear_search').hide();
		$('.dataTables_filter input').css('background','none');
		return false;
	});

	$('.dataTables_filter input').on('keyup', function(e) {
		stopEvent(e);
		if($(this).val().length == 0){
			$('.clear_search').hide();
			$('.dataTables_filter input').css('background','none');
		} else {
			$('.clear_search').show();
			$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
		}
		return false;
	});

	// DataTables add search filter
	var filter = getParameterByName('rt').split('/')[2];
	if(filter) {	
		filter = filter.charAt(0).toUpperCase() + filter.slice(1);
		oOuterTable.fnFilter(filter,null,false,true,true,false);
		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
	}

});	

// Handle radio deletion
function deleteCredential() {
    setStatus('Deleting the credential...');
    
    var params = getParameters('credential/delete', { sirius_userdetail_username: oEntity.sirius_userdetail_username, sirius_userdetail_id: oEntity.sirius_userdetail_id });
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            oOuterTable.fnDeleteRow(oEntity.row);
            showSuccess('Deleted the credential "' + oEntity.description + '"');
        }
    });

    setStatus('Done deleting the credential');
	hideRemoveModal();    
}	