var oOuterTable = null;
var oInnerTable = null;

$(document).ready(function () {
	
	clearMessages();
	OuterTable.init();
	InnerTable.init();

	// Fix to be able to hide page alert
	$('button.close').on('click', function(e) {
	  $(this).parent().hide();
	  return false;
	});

    oOuterTable = $('#data_table').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
		"sDom": '<"headwrap"lf<"clear_search">>rtip',
      	"fnDrawCallback": function( oSettings ) {
        	if($('.dataTables_filter input').val().length >0) {
          		$('.clear_search').show();
          		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
        	}
        },        
		"aoColumns": [ 
			/* Counter */   { "sWidth": "3.5%", "bSortable": false },
			/* Details */   { "sWidth": "07%", "bSortable": false },
			/* Id */ 		{ "sWidth": "07%", "bSortable": false },
			/* Name */ 		{ "sWidth": "30%", "bSortable": true },
			/* Notes */ 	{ "sWidth": "*", "bSortable": true },
			/* Edit */ 		{ "sWidth": "05%", "bSortable": false },
			/* Delete */ 	{ "sWidth": "03%", "bSortable": false },
			/* Empty */ 	{ "sWidth": "01%", "bSortable": false }
		]					
    });
									
	// Add search filter
	$('body').on('mousedown', '.clear_search', function() {
		$('.dataTables_filter input').val('');
		oOuterTable.fnFilterClear();
		$('.clear_search').hide();
		$('.dataTables_filter input').css('background','none');
	});

	$('.dataTables_filter input').on('keyup', function() {
		if($(this).val().length == 0){
			$('.clear_search').hide();
			$('.dataTables_filter input').css('background','none');
		} else {
			$('.clear_search').show();
			$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
		}
	});

	// Load inner table
	var anOpen = [];
	var link, icon, nDetailsRow, oldRow, nTr;

	$('#data_table').on('mousedown', '.expand', function (e) {

		stopEvent(e);
        clearMessages();

    	oldRow = nTr;
    	oldLink = link;
    	oldIcon = icon;

        link = $(this);
        icon = $(this).prev();	
		nDetailsRow = null;

		// COLLAPSE THE OLD ROW //
    	if(oldRow) {	
			$('div.innerDetails',  oldRow[0]).slideUp(function () {
				oldIcon.removeClass('icon-spinner icon-mirrored icon-spin').removeClass('icon-minus');
				oldIcon.addClass('icon-plus'); 
				oldLink.text(' Expand');
			  	oOuterTable.fnClose(oldRow);
			  	anOpen.shift();
			  	$('.delete, .edit', '#data_table').show();
			});	
		}

		nTr = $(this).closest('tr')[0];
		var i = $.inArray(nTr, anOpen);
		var aData = oOuterTable.fnGetData(nTr);
		
		Folder.list_id = aData[2].trim();
		Folder.title = aData[3].trim();

		// EXPAND THE NEW ROW //
		if(nTr !== oldRow || i === -1) {
			icon.removeClass('icon-plus');
			icon.addClass('icon-spinner icon-mirrored icon-spin'); 

			var params = Folder.parameterizeFor('channels');
    		ajaxRequest(params, function(response) {

		 		nDetailsRow = oOuterTable.fnOpen(nTr, response, 'details');

		 		oInnerTable = $('#data_table_inner').dataTable({
			        "bDestroy"	: true,
			        "bPaginate" : false,
			        "sDom"		: '',
					"aoColumns"	: [ 

						/* ListID */ 	{ "sWidth": "0%"},
						/* EntryID */ 	{ "sWidth": "7.5%" },
						/* Title */ 	{ "sWidth": "30%" },
						/* URL */ 		{ "sWidth": "*" },
						/* Edit */ 		{ "sWidth": "5%", "bSortable": false },
						/* Delete */ 	{ "sWidth": "3%", "bSortable": false },
						/* Empty */ 	{ "sWidth": "1%", "bSortable": false }
					]						 			
		 		});    	
		 		
		 		// var jqTds = $('>td', nRow);
		 		$('.delete, .edit', '#data_table').hide();
		 		$('div.innerDetails', nDetailsRow).slideDown(); 
				icon.removeClass('icon-spinner icon-mirrored icon-spin');
				icon.addClass('icon-minus');						
				link.text(' Collapse');
				anOpen.push(nTr);
			});

		} else {
			$('div.innerDetails', $(oldRow).next()[0]).slideUp( function () {
				anOpen.shift();
				oOuterTable.fnClose(nTr);
			});
		}

	    return false;
	});

    // Select2 the datatable 'records per page' select element
    $('div.dataTables_length select').select2();

});
	
function deleteFolder() {

	logMethod('channel-manage:deleteFolder', 'triggered');
    setStatus('Deleting ' + Folder.title + '...');

    var params = Folder.parameterizeFor('delete');
    ajaxRequest(params, function(response) {
        result = JSON.parse(response);
	    if(result.error) {
	        showError(result.error.message);
	    } else {
	       	oOuterTable.fnDeleteRow(Folder.row);
	        showSuccess('The lineup "' + Folder.title + '" was deleted.');
	    }
	});

	hideRemoveModal();
    setStatus('Done processing the action');
	logMethod('channel-manage:deleteFolder', 'exited');
}

function deleteChannel() {

    logMethod('channel-manage:deleteChannel', 'triggered');
    setStatus('Disassociating ' + Channel.title + '...');
    
    var params = Channel.parameterizeFor('unlink');
console.log('lineup-manage:deleteChannel params:');
console.log(params);    
    ajaxRequest(params, function(response) {
        result = JSON.parse(response);
	    if(result.error) {
	        showError(result.error.message);
	    } else {
	        oInnerTable.fnDeleteRow(Channel.row);
	        showSuccess('The channel "' + Channel.title + '" was deleted from the lineup ' + Folder.title + '.');
	    }
	});
	  
    hideRemoveModal();
    setStatus('Done processing the action');
    logMethod('channel-manage:deleteChannel', 'exited');
}   