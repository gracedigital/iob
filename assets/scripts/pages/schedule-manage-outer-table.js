var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {

            var nRow = null;
            var nEditing = null;

            function restoreRow(oOuterTable, nRow) {

                if(oOuterTable) {
                    var aData = oOuterTable.fnGetData(nRow);
                    var jqTds = $('>td', nRow);

                    for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                        oOuterTable.fnUpdate(aData[i], nRow, i, false);
                    }

                    $('#th_schedule_delete').empty().append('Delete');
                    oOuterTable.fnDraw();
                }
            }

            function editRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow); 
                if(!jqTds) {console.log(jqTds);}
                jqTds[3].innerHTML = '<input type="text" class="m-wrap large" value="' + aData[3] + '">';
                jqTds[5].innerHTML = '<span class="edit-schedule" style="cursor:pointer;color:#0D638F;">Save</span>';
                jqTds[6].innerHTML = '<span class="cancel-edit" style="cursor:pointer;color:#0D638F;">Cancel</span>';
            }

            function saveRow(oOuterTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oOuterTable.fnGetData(nRow);
                var modified = moment().format("YYYY-DD-MM hh:mm:ss a");
                var params = getParameters('schedule/update', { schedule_id: aData[2], description: jqInputs[0].value, note: modified, obsolete: false });

                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        if(result.schedule_id) {
                            oOuterTable.fnUpdate(result.schedule_id, nRow, 2, false);
                        }
                        oOuterTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                        oOuterTable.fnUpdate(modified, nRow, 4, false);
                        oOuterTable.fnUpdate('<span class="edit-schedule" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt" style="width:100%;"></i></span>', nRow, 5, false);
                        oOuterTable.fnUpdate('<span class="delete-schedule" style="cursor:pointer;color:#0D638F;"><i class="icon-remove" style="width:100%;"></i></span>', nRow, 6, false);
                        oOuterTable.fnDraw();   
                        showSuccess('The schedule "' + jqInputs[0].value + '" has been updated!');
                    }
                });

                $('#th_schedule_delete').empty().append('Delete');

            }

            function cancelEditRow(oOuterTable, nRow) {
                var jqInputs = $('input', nRow);
                oOuterTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oOuterTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                oOuterTable.fnDraw();
            }   


            // Delete Schedule
            $('#data_table').on('mousedown', '.delete-schedule', function (e) {
                
                stopEvent(e);
                nRow = $(this).parents('tr')[0];
                var aData = oOuterTable.fnGetData(nRow);

                // cache schedule properties
                oSchedule.row = nRow;
                oSchedule.id = aData[2];   
                oSchedule.message =  'Are you sure you want to delete the schedule "<b>' + aData[3] + '</b>"?';
                oSchedule.callback = 'javascript:deleteSchedule()';

                showRemoveModal(oSchedule)
            });

            $('#data_table').on('mousedown', '.cancel-edit', function (e) {

                stopEvent(e);
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oOuterTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oOuterTable, nEditing);
                    nEditing = null;
                }
                return false;
            });

            $('#data_table').on('mousedown', '.edit-schedule', function (e) {

                stopEvent(e);
                $('#th_schedule_delete').empty().append('');

                /* Get the row as a parent of the link that was mousedowned on */
                var nRow = $(this).closest('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oOuterTable, nEditing);
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oOuterTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                }
                return false;
            });
        }

    };

}();