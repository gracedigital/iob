var oEntity = {};
var oTable = null;
var sourceId = 0;
var targetId = 0;
var targetName = '';
var sourceData = '';
var credentials = new Array();
var rowNodes = new Array();

$(document).ready(function() {		

	// Fix to be able to hide page alert
	$('button.close').on('click', function(e) {
	  	$(this).parent().hide();
	  	return false;
	});

	// Initalize select elements
	$('#sourceList').select2({
		placeholder: "Choose a group", 
		allowClear: true,
		width: '355'
	});				

	$('#targetList').select2({
		placeholder: "Choose a group", 
		allowClear: true,
		width: '355'
	});		

  	// Souce List Change
	$('#sourceList').on('change', function() {

		// make sure there is a selected value not just cleared
		if($('option:selected', $(this)).val() > 0) {
			clearMessages();

			// Show the option in target list that was previously hidden
			if(sourceId > 0) {
				$("option[value='" + sourceId + "']", $('#targetList')).show();
			}

			// Set the id of the selected folder
			sourceId = $("option:selected", this).val();

			// Reset target list
			$("#targetList").append(sourceData);
			$('#targetList').select2({
				placeholder: "Choose a group", 
				allowClear: true,
				width: '355'
			});		

			// Get the data
			var params = getParameters('credential/list', { radiogroup_id: sourceId, expand: true });
			ajaxRequest(params, function(response) {
				var result = JSON.parse(response);
				if(result.error) {
					showError(result.error.message);
				} else {
					loadCredentialTable(result);		
				}
			});
		}

		// reset checkboxes
		$("#data_table input:checkbox").each(function() {
			$(this).prop('checked', false);
		});	
		// reset table data
		if(oTable) {
			oTable.fnClearTable();
		}	
		// reset button
		setButtonState();
	});

	// Target List Change
	$('#targetList').on('change', function() {
	    setButtonState();	
		targetId = $('option:selected', this).val();
		targetName = $("option:selected", this).text();
	});

	// Handle check all
    $('#data_table').on('change', '.group-checkable', function () {

        var checked = $(this).is(":checked");
        var set = $('#data_table input:checkbox');
        $(set).each(function () {
        	// alert(checked);
            $(this).prop("checked", checked);
        });

        $.uniform.update(set);		
       	setButtonState();
    });

    // Handle manual checkbox change
	$('#data_table').on('change', '.checkboxes', function () {
		if(!$(this).prop("checked")) {
			$('.group-checkable').prop('checked', false);
		}
		setButtonState();
	});

	$('button.move-credential').on('mousedown', function (e) {
		oEntity.type = 'credential';
		oEntity.source = $("option:selected", '#sourceList').text();
		oEntity.target = $("option:selected", '#targetList').text(); 
		oEntity.callback = 'javascript:moveCredentials()';
		oEntity.message = 'Are you sure you want to move the credential(s) from <br><small style="margin:10px;">"<b>' + oEntity.source + '</b>" to "<b>' + oEntity.target + '</b>"?</small>';
		showMoveModal(oEntity);
    });

	var setButtonState = function() {
		var state = ($("#data_table input:checkbox:checked").length > 0) && ($('option:selected', '#sourceList').val() > 0) && ($('option:selected', '#targetList').val() > 0) && ($('option:selected', '#sourceList').val() != $('option:selected', '#targetList').val());
		$('button.move-credential').attr('disabled', !state);
	}

	$('#data_table').on('mousedown', '.move-credential', function() {
		moveCredentials();
	});

	// Load DataTable 
	var loadCredentialTable = function (data) {
	  oTable = $('#data_table').dataTable (
	  {
	    "aaSorting"	: [[0, "desc"]],
	    "bDestroy"	: true,
	    "bFilter"  	: true,
	    "aaData"	: data.aaData,   
		"aoColumns"	: [ 
			/* Counter */   { "sWidth": "01%", "bSortable": false },
			/* ID */ 		{ "sWidth": "07%", "bSortable": true },
			/* Username */ 	{ "sWidth": "20%", "bSortable": true },
			/* Descrption */{ "sWidth": "*", "bSortable": true }
		],	       
	    "aoColumnDefs": [{
	      "bSortable": false,
	      "aTargets": [0],
	      "mRender": function (result, type, full) {
	        return '<input type=\"checkbox\" class=\"checkboxes\" value=\"\" />';
	      }
	    }],
	    "fnInitComplete": function(oSettings, json) {
	      // Select2 the DataTable 'records per page' select element
	      $('div.dataTables_length select').select2();      
	    }      
	  });
	}
	 
});

// Move Radios
function moveCredentials() {

	if ($('input:checked', oTable.fnGetNodes()).length > 0) {

		clearMessages();
		$('#modal-move').modal('hide');

		// Get data for checked rows and submit for deletion
		$('input:checked', oTable.fnGetNodes()).each(function() {
			var tr = $(this).closest('tr');
			rowNodes.push(tr);
			var rowIndex = oTable.fnGetPosition(tr[0]);
	    	var rowData = oTable.fnGetData(rowIndex);
			// Get the data from the id & serial columns
	    	credentials.push({ radiogroup_id:targetId, sirius_userdetail_id:rowData[1], sirius_userdetail_username:rowData[2] });
	    });					

		// Set the route
		var params = getParameters('credential/move', { list: credentials });
		// Send the data
		ajaxRequest(params, function(response) {
			var result = JSON.parse(response);
			if(result.error) {
				showError(result.error.message);
			} else {
				// Remove rows from loaded table
				for (index = 0; index < rowNodes.length; ++index) {
					oTable.fnDeleteRow(rowNodes[index][0]);
				}
				
				if(rowNodes.length > 1) {							
					showSuccess('The selected credentials were moved to the "' + targetName + '" group');
				} else {
					showSuccess('The selected credentials was moved to the "' + targetName + '" group');
				}

				// reset form
				$('#btn-move-object').attr('disabled', true);
				$('.group-checkable').prop('checked', false);
				$('select.select2').each(function(){
					$(this).select2('data', null);
				});
				params = null;
			}
		});
		return false;

	} else {
		showError('No credentials have been selected to be moved!');
	}
	return false;
}