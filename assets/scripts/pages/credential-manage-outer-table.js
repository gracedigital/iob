var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {

            function restoreRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oOuterTable.fnUpdate(aData[i], nRow, i, false);
                }
                
                $('#th_credential_delete').empty().append('Delete');
                oOuterTable.fnDraw();
            }

            function editRow(oOuterTable, nRow) {

                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow); 

                getGroups(aData[8]);
                jqTds[2].innerHTML = '<input type="text" class="m-wrap" maxlength="40" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<select id="groupList" class="m-wrap select2">'+ oGroups.list + '</select>';
                jqTds[5].innerHTML = '<a class="edit" href="">Save</a>';
                jqTds[6].innerHTML = '<a class="cancel" href="">Cancel</a>';

                $('#groupList').select2({width:250});

            }

            function saveRow(oOuterTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oOuterTable.fnGetData(nRow);
                
                // Get the updated properties
                oEntity.description = isEmpty(jqInputs[0].value) ? '&nbsp;': jqInputs[0].value;
                oEntity.sirius_userdetail_id = $(nRow).attr('id');
                oEntity.sirius_userdetail_username = aData[1];
                oEntity.radiogroup_id = $('#groupList', nRow).select2('data').id;
                oEntity.radiogroup_description = $('#groupList', nRow).select2('data').text;
                for (var property in oEntity) {
                    if(isEmpty(oEntity[property])) {
                        delete oEntity[property];
                    }
                }

                var params = $.param({route: 'credential/update', params: oEntity});
                ajaxRequest(params, function(response) {

                    var result = JSON.parse(response);
                    if(!isEmpty(result.error)) {
                        showError(result.error);
                    } else {
                        oOuterTable.fnUpdate(oEntity.description, nRow, 2, false);
                        oOuterTable.fnUpdate(oEntity.radiogroup_description, nRow, 3, false);
                        oOuterTable.fnUpdate('<a class="edit" href="javascript:;"><i class="icon-bolt" style="width:100%;"></i></a>', nRow, 5, false);
                        oOuterTable.fnUpdate('<a class="delete" href="javascript:;"><i class="icon-remove" style="width:100%;"></i></a>', nRow, 6, false);
                        oOuterTable.fnUpdate(oEntity.radiogroup_id, nRow, 8, false); // update hidden column
                        showSuccess('The record was updated!');
                    }
                });
                
                $('#th_credential_delete').empty().append('Delete');
            
            }

            function cancelEditRow(oOuterTable, nRow) {
                var jqInputs = $('input', nRow);
                oOuterTable.fnUpdate(jqInputs[0].value, nRow, 4, false);
                oOuterTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 6, false);
                oOuterTable.fnDraw();
            }

            var getGroups = function(radiogroupId) {

                var container = blockContainer();
                var params = $.param({ route: 'group/list', params: { obsolete:false } });
                ajaxSyncRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {
                        showError(result.error.message);
                    } else {
                        var start = result.indexOf(radiogroupId) + radiogroupId.length + 1;  // add one to account for closing quote
                        var end = result.indexOf('>', start);                   
                        oGroups.list = result.substring(0, start) + ' selected="selected"' +  result.substring(end, result.length);
                    }
                });
            }

            /**********************
                DELETE CREDENTIAL
            **********************/
            $('#data_table').on('click', 'a.delete', function (e) {

                stopEvent(e);
                var nRow = $(this).closest('tr')[0];
                var aData = oOuterTable.fnGetData(nRow);
                
                // Get the updated properties
                oEntity.row = nRow;
                oEntity.type = 'credential';
                oEntity.description = aData[2];
                oEntity.sirius_userdetail_id = $(nRow).attr('id');
                oEntity.sirius_userdetail_username = aData[1];
                oEntity.message = 'Are you sure you want to delete the credential "<b>' + oEntity.sirius_userdetail_username + '</b>" from the portal?';
                oEntity.callback = 'javascript:deleteCredential()';

                showRemoveModal(oEntity);
            });

            var nEditing = null;

            $('#data_table').on('click', 'a.cancel', function (e) {
                stopEvent(e);
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oOuterTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oOuterTable, nEditing);
                    nEditing = null;
                }
            });

            /********************
                EDIT CREDENTIAL
            ********************/
            $('#data_table').on('mousedown', 'a.edit', function (e) {

                icon = $(this).find('.icon-bolt');  
                icon.addClass('icon-spinner icon-mirrored icon-spin'); 

                stopEvent(e);
                $('#th_credential_delete').empty().append('');

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).closest('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oOuterTable, nEditing);
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                    
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oOuterTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                }

                icon.removeClass('icon-spinner icon-mirrored icon-spin');
            });
        }

    };

}();