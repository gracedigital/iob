var oSchedule = {};
var oOuterTable = null;
var oInnerTable = null;

$(document).ready(function () {
	
	clearMessages();
	OuterTable.init();

	// Fix to be able to hide page alert
	$('button.close').on('mousedown', function(e) {
	  $(this).parent().hide();
	  return false;
	});

    oOuterTable = $('#data_table').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
		"sDom": '<"headwrap"lf<"clear_search">>rtip',
      	"fnDrawCallback": function( oSettings ) {
        	if($('.dataTables_filter input').val().length >0) {
          		$('.clear_search').show();
          		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
        	}
        },        
		"aoColumns": [ 
			/* Counter */   { "sWidth": "3.5%", "bSortable": false },
			/* Details */   { "sWidth": "07%", "bSortable": false },
			/* Id */ 		{ "sWidth": "07%", "bSortable": true },
			/* Name */ 		{ "sWidth": "60%", "bSortable": true },
			/* Desc */		{ "sWidth": "20%", "bSortable": true },
			/* Edit */ 		{ "sWidth": "05%", "bSortable": false },
			/* Delete */ 	{ "sWidth": "05%", "bSortable": false },
			/* Empty */ 	{ "sWidth": "01%", "bSortable": false }
		]				
    });
					
    // DataTables filter clear
	$('body').on('mousedown', '.clear_search', function(e) {
		stopEvent(e);
		$('.dataTables_filter input').val('');
		oOuterTable.fnFilterClear();
		$('.clear_search').hide();
		$('.dataTables_filter input').css('background','none');
		return false;
	});

	$('.dataTables_filter input').on('keyup', function(e) {
		stopEvent(e);
		if($(this).val().length == 0){
			$('.clear_search').hide();
			$('.dataTables_filter input').css('background','none');
		} else {
			$('.clear_search').show();
			$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
		}
		return false;
	});


	// Load inner table
	var anOpen = [];
	var link, icon, nDetailsRow, oldRow, nTr;

	$('#data_table').on('mousedown', '.expand', function (e) {

		stopEvent(e);
        clearMessages();

    	oldRow = nTr;
    	oldLink = link;
    	oldIcon = icon;

        link = $(this);
        icon = $(this).prev();	
		nDetailsRow = null;

		// COLLAPSE THE OLD ROW //
    	if(oldRow) {	
			$('div.innerDetails',  oldRow[0]).slideUp(function () {
				oldIcon.removeClass('icon-spinner icon-mirrored icon-spin').removeClass('icon-minus');
				oldIcon.addClass('icon-plus'); 
				oldLink.text(' Expand');
			  	oOuterTable.fnClose(oldRow);
			  	anOpen.shift();
			  	$('.delete, .edit', '#radio_table').show();
			});	
		}

		nTr = $(this).closest('tr')[0];
		var i = $.inArray(nTr, anOpen);
		var aData = oOuterTable.fnGetData(nTr);

		oSchedule = {};
		oSchedule.schedule_id = aData[2].trim();			
		oSchedule.description = aData[3].trim();

		// EXPAND THE NEW ROW //
		if(nTr !== oldRow || i === -1) {
			icon.removeClass('icon-plus');
			icon.addClass('icon-spinner icon-mirrored icon-spin'); 

			var params = getParameters('schedule/groups', {schedule_id: oSchedule.schedule_id});
    		ajaxRequest(params, function(response) {

		 		nDetailsRow = oOuterTable.fnOpen(nTr, response, 'details');

		 		oInnerTable = $('#data_table_inner').dataTable({
			        "bDestroy"	: true,
			        "bPaginate" : false,
			        "sDom"		: '',
					"aoColumns"	: [ 
						/* ID */ 		{ "sWidth": "5%" },
						/* Title */ 	{ "sWidth": "94%" },
						/* Delete */ 	{ "sWidth": "1%", "bSortable": false }
					]						 			
		 		});    	
		 		
		 		$('.delete, .edit', '#radio_table').hide();
		 		$('div.innerDetails', nDetailsRow).slideDown(); 
				icon.removeClass('icon-spinner icon-mirrored icon-spin');
				icon.addClass('icon-minus');						
				link.text(' Collapse');
				anOpen.push(nTr);
			});

		} else {
			$('div.innerDetails', $(oldRow).next()[0]).slideUp( function () {
				anOpen.shift();
				oOuterTable.fnClose(nTr);
			});
		}

	    return false;
	});	

    // Select2 the DataTable 'records per page' select element
    $('div.dataTables_length select').select2({width:60}); 


	/*************************
		LINK GROUP EVENT
	**************************/
	$('#data_table').on('mousedown', '.link-group', function() {
		var params = getParameters('schedule/unassociated', { schedule_id: oSchedule.schedule_id });

	    ajaxRequest(params, function(response) {   
	        var result = JSON.parse(response);
	        if(result.error) {
	            showError(result.error.message);
	        } else {
	        	showAddModal(result.data, '<i class="icon-link"></i>&nbsp;Link a group to this schedule', 'javascript:linkGroup()');
	        }
	        return false;
	    });		
	});


	/*************************
		UNLINK GROUP EVENT
	**************************/
	$('#data_table').on('mousedown', '.unlink-group', function(e) {

		var nRow = $(this).closest('tr')[0];
		var aData = oInnerTable.fnGetData(nRow);	

	    oSchedule.row = nRow;
	    oSchedule.radiogroup_id = aData[0].trim();
	    oSchedule.radiogroup_description = aData[1].trim().length == 0 ? 'Unknown' : aData[1].trim(); 
	    
	    var message = 'Are you sure you want to unlink the group "<b>' + oSchedule.radiogroup_description + '</b>"?<br>';
	    	message += '<span style="margin:10px;line-height:150%;"><small>Unlinking the group from this schedule will take radios in the group off of this schedule!</small><span>';

	    oSchedule.message = message;	
	    oSchedule.callback = 'javascript:unlinkGroup()';
	    showUnlinkModal(oSchedule);	

	});

	/*************************
		DELETE SCHEDULE EVENT
	**************************/
	$('#data_table').on('mousedown', '.delete-schedule', function(e) {

		var nRow = $(this).closest('tr')[0];
		var aData = oOuterTable.fnGetData(nRow);	

	    oSchedule.row = nRow;
	    oSchedule.schedule_id = aData[2].trim();
	    oSchedule.description = aData[3].trim(); 
	    oSchedule.message = 'Are you sure you want to delete the schedule "<b>' + oSchedule.description + '</b>"?';
	    oSchedule.callback = 'javascript:deleteSchedule()';
	    showRemoveModal(oSchedule);	

	});	

    // Handle enter key to save an update
    $('#data_table').on('keypress', 'input[type="text"]', function(e) {
        if(e.which == 13) {
            $('.edit-schedule', $(this).closest('tr')).mousedown();
        }
    });  	
});

/*************************
	DELETE SCHEDULE
**************************/
function deleteSchedule() {

    setStatus('Sending the data to the server');
    
    var params = getParameters('schedule/delete', { schedule_id: oSchedule.schedule_id });
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            oOuterTable.fnDeleteRow(oSchedule.row);
            showSuccess('Deleted the schedule "' + oSchedule.description + '"');
        }

        hideRemoveModal();
        setStatus('Done sending the data to the server');
    });
}

/*************************
		ADD GROUP
**************************/
function linkGroup() {

    setStatus('Sending the data to the server');
	oSchedule.radiogroup_id = $('#lst-entity-add').select2('data').id;
	oSchedule.radiogroup_description = $('#lst-entity-add').select2('data').text;
	
    var params = getParameters('schedule/associate', {radiogroup_id: oSchedule.radiogroup_id, schedule_id: oSchedule.schedule_id});
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            showSuccess('Associated the "' + oSchedule.radiogroup_description + '" group to the "' + oSchedule.description + '" schedule!');
            $('#data_table_inner').dataTable().fnAddData( [
		        oSchedule.radiogroup_id,
		        oSchedule.radiogroup_description,
		        '<span class="unlink-group" style="cursor:pointer;color:#0D638F;"><i class="icon-unlink"></i></span>' ] 
		    );
		    $('div.dataTables_length select').select2({width:60}); 
		}
    });
    
    hideAddModal();
    setStatus('Done sending the data to the server');
}

/*************************
		UNLINK GROUP
**************************/
function unlinkGroup() {
    setStatus('Sending the data to the server');

    var params = getParameters('schedule/disassociate', { radiogroup_id: oSchedule.radiogroup_id, schedule_id: oSchedule.schedule_id });
    
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
        	oInnerTable.fnDeleteRow(oSchedule.row);
            showSuccess('The group "' + oSchedule.radiogroup_description + '" was unlinked from the schedule "' + oSchedule.description + '"');
        }
    });
    
    hideUnlinkModal();
    setStatus('Done sending the data to the server');
}