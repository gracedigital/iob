var oSchedule = {};

$(document).ready(function() {

    /**
        PAGE INITALIZATAION
    **/

    Schedule.clear();

    // fix to be able to hide page alert
    $('button.close').on('click', function(e) {
      $(this).parent().hide();
      return false;
    });

    // set active tab
    $(".tab-pane:first").addClass('active');

    // initalize channel selector
    $('select[name="stn"]').select2({ allowClear: true });

    // initalize group selector
    $('#groupList').select2({ allowClear: true });

    // initalize volume selector
    $("input[name=vol]").pcntspinner({ min: 0, max: 100, step: 1 });

    // initialize time selector
    $('.timepicker-default').timepicker({ minuteStep: 5});

    $('.tab-pane').each(function() {  
        $('.timepicker-default:first', $(this)).timepicker('setTime', '07:00 AM').attr('value','07:00 AM');
        $('.timepicker-default:last', $(this)).timepicker('setTime', '12:00 AM').attr('value','12:00 AM');
    });

    $('#schedule_name').pulsate({ color:"#35aa46",repeat: 2 });
    $('#schedule_name').focus();

    // cache active tab html - strip line breaks
    oSchedule.html = $.trim($('.tab-pane.active').html().replace( new RegExp( "\>[\n\t ]+\<" , "g" ) , "><" ));


    /**
        PAGE EVENT HANDLERS 
    **/

    // $('.tab-pane.active').on('select2-close', 'select.select2', function() {
    //     $(this).prev().removeClass('alert alert-error');
    // });

    $('body').on('change', ':input[name=vol]', function(e) {
        $(this).attr('value', $(this).val());
    });

    // show volume as percentage 
    $('body').on('change', 'input[type=number]', function(){
        $('.volume-percent', $(this).parent()).text(($(this).val()*5)+'%');
    });

	// handle form cancellation
    $('#cancel').on('click', function () {
        clearLoading();
        setStatus('Resetting the entire schedule');
    	resetSchedule();
    });

    // handle form submission
    $('#submit').on('click', function () {

        clearStatus();
        clearMessages();
        clearFormErrors();

    	$('#status-bar').show();
        $('#submit-icon').hide();
    	$('#loading-icon').show();

        if (!isValidSchedule()) {
            clearLoading();                    
            return false;
        }

        Schedule.description = $('#schedule_name').val();
        Schedule.note = moment().format("YYYY-DD-MM hh:mm:ss a");
        if (!isEmpty($('#groupList').select2('data').text)) {
            Schedule.groupId = $('#groupList').select2('data').id;
            Schedule.groupName = $('#groupList').select2('data').text;
        }

        $('.tab-pane').each(function(index) {
    		// get data for each day parts / events
    		$('table#day-parts > tbody > tr', $(this)).each(function(rowIndex) {
                var event = {};
                var standardTime = $(this).find('input[name="event_time"]').attr('value');
                event['event_time'] = convertToMilitaryTime(standardTime);
                event['stn'] = $(this).find('select[name="stn"]').select2('data').id;
                event['vol'] = $(this).find('input[name="vol"]').pcntspinner('value');
                event['sort'] = rowIndex;

                Schedule.addEvent(index, event);
    		});
    	});


        setStatus('Sending the data to the server');
        var params = getParameters('schedule/add_events', Schedule.serialize());
        ajaxPost(params, function(response) {
            var result = JSON.parse(response);
            if (result.error) {
                showError(result.error.message);
            } else {
                showSuccess(getSuccessMessage(Schedule.description, result));
                resetSchedule();
            }

            setStatus('Done sending the data to the server');
            $('#loading-icon').hide();
            $('#submit-icon').show();
            return false;
        });
    });

    // Copy the active tab to rest of week
    $("#btn-copy-schedule").on('mousedown', function () {

        clearMessages();
        clearFormErrors();

        // set the dom values for these controls - workaround
        // $('.timepicker-default', $(".tab-pane.active")).each(function() {
        //     $(this).attr('value', $(this).val());
        // });

        oSchedule.activeTab =  $('.tab-pane.active').attr('id').slice(-1);

        // validate active tab before copying
        try {
            if ( (!isValidScheduleTimes($(".tab-pane.active"), oSchedule.activeTab)) || (!isValidScheduleChannels($(".tab-pane.active"), oSchedule.activeTab)) ) {
                $(':checkbox').prop('checked', false);
                $.uniform.update(':checkbox');
                $('.tab-pane:first').addClass('active');
                return false;
            }
        } catch(error) {
            setStatus('The schedule is invalid'); 
            showError(error);
            clearLoading();
            return false;        
        }

        // destroy channel and volume controls to clear their DOM changes
        $('.tab-pane.active').find('select').select2("destroy");
        $('.tab-pane.active').find('input[name=vol]').pcntspinner("destroy");

        // cache the active tab data
        var html = $.trim($('.tab-pane.active').html().replace(new RegExp( "\>[\n\t ]+\<" , "g" ) , "><" ));

        // restore
        $('.tab-pane.active').find('select').select2();
        $('.tab-pane.active').find('input[name=vol]').pcntspinner({ min: 0, max: 100, step: 1 });  

        // cache selected channels - because they don't copy over
        oSchedule.activeChannels = [];
        $('select.select2', $('.tab-pane.active')).each(function(index) {
            if ($(this).select2('val')) {
                oSchedule.activeChannels[index] = $(this).select2('val');    
            }
        });

        // cache selected volumes - because they don't copy over
        oSchedule.activeVolumes = [];
        $('input[name=vol]', $('.tab-pane.active')).each(function(index) {
            oSchedule.activeVolumes[index] = $(this).pcntspinner('value');    
        });

        oSchedule.inital = [];

        // cache schedule of each dow
        $('.tab-pane').not('.active').each(function(index) {

            var day = $(this).attr('id').slice(-1);
            oSchedule.inital[day] = $(this).html();

            // set new schedule
            $(this).empty().append(html);
            
            // restore select2 and assign value
            $('table#day-parts > tbody > tr', $(this)).each(function(index) {
                $('select.select2', this).select2().select2('val', oSchedule.activeChannels[index]);
                $('input[name=vol]', this).pcntspinner().pcntspinner('value', oSchedule.activeVolumes[index]);           
            });
        });


        // update dow elements
        $('.tab-pane').each(function(index) {   
            $(':input[name=day_of_week]', $(this)).val(index);
        });

        setStatus('Finished copying the schedule');
        showSuccess('Copied schedule for ' + daysofweek[oSchedule.activeTab] + ' to the other days of the week!');

        clearStatus();
        return false;

    });

    var isValidSchedule = function() {
        var dow;
        try {
            $('.tab-pane').each(function(index) {
                dow = $(this).attr('id').slice(-1);
                // if (isValidScheduleName() && isValidScheduleGroup() && isValidScheduleTimes($(this), dow) && isValidScheduleChannels($(this), dow)) {
                if (isValidScheduleName() && isValidScheduleTimes($(this), dow) && isValidScheduleChannels($(this), dow)) {
                    setStatus('The schedule is valid');
                }
            });
        } catch(error) {
            setStatus('The schedule is invalid');
            $('#tab_' + dow).click();
            showError(error);
            clearLoading();
            return false;            
        }
        return true;
    }  
     
    var isValidScheduleName = function() {

        var element = $('#schedule_name');
        if (isEmpty(element.val())) {
            element.css('border-color','#b94a48').focus();
            throw 'You must provide a schedule name!';
        } 

        element.parent().parent().removeClass('error');
        $('span:last', element.parent()).hide();
        return true;
    }

    var isValidScheduleGroup = function() {
        if (isEmpty($('#groupList').select2('data').text)) {
            $('#groupList').addClass('error-border').focus();
            throw 'You need to select a group for this schedule!';
        }
        return true;
    }

    
    var isValidScheduleTimes = function (tab, dow) {

        setStatus('Validating times for ' + daysofweek[dow]);
        var pickers = $('.timepicker-default', tab);

        for (var index = 0; index < pickers.length; index++) {
            // check for empty event time
            var element = $(pickers[index]);
            if (isEmpty(element.val())) {    
                $('#tab_' + dow).click();
                element.parent().parent().addClass('error').focus();
                throw 'You must choose a time for this event!'
            } 

            // check for earlier time
            if (index > 0) {
                var curTime = $(pickers[index]).val();
                var prevTime = $(pickers[index-1]).val();
               
                if (curTime == prevTime) {
                    $('input:checkbox').removeAttr('checked');
                    element.parent().parent().addClass('error');
                    throw 'There is an invalid event time ' + daysofweek[dow] + '! Two consecutive events may not have the same start time.';
                }
            }
        }
        return true;
    }

    var isValidScheduleChannels = function(tab,dow) {
        
        setStatus('Validating channels for ' + daysofweek[dow]);
        var selects = $('.select2', tab);

        for (var index = 0; index < selects.length; index++) {
            var element = $(selects[index]);
            if (isEmpty(element.select2('data').text)) {
                $('#tab_' + dow).click();
                element.addClass('alert alert-error');
                throw 'You need to select a channel for daypart "' + $('.daypart',element.closest('tr')).val() + '" on ' + daysofweek[dow];
            }
        }
        return true;
    }


    var resetSchedule = function() {
        $('.tab-pane').each(function( index ) {
            Schedule.clear();
            $('#schedule_name').val('');
            $('#groupList').select2('data', null);
            $(this).html(oSchedule.html);
            // uncheck
            $(':checkbox').prop('checked', false);
            $.uniform.update(':checkbox');
            // reset controls - have to, because these controls suck
            $('.timepicker-default:first', $(this)).val('07:00 AM');
            $('.timepicker-default:last', $(this)).val('12:00 AM'); 
            $('select', $(this)).select2();
            $('div.select2-container:first', $(this)).remove();
        });
        clearFormErrors();
        $('#tab_0').click();
    }

    var clearLoading = function() {
        $('#loading-icon').hide();
        $('#submit-icon').show();
    }

    var clearFormErrors = function() {
        $('.select2-container').removeClass('alert alert-error');
    }

    var clearStatus = function() {
        setTimeout(function(){
            $('#status-msg').text('');
            $('#status-bar').slideUp('slow');
        }, 5000);
    }

    var setStatus = function(message) {
        $('#status-bar').slideDown();
        $('#status-msg').text(message + '...');
    }

    var getPlural = function(count) {
        return (count == 1) ? 'event' : 'events';
    }

    var getSuccessMessage = function(description, result) {
        var msg = 'The schedule "' + Schedule.description + '" was saved.  There were (' + result.added + ') ' + getPlural(result.added) + ' added.';
        return msg;
    }

    var isEmpty = function(obj) {
        if (typeof obj == 'undefined' || obj === null || obj === '' || obj == 'undefined' || obj == '[]') return true;
        if (typeof obj == 'number' && isNaN(obj)) return true;
        if (obj instanceof Date && isNaN(Number(obj))) return true;
        return false;
    }

});

