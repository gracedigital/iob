$(document).ready(function() { 

	// Hide page alert div
	$('button.close').on('click', function(e) {
	  	$(this).parent().hide();
	  	return false;
	});

	$('#btn-save').on('click',function(e) {
		stopEvent(e);
		doSubmit();
	});

	$('#btn-cancel').on('click',function(e) {
		loadContent('dashboard/view'); 
	});

	$('#lst-group').select2({
		placeholder: 'All credentials must be assigned to a group', 
		allowClear: true,
		width: '335'
	});

	/* Functions */					
	var doSubmit = function () {
	    clearMessages();
	    if(!isValidForm()) {
	    	return false;
	    }

		var params = getParameters('credential/register', {
			sirius_userdetail_username: $('#username').val().trim(), 
			radiogroup_id: $('#lst-group').select2('data').id,
			description: $('#description').val(),
			check: true
		});

      	ajaxRequest(params, function(response) {
			var result = JSON.parse(response);
			if(result.error) {          	  
				showError('There was an error provisioning these credential ' + result.error.message);    
			} else {
				showSuccess('The credentials "' + $('#username').val() + '" were successfully provisioned');
				$('#btn-submit').hide();
				$("#btn-repeat").show();
			}
			return false;                
		});

	}

	var isValidForm = function() {
		var fields = ['username', 'description'];
	    for(var i=0;i<fields.length;i++) {
		    if(parseInt($('#'+fields[i]).val().length) < 1) {
		    	showError(fields[i].charAt(0).toUpperCase() + fields[i].slice(1) + ' is a required field!');
		    	$('#'+fields[i]).focus().pulsate({color:'#E02222',repeat:2});
		    	return false;
		    }
		}

	    if(isEmpty($('#lst-group').select2('data').id)){
	    	showError('You must select a radio group to associate to these credentials!');
	    	$('.select2-container').pulsate({color:'#E02222',repeat:2});
	    	return false;
	    }
	    return true;
	}

});