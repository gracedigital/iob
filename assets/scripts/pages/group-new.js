$(document).ready(function() {   

  // Fix to be able to hide page alert
  $('button.close').on('click', function(e) {
    $(this).parent().hide();
    return false;
  });

  $('#btn-cancel').on('mousedown', function () { 
    clearMessages();
    $('#desc').val('');
    $('#note').val('');
  });

  $('#btn-save').on('mousedown', function () { 
    clearMessages();

    if($('#desc').val().length === 0) {
      showError('You must enter a name for this group.');
      $('#desc').focus();
      return false;
    }

    var params = getParameters('group/add', { description: $('#desc').val(), note: $('#note').val(), obsolete: false });
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {            
          showError(result.error.message);                    
        } else {
          showSuccess('The group "'+ $('#desc').val() +'" has been created');
          $('form').trigger('reset');          
        }
        return false;
    });

  });
  
});