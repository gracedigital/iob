var oEntity = {};

$(document).ready(function() {   

  // Initalize Dropdown
  $('#lst-folders').select2({
      allowClear: true,
  });

  // Create Channel Tab Selected
  $('#tab-channel').on('mousedown', function(e) {
      $('#portlet-title').text('Add New Channel');
      $('#tab-folder').css('color', '#fff;');
      $(this).css('color', '#0d638f');
      $('#chnl-title').focus();
       oEntity.type = 'channel';
  });  

  // Create Folder Tab Selected
  $('#tab-folder').on('mousedown', function(e) {
      $('#portlet-title').text('Add New Lineup');
      $('#tab-channel').css('color', '#fff;');    
      $(this).css('color', '#0d638f');
      $('#fldr-title').focus();
      oEntity.type = 'folder';
  });  

  $('#tab-folder, #tab-channel').on('mouseup', function() {
      if(oEntity.element) {
        oEntity.element.css('border-color', '#E5E5E5');
      }
      clearMessages();
  });

  // Handle enter key for the form
  $('#add_channel, #add_folder').on('keypress', 'input', function(e) {
      if(e.which == 13 && $('.btn.green').is(":enabled")) {
          $('#btn-chnl-save').mousedown();
      }
  });


  // Handle Save
  $('.green').on('mousedown', function(e) {
    
    console.log('lineup-new:.green_mousedown triggered @ ' + moment().format("hh:mm:ss a"));

    stopEvent(e);    
    clearMessages();
    var input = {};

    if($('form:visible').attr('id') === 'add_channel') {

        if(!isValidChannel()) {
            return false;
        }

        input.parent_id = '0';
        input.title = $('#chnl-title').val().trim();
        input.note = $('#chnl-note').val().trim();
        input.folder = $('#chnl-folder').val().trim();
        input.stream_url = $('#chnl-url').val().trim();
        input.list_id = $('#lst-folders').select2('data').id;

        var params = getParameters('channel/add', input);

        ajaxRequest(params, function(response) {
            var result = JSON.parse(response);
            if(result.error) {
              showError(result.error.message);
            } else {
              showSuccess(getSuccessMessage());
              $('form:visible')[0].reset();
              $('select').select2('data', null);
            }
            return false;
        });

    } else {

        if(!isValidFolder()) {
            return false;
        }

        input.note = $('#fldr-note').val(); 
        input.title = $('#fldr-title').val();
        input.email = $('#fldr-email').val(); 

        var params = getParameters('folder/add', input);

        ajaxRequest(params, function(response) {

            var result = JSON.parse(response);
            if(result.error) {
              showError(result.error.message);
            } else {
              // Add new folder to folder list on channel tab
              var options = $('#lst-folders').html() + '<option value="' + result.list_id +'">'+ input.title + '</option>';
              $('#lst-folders').empty().append(unescape(options));
              $('#lst-folders').select2();

              showSuccess(getSuccessMessage());
              $('form:visible')[0].reset();
            }
            return false;
        });
    }

    console.log('lineup-new:.green_mousedown exited @ ' + moment().format("hh:mm:ss a"));
    return false;

  });
  
    // Handle cancel button
    $('.cancel').on('mousedown', function(e) {
      $('form:visible')[0].reset();
    });


    // Validate Folder
    var isValidFolder = function() {
        try {
              if(isValidFolderName()) {
                  setStatus('The lineup is valid');
              }
        } catch(error) {
            setStatus('The lineup is invalid');
            showError(error);
            clearLoading();
            return false;            
        }
        return true;
    }  
     
    var isValidFolderName = function() {
        oEntity.element = $('#fldr-title');
        if(isEmpty(oEntity.element.val())) {
            oEntity.element.css('border-color','#b94a48').focus();
            throw 'You must provide a lineup name!';
        } 
        return true;
    }

    // Validate Channel
    var isValidChannel = function() {
        try {
            if(isValidChannelName() && isValidChannelURL() && isValidChannelLineup()) {
                setStatus('The channel is valid');
            }
        } catch(error) {
            setStatus('The channel is invalid');
            showError(error);
            clearLoading();
            return false;            
        }
        return true;
    }  

    var isValidChannelName = function() {
        oEntity.element = $('#chnl-title');
        if(isEmpty(oEntity.element.val())) {
            oEntity.element.css('border-color','#b94a48').focus();
            throw 'You must provide a channel name!';
        } 
        return true;
    }

    var isValidChannelURL = function() {
        oEntity.element = $('#chnl-url');
        if(isEmpty(oEntity.element.val())) {
            oEntity.element.css('border-color','#b94a48').focus();
            throw 'You must provide a valid URL for this channel!';
        } 
        return true;
    }

    var isValidChannelLineup = function() {
        oEntity.element = $('#lst-folders');
          if(isEmpty(oEntity.element.select2('data')) || isEmpty(oEntity.element.select2('data').text)) {
              oEntity.element.addClass('error-border').focus();
              throw 'You need to assign this channel to a lineup!';
          }
        return true;
    }  

    // Fix to be able to hide page alert
    $('button.close').on('mousedown', function(e) {
      $(this).parent().hide();
      return false;
    });    

    // Handle enter key to save the form
    $('#add_channel, #add_folder').on('keypress', 'input[type="text"], textarea', function(e) {
        if(e.which == 13) {
            $('button.green', $('form:visible')).mousedown();
        }
    });          

});

function getSuccessMessage() {
  var msg = '';
  if($('form:visible').attr('id') === 'add_channel') {
    msg = 'Channel "<b>' + $('#chnl-title').val() + '</b>" was sucessfully added to the "' + $('#lst-folders').select2("data").text + '" folder'
  } else {
    msg = 'Folder "<b>' + $('#fldr-title').val() + '</b>" was sucessfully added';
  }
  return msg;
}