var OuterTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oOuterTable.fnUpdate(aData[i], nRow, i, false);
                }

                $('#th_group_delete').empty().append('Delete');
                oOuterTable.fnDraw();
            }

            function editRow(oOuterTable, nRow) {
                var aData = oOuterTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);              
                jqTds[3].innerHTML = '<input type="text" class="m-wrap" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<input type="text" class="m-wrap" value="' + aData[4] + '">';
                jqTds[5].innerHTML = '<span class="edit-group" style="cursor:pointer;color:#0D638F;">Save</span>';
                jqTds[6].innerHTML = '<span class="cancel-group" style="cursor:pointer;color:#0D638F;">Cancel</span>';
            }

            var saveRow = function(oOuterTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oOuterTable.fnGetData(nRow);    

                // Set the group properties for update
                Group.prepareFor('update', nRow, aData, jqInputs); 

                var params = Group.parameterizeFor('update');
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else {
                        oOuterTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                        oOuterTable.fnUpdate(jqInputs[1].value, nRow, 4, false);
                        oOuterTable.fnUpdate('<span class="edit-group" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt" style="width:100%;"></i></span>', nRow, 5, false);
                        oOuterTable.fnUpdate('<span class="delete-group" style="cursor:pointer;color:#0D638F;"><i class="icon-remove" style="width:100%;"></i></span>', nRow, 6, false);
                        oOuterTable.fnUpdate('', nRow, 7, false);
                        oOuterTable.fnDraw();   
                        showSuccess('The group "' + Group.description + '" was updated!');
                    }
                }); 

                $('#th_group_delete').empty().append('Delete');
            }

            function cancelEditRow(oOuterTable, nRow) {
                var jqInputs = $('input', nRow);
                oOuterTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oOuterTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oOuterTable.fnUpdate('<span class="edit-group" style="cursor:pointer;color:#0D638F;">Edit</span>', nRow, 5, false);
                oOuterTable.fnDraw();
                return false;
            }

            var nEditing = null;

            /********************
                EDIT CHANNEL
            ********************/
            $('#data_table').on('mousedown', '.edit-group', function (e) {

                stopEvent(e);
                $('#th_group_delete').empty().append('');

                /* Get the row as a parent of the link that was mousedowned on */
                var nRow = $(this).closest('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oOuterTable, nEditing);
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oOuterTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oOuterTable, nRow);
                    nEditing = nRow;
                }

                return false;
            });         

            /********************
                CANCEL EDIT
            ********************/
            $('#data_table').on('mousedown', '.cancel-group', function (e) {
                stopEvent(e);

                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oOuterTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oOuterTable, nEditing);
                    nEditing = null;
                }
                return false;
            });

            /********************
                DELETE GROUP
            ********************/
            $('#data_table').on('mousedown', '.delete-group', function (e) {
                var nRow = $(this).parents('tr')[0];
                var aData = oOuterTable.fnGetData(nRow);
                Group.prepareFor('delete', nRow, aData, null);
                showRemoveModal(Group);
            });


            // Handle enter key to save an update
            $('#data_table').on('keypress', 'input[type="text"]', function(e) {
                if(e.which == 13) {
                    $('.edit-group', $(this).closest('tr')).mousedown();
                }
            });  
        }

    };

}();