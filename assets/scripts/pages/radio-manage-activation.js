
// Handle Radio Deactivation
$('#data_table').on('mousedown', '.icon-unlock', function (e) {
	var nRow = $(this).closest('tr')[0];
	var aData = oOuterTable.fnGetData(nRow);
    Radio.prepareFor('deactivate', nRow, aData, $(this));
	showActivateModal(Radio);
});

// Handle Radio Activation
$('#data_table').on('mousedown', '.icon-lock', function (e) {
	var nRow = $(this).closest('tr')[0];
	var aData = oOuterTable.fnGetData(nRow);
    Radio.prepareFor('activate', nRow, aData, $(this));
	showActivateModal(Radio);
});

function deactivateRadio() {

	var params = getParameters('radio/deactivate', { serial: Radio.serial });
	ajaxRequest(params, function(response) {
	    var result = JSON.parse(response);
	    if(result.error) {
	        showModalError(result.error.message);
	    } else {
            showSuccess("The radio was successfully deactivated!");      
            Radio.element.removeClass('icon-unlock').addClass('icon-lock inactive-radio');
    		Radio.element.children(1).text(' Inactive');	                         
    	}
    });

    hideActivateModal();
}

function activateRadio() {

	var params = getParameters('radio/activate', { serial: Radio.serial });
	ajaxRequest(params, function(response) {
	    var result = JSON.parse(response);
	    if(result.error) {
	        showModalError(result.error.message);
	    } else {
            showSuccess("The radio was successfully activated!");
            Radio.element.removeClass('icon-lock inactive-radio').addClass('icon-unlock');
    		Radio.element.children(1).text(' Active');                     
    	}
    });

    hideActivateModal();
}