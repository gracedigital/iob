var InnerTable = function () {

    return {

        //main function to initiate the module
        init: function () {
            function restoreRow(oInnerTable, nRow) {
                var aData = oInnerTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oInnerTable.fnUpdate(aData[i], nRow, i, false);
                }
                $('#th_channel_delete').empty().append('Delete');
                oInnerTable.fnDraw();
            }

            function editRow(oInnerTable, nRow) {
                var aData = oInnerTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                jqTds[2].innerHTML = '<input type="text" class="m-wrap large" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="m-wrap large" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<span class="edit-channel" style="cursor:pointer;color:#0D638F;">Save</span>';
                jqTds[5].innerHTML = '<span class="cancel-channel" style="cursor:pointer;color:#0D638F;">Cancel</span>';
            }

            function saveRow(oInnerTable, nRow) {

                // Get input form elements from row
                var jqInputs = $('input', nRow);
                var aData = oInnerTable.fnGetData(nRow);

                // Set the channel properties for update
                Channel.prepareFor('update', nRow, aData, jqInputs);
                
                var params = Channel.parameterizeFor('update');
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {   
                        showError(result.error.message);
                    } else { 
                        showSuccess('The channel "' + Channel.title + '" was updated!');
                    }
                });

                oInnerTable.fnUpdate(jqInputs[0].value, nRow, 2, false);
                oInnerTable.fnUpdate(jqInputs[1].value, nRow, 3, false);
                oInnerTable.fnUpdate('<span class="edit-channel" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt" style="width:100%;"></i></channel>', nRow, 4, false);
                oInnerTable.fnUpdate('<span class="delete-channel" style="cursor:pointer;color:#0D638F;"><i class="icon-remove"></i></span>', nRow, 5, false);
                oInnerTable.fnDraw();    

                return false;                
            }

            function cancelEditRow(oInnerTable, nRow) {
                var jqInputs = $('input', nRow);
                oInnerTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oInnerTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oInnerTable.fnUpdate('<span class="edit-channel" style="cursor:pointer;color:#0D638F;">Edit</span>', nRow, 5, false);
                oInnerTable.fnDraw();
            }

            var nEditing = null;

            /********************
                CANCEL EDIT
            ********************/
            $('#data_table').on('mousedown', '.cancel-channel', function (e) {
                stopEvent(e);

                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oInnerTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oInnerTable, nEditing);
                    nEditing = null;
                }
            });

            /********************
                EDIT CHANNEL
            ********************/
            $('#data_table').on('mousedown', '.edit-channel', function (e) {

                $('#th_channel_delete').empty().append('');

                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).closest('tr')[0];
                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oInnerTable, nEditing);
                    editRow(oInnerTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oInnerTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oInnerTable, nRow);
                    nEditing = nRow;
                }
            });

            /********************
                DELETE CHANNEL
            ********************/
            $('#data_table').on('mousedown', '.delete-channel', function(e) {
                stopEvent(e);
                var nRow = $(this).closest('tr')[0];
                var aData = oInnerTable.fnGetData(nRow);
                Channel.prepareFor('delete', nRow, aData, null);
                showRemoveModal(Channel);
            });      

            // Handle enter key to save an update
            $('#data_table_inner').on('keypress', 'input[type="text"]', function(e) {
                if(e.which == 13) {
                    $('.edit-channel', $(this).closest('tr')).mousedown();
                }
            });              
        }
    };

}();