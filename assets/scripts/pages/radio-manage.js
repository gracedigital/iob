var oOuterTable = null;

$(document).ready(function() {		

	OuterTable.init();
	clearMessages();

	// Initialize the outer table
    oOuterTable = $('#data_table').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
		"sDom": '<"headwrap"lf<"clear_search">>rtip',
      	"fnDrawCallback": function( oSettings ) {
        	if($('.dataTables_filter input').val().length >0) {
          		$('.clear_search').show();
          		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
        	}
        },
		"aoColumns": [ 
		    /* counter */   { "sWidth": "01%", "bSortable": false },
			/* Details */   { "sWidth": "7%", "bSortable": false },
			/* Status */    { "sWidth": "10%", "bSortable": true },
			/* Serial */ 	{ "sWidth": "10%", "bSortable": true },
			/* Name */ 		{ "sWidth": "25%", "bSortable": true },
			/* Group */		{ "sWidth": "*", 	"bSortable": true },
			/* Edit */ 		{ "sWidth": "01%", "bSortable": false },
			/* Delete */	{ "sWidth": "01%", "bSortable": false },
			/* Empty */ 	{ "sWidth": "01%", "bSortable": false }
		]       		
    });	  

    // DataTables filter clear
	$('body').on('mousedown', '.clear_search', function(e) {
		stopEvent(e);
		$('.dataTables_filter input').val('');
		oOuterTable.fnFilterClear();
		$('.clear_search').hide();
		$('.dataTables_filter input').css('background','none');
		return false;
	});

	$('.dataTables_filter input').on('keyup', function(e) {
		stopEvent(e);
		if($(this).val().length == 0){
			$('.clear_search').hide();
			$('.dataTables_filter input').css('background','none');
		} else {
			$('.clear_search').show();
			$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
		}
		return false;
	});

	// DataTables add search filter
	var filter = getParameterByName('rt').split('/')[2];
	if(filter) {	
		filter = filter.charAt(0).toUpperCase() + filter.slice(1);
		oOuterTable.fnFilter(filter,null,false,true,true,false);
		$('.dataTables_filter input').css('background','url(http://i42.tinypic.com/z7qdv.png) no-repeat scroll 98% 50% transparent');
	}
	

	// Load inner table
	var anOpen = [];
	var link, icon, nDetailsRow, oldRow, nTr;

	$('#data_table').on('mousedown', '.expand', function (e) {

		stopEvent(e);
        clearMessages();

    	oldRow = nTr;
    	oldLink = link;
    	oldIcon = icon;

        link = $(this);
        icon = $(this).prev();	
		nDetailsRow = null;

		// COLLAPSE THE OLD ROW //
    	if(oldRow) {	
			$('div.innerDetails',  oldRow[0]).slideUp(function () {
				oldIcon.removeClass('icon-spinner icon-mirrored icon-spin').removeClass('icon-minus');
				oldIcon.addClass('icon-plus'); 
				oldLink.text(' Expand');
			  	oOuterTable.fnClose(oldRow);
			  	anOpen.shift();
			  	$('.delete, .edit', '#radio_table').show();
			});	
		}

		nTr = $(this).closest('tr')[0];
		var i = $.inArray(nTr, anOpen);
		var aData = oOuterTable.fnGetData(nTr);

		Radio.reset();
		Radio.serial = aData[3].trim();
		Radio.title = aData[4].trim();

		console.log("RADIO:");
		console.log(Radio);

		// EXPAND THE NEW ROW //
		if(nTr !== oldRow || i === -1) {
			icon.removeClass('icon-plus');
			icon.addClass('icon-spinner icon-mirrored icon-spin'); 

			var params = Radio.parameterizeFor('folders');
    		ajaxRequest(params, function(response) {

		 		nDetailsRow = oOuterTable.fnOpen(nTr, response, 'details');

		 		oInnerTable = $('#data_table_inner').dataTable({
			        "bDestroy"	: true,
			        "bPaginate" : false,
			        "sDom"		: '',
					"aoColumns"	: [ 
						/* ID */ 		{ "sWidth": "7.5%" },
						/* Title */ 	{ "sWidth": "90%" },
						/* Delete */ 	{ "sWidth": "3%", "bSortable": false },
						/* Empty */ 	{ "sWidth": "1%", "bSortable": false }
					]						 			
		 		});    	
		 		
		 		$('.delete, .edit', '#radio_table').hide();
		 		$('div.innerDetails', nDetailsRow).slideDown(); 
				icon.removeClass('icon-spinner icon-mirrored icon-spin');
				icon.addClass('icon-minus');						
				link.text(' Collapse');
				anOpen.push(nTr);
			});

		} else {
			$('div.innerDetails', $(oldRow).next()[0]).slideUp( function () {
				anOpen.shift();
				oOuterTable.fnClose(nTr);
			});
		}

	    return false;
	});	

	/*************   EVENT HANDLERS   **************/

	// Fix to be able to hide page alert
	$('button.close').on('mousedown', function(e) {
	  	$(this).parent().hide();
	  	return false;
	});


	// Clear filter	
	$('#lnk-manage').on('mousedown', function(e) {
		oOuterTable.fnFilter('');
	});


    // Set title for activate/deactivate links
    $('.icon-lock').attr('title', 'Click to Activate');
    $('.icon-unlock').attr('title', 'Click to Deactivate');


    // Select2 the datatable 'records per page' select element
    $('div.dataTables_length select').select2({width:60});

    /*********************
	/ HANDLE UNLINK FOLDER
    /********************/
    $('#data_table').on('mousedown', '.unlink-folder', function() {
        var nRow = $(this).closest('tr')[0];
        var aData = oInnerTable.fnGetData(nRow);
        Radio.prepareFor('unlink', nRow, aData, null);
        showUnlinkModal(Radio);
    });     

    /*********************
    / HANDLE LINK FOLDER
    /********************/
	$('#data_table').on('mousedown', '.link-folder', function() {
		var params = getParameters('radio/unassociated_folders', { serial: Radio.serial });
	    ajaxRequest(params, function(response) { 
	        var result = JSON.parse(response);
	        if(result.error) {
	            showError(result.error.message);
	        } else {
	        	showAddModal(result, '<i class="icon-link"></i>&nbsp;Link a lineup to this radio', 'javascript:linkFolder()');
	        }
	        return false;
	    });		
	});

	
    /*********************
    / HANDLE DELETE RADIO
    /********************/
    $('#data_table').on('mousedown', '.delete-radio', function (e) {
        var nRow = $(this).parents('tr')[0];
        var aData = oOuterTable.fnGetData(nRow);
        Radio.prepareFor('delete', nRow, aData, null);
        showRemoveModal(Radio);
    });


    // Handle enter key to save an update
    $('#data_table').on('keypress', 'input[type="text"]', function(e) {
        if(e.which == 13) {
            $('.edit-radio', $(this).closest('tr')).mousedown();
        }
    });  	

});
	

/*********************
/ DELETE RADIO
/********************/
function deleteRadio() {

    setStatus('Removing the radio...');

    var params = getParameters('radio/unregister', { serial: Radio.serial });
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            oOuterTable.fnDeleteRow(Radio.row);
            showSuccess('Deleted the radio serial # "' + Radio.serial + '"');
        }
    });

    hideRemoveModal();
    setStatus('Done deleting the radio');
}

/*********************
/ UNLINK FOLDER
/********************/
function unlinkFolder() {

    setStatus('Sending the data to the server');

    var params = Radio.parameterizeFor('unlink');
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
        	oInnerTable.fnDeleteRow(Radio.row);
            showSuccess('The "' + Radio.description + '" lineup was unlinked from radio serial # ' + Radio.serial);
        }
    });
	  
    hideUnlinkModal();
    setStatus('Done processing the action');
    logMethod('channel-manage:unlinkChannel', 'exited');	
}

/*********************
/ LINK FOLDER
/********************/
function linkFolder() {

    setStatus('Sending the data to the server');
	Radio.list_id = $('#lst-entity-add').select2('data').id;
	Radio.list_name = $('#lst-entity-add').select2('data').text;
	
    var params = getParameters('radio/associate_folder', { serial: Radio.serial, list_id: Radio.list_id });
    ajaxRequest(params, function(response) {
        var result = JSON.parse(response);
        if(result.error) {
            showError(result.error.message);
        } else {
            showSuccess('The lineup "' + Radio.list_name + '" was linked to the radio "' + Radio.serial + '"');
            $('#data_table_inner').dataTable().fnAddData( [
		        Radio.list_id,
		        Radio.list_name,
		        '<span class="unlink-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-unlink"></i></span>','' ] 
		    );
		}
    });
    
    hideAddModal();
    setStatus('Done sending the data to the server');
}