var oOuterTable = null;

$(document).ready(function() {

	clearMessages();
	OuterTable.init();
	InnerTable.init();

	// Fix to be able to hide page alert
	$('button.close').on('click', function(e) {
	  $(this).parent().hide();
	  return false;
	});
	
	// Initialize the outer table
	oOuterTable = $('#data_table_outer').dataTable({
        "bDestroy": true,
        "bPaginate" : true,
		"aoColumns": [ 
			/* Id */ 		{"sWidth": "05%", "bSortable": true  },
			/* Details */   {"sWidth": "*", "bSortable": false },
			/* Username */ 	{"sWidth": "*", "bSortable": true  },
			/* Password */ 	{"sWidth": "*", "bSortable": false },
			
			
			/* Notes */ 	{"sWidth": "*", "bSortable": true  },	
			/* Children */ 	{"sWidth": "*", "bSortable": true  },	
			/* Edit */ 		{"sWidth": "05%", "bSortable": false },
			/* Empty */     {"sWidth": "05%", "bSortable": false }
			/* Delete 		{"bSortable": false, "sWidth": "5%"} */						    
		]         
	});

	// Load inner table
	var anOpen = [];
	$('#data_table_outer').on('mousedown', '.expand-users', function () {

        clearMessages();
        var link = $(this);
        var icon = $(this).prev();	
	   	var nTr = $(this).closest('tr')[0];
	   	var i = $.inArray(nTr, anOpen);

		if ( i === -1 ) {

			icon.removeClass('icon-plus');
			icon.addClass('icon-spinner icon-mirrored icon-spin'); 
			if(!oOuterTable.fnIsOpen(nTr)) {

				anOpen.push(nTr);	
				var nDetailsRow = null;
				var oData = oOuterTable.fnGetData(nTr);

				// Get the data for the inner table
				var params = getParameters('user/details', {username: oData[2]});
				ajaxRequest(params, function(response) {

				 		nDetailsRow = oOuterTable.fnOpen(nTr, response, 'details');
				 		oInnerTable = $('#data_table_inner').dataTable({
					        "bDestroy": true,
					        "bPaginate": false,
					        "bFilter": false,
					        "bInfo": false,
							"aoColumns": [ 
								/* ID */ 		{ "bSortable": false, "sWidth": "5%" },
								/* Username */ 	{ "bSortable": false, "sWidth": "*"  },
								/* Password */ 	{ "bSortable": false, "sWidth": "*"  },
								/* Notes */ 	{ "bSortable": false, "sWidth": "*"  },
								/* Edit */ 		{ "bSortable": false, "sWidth": "5%" },
								/* Delete */ 	{ "bSortable": false, "sWidth": "5%" }
							]						 			
				 		});  

				 		$('div.innerDetails', nDetailsRow).slideDown(); 
						icon.removeClass('icon-spinner icon-spin');
						icon.addClass('icon-minus');						
				});			
			}	
	    }
	    else {
	    	var icon = $('i', this);
			$('div.innerDetails', $(nTr).next()[0]).slideUp( function () {
			  	oOuterTable.fnClose( nTr );
				anOpen.splice( i, 1 );
				icon.removeClass('icon-minus');
				icon.addClass('icon-plus');					  
			});
	    }
	});


    // Select2 the DataTable 'records per page' select element
    $('div.dataTables_length select').select2({width:60});

});
