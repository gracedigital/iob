
var InnerTable = function () {

    return {

        //main function to initiate the module
        init: function () {

            function restoreRow(oInnerTable, nRow) {
                var aData = oInnerTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                for (var i = 1, iLen = jqTds.length; i < iLen; i++) {
                    oInnerTable.fnUpdate(aData[i], nRow, i, false);
                }
                oInnerTable.fnDraw();
            }

            function editRow(oInnerTable, nRow) {
                var aData = oInnerTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                jqTds[0].innerHTML = '<span class="m-wrap small">' + aData[0] + '</span>';
                jqTds[1].innerHTML = '<span class="m-wrap small">' + aData[1] + '</span>';
                jqTds[2].innerHTML = '<input type="password" class="m-wrap small" value="' + aData[2] + '">';
                jqTds[3].innerHTML = '<input type="text" class="m-wrap small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<a id="edit-user" href="">Save</a>';
                jqTds[5].innerHTML = '<a id="cancel-edit-user" href="">Cancel</a>';
            }

            function saveUpdated(oInnerTable, nRow) {

                var jqInputs = $('input', nRow);
                if(!validInputs(jqInputs)) {
                    return false;
                }

                var aData = oInnerTable.fnGetData(nRow);

                if(_.isEmpty(jqInputs[0].value)) {
                    var params = getParameters('user/update', { subaccount_username:aData[1], note:jqInputs[1].value });
                } else {
                    var params = getParameters('user/update', { subaccount_username:aData[1], subaccount_new_password:jqInputs[0].value, note:jqInputs[1].value });
                }
                
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        showSuccess("The user record was saved!");
                    }
                    return false;
                });

                oInnerTable.fnUpdate(jqInputs[0].value, nRow, 2, false);
                oInnerTable.fnUpdate(jqInputs[1].value, nRow, 3, false);
                oInnerTable.fnUpdate('<a id="edit-user" href="">Edit</a>', nRow, 4, false);
                oInnerTable.fnUpdate('<a id="delete-user" href="">Delete</a>', nRow, 5, false);
                oInnerTable.fnDraw();
            }

            function saveNew(oInnerTable, nRow) {

                var aData = oInnerTable.fnGetData(nRow);
                var jqInputs = $('input', nRow);

                var params = getParameters('user/add', { subaccount_username:jqInputs[0].value, subaccount_password:jqInputs[1].value, note:jqInputs[2].value });
                ajaxRequest(params, function(response) {
                    var result = JSON.parse(response);
                    if(result.error) {      
                        showError(result.error.message);
                    } else { 
                        oInnerTable.fnUpdate(jqInputs[0].value, nRow, 3, false);
                        oInnerTable.fnUpdate(jqInputs[1].value, nRow, 4, false);
                        oInnerTable.fnUpdate('<a class="edit" href="">Edit</a>', nRow, 5, false);
                        oInnerTable.fnUpdate('<a class="delete" href="">Delete</a>', nRow, 6, false);
                        oInnerTable.fnDraw();                                
                        showSuccess('Child user account "' + jqInputs[0].value + '" was successfully added and give ID # ' + result.id);
                    }
                    return false;
                });
            }

            function saveRow(oInnerTable, nRow) {
                var aData = oInnerTable.fnGetData(nRow);
                if(aData[5].indexOf('data-mode') > 0) {
                    saveNew(oInnerTable, nRow);
                } else {
                    saveUpdated(oInnerTable, nRow);
                }
            }

            function validInputs(aInputs) {
                // Check Password
                if(aInputs[0].value) {
                    if(aInputs[0].value.length < 1) {
                        showError('Invalid Password.  Must be 1 or more characters in length');
                        return false;
                    }
                }
                
                // Check Notes
                if(aInputs[1].value) {
                    if(aInputs[1].value.length < 1) {
                        showError('Invalid Note.  Must be 1 or more characters in length');
                        return false;
                    }
                }
                return true;
            }

            var nEditing = null;

            $('#btn-add-object').click(function (e) {
                stopEvent(e);
                var aiNew = oInnerTable.fnAddData([null, '', '', '', '',
                    '<a class="edit" href="">Edit</a>', '<a class="cancel" data-mode="new" href="">Cancel</a>'
                ]);
                var nRow = oInnerTable.fnGetNodes(aiNew[0]);
                editNewRow(oInnerTable, nRow);
                nEditing = nRow;
            });

            // $('#delete-user').on('mousedown', function (e) {
            //     stopEvent(e);
            //     bootbox.dialog("<p>&nbsp;You do not have permission to complete this action.</p>", [
            //         {
            //             "label" : "OK",
            //             "icon"  : "icon-ok",
            //             "class" : "green",
            //             "callback": function() {
            //                 bootbox.hideAll();
            //             }
            //         }
            //     ]);
            // });

            $('#cancel-edit-user').on('mousedown', function (e) {
                stopEvent(e);
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oInnerTable.fnDeleteRow(nRow);
                } else {
                    restoreRow(oInnerTable, nEditing);
                    nEditing = null;
                }
            });

            $('#edit-user').on('mousedown', function (e) {
                stopEvent(e);
                /* Get the row as a parent of the link that was clicked on */
                var nRow = $(this).parents('tr')[0];

                if (nEditing !== null && nEditing != nRow) {
                    /* Currently editing - but not this row - restore the old before continuing to edit mode */
                    restoreRow(oInnerTable, nEditing);
                    editRow(oInnerTable, nRow);
                    nEditing = nRow;
                } else if (nEditing == nRow && this.innerHTML == "Save") {
                    /* Editing this row and want to save it */
                    saveRow(oInnerTable, nEditing);
                    nEditing = null;
                } else {
                    /* No edit in progress - let's start one */
                    editRow(oInnerTable, nRow);
                    nEditing = nRow;
                }
            });
        }

    };

}();