var Radio = function () {

	// private variables
	var row = {};
	var name = {};
	var serial = {};
	var action = {};
	var list_id = {};
	var element = {};
	var message = {};
	var callback = {};
	var list_name = {};
	var description = {};

	// private constants

	// private methods
	function parameterize(route, input) {
		return $.param({route:route, params:input});		    
	}

	// public methods
 	return {

        //main function to initiate template pages
        reset: function () {
			row = {};
			name = {};
			serial = {};
			action = {};
			element = {};
			list_id = {};
			message = {};
			callback = {};
			list_name = {};
			description = {};
        },

        print: function () {
	    	console.log('RADIO_PROPERTIES:');        		
	    	console.log("NAME: " + this.name);
	    	console.log("SERIAL: " + this.serial);
        },

       	serialize: function () {
	       	var output =  '{';
	       		output += '"name":"' + this.name + '", ';
	       		output += '"serial":"' + this.serial + '", ';
	       		output += '}';
	       	return output;
       	},

		parameterizeFor: function (action) {
			var result;
			switch (action) {
				case 'folders':
					result = parameterize('radio/folders', { serial: this.serial });
					break;				
				case 'delete':
					result = parameterize('radio/delete', { serial: this.serial });
					break;
				case 'rename':
					result = parameterize('radio/rename', { serial: this.serial, name: this.name });
					break;				
				case 'link':
					result = parameterize('radio/associate_folder', { serial: this.serial, list_id: this.list_id });
					break;				
				case 'unlink':
					result = parameterize('radio/disassociate_folder', { serial: this.serial, list_id: this.list_id });
					break;
			}
			return result;
		}, 

        prepareFor: function (action, row, data, inputs) {

        	this.reset();
            this.row = row;

            switch (action) 
            {
            	case 'activate':
            		this.action = 'activate';
					this.element = inputs;	
					this.serial = data[3].trim();
					this.message = 'Are you sure you want to activate the radio serial # <b>' + this.serial + '</b>';
					this.callback = 'javascript:activateRadio()';
            		break;

            	case 'deactivate':
            		this.action = 'deactivate';
					this.element = inputs;	
					this.serial = data[3].trim();	
					this.message = 'Are you sure you want to deactivate the radio serial # <b>' + this.serial + '</b>';
					this.callback = 'javascript:deactivateRadio()';
            		break;

            	case 'delete':
            	 	this.serial = data[3].trim();
		            this.name = data[4].trim();
		            this.message = 'Are you sure you want to delete the radio "<b>' + this.name + '</b>"?';
		            this.callback = 'javascript:deleteRadio()';                  
	               	break;

	            case 'rename':
		            this.serial = data[3].trim();
		            this.name = inputs[0].value.trim().length == 0 ? 'CLEAR' : inputs[0].value.trim();
	            	break;	 

	            case 'unlink':
	            	this.list_id = data[0].trim(); 
	            	this.description = data[1].trim(); 
		           	this.message = 'Are you sure you want to unlink the lineup "<b>' + this.description + '</b>"?';
		            this.callback = 'javascript:unlinkFolder()';    
	            	break;
            }
        }

	};        

}();