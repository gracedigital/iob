function blockContainer() {
    var container = $('.portlet-body:visible', 'body');
    container.block({message:''}).spin({color: '#fff', lines:11, radius:20, length:15});
    return container;
}

function unblockContainer(container) {
    // container.spin(false).unblock();  
}

function getParameters(route, input) {
	return $.param({route:route, params:input});		    
}

function ajaxPost(params, callback) {
	clearMessages();
	var container = $('.portlet-body:last');
	container.block({message:''}).spin({color: '#fff', lines:11, radius:20, length:15});
    ajaxCall(container, params, callback, true, 'POST');
}

function ajaxSyncRequest(params, callback) {
    clearMessages();
    var container = $('.portlet-body:last');
    container.block({message:''}).spin({color: '#fff', lines:11, radius:20, length:15});
    ajaxCall(container, params, callback, false, 'GET');
}

function ajaxRequest(params, callback) {
    clearMessages();
    var container = $('.portlet-body:last');
    container.block({message:''}).spin({color: '#fff', lines:11, radius:20, length:15});
    ajaxCall(container, params, callback, true, 'GET');
}

function ajaxCall(container, params, callback, async, type) {
    $.ajax({
        data: params,
        async: async,
        type: type,
        timeout: 120000,
        url: 'application/proxy.php'
    })
    .done(function(response, textStatus, jqXHR) {
        if(response) {
            var result = (response.hasOwnProperty('responseText')) ? response.responseText : response;
            if(callback) {
                callback(result);
            }
        } else {
            showError(textStatus + ' ' + jqXHR.responseText);
        }
	})
    .fail(function(jqXHR, textStatus, errorThrown) {
        showError(errorThrown);
        jqXHR.abort();
        
    })
    .always(function(data, textStatus, jqXHR) {
        container.spin(false).unblock();  
    });
}

// BlockUI Setup
$.blockUI.defaults.overlayCSS.opacity = 0.2; 