var Group = function () {

	// private variables
	var row = {};
	var note = {};
	var action = {};
	var message = {};
	var callback = {};
	var description = {};
	var schedule_id = {};
	var radiogroup_id = {};
	var schedule_description = {};

	// private constants
	var obsolete;

	// private methods
	function parameterize(route, input) {
		return $.param({route:route, params:input});		    
	}

	// public methods
 	return {

        //main function to initiate template pages
        reset: function () {
			row = {};
			note = {};
			title = {};
			action = {};
			message = {};
			callback = {};
			schedule_id = {};
			radiogroup_id = {};
			schedule_description= {};
			this.obsolete = false;
        },

        print: function () {
	    	console.log('GROUP_PROPERTIES:');        		
	    	console.log("NOTE: " + this.note);
	    	console.log("OBSOLETE: " + this.obsolete);
	    	console.log("DESCRIPTION: " + this.description);
	    	console.log("RADIOGROUP_ID: " + this.radiogroup_id);
        },

       	serialize: function () {
	       	var output =  '{';
	       		output += '"note":"' + this.note + '", ';
	       		output += '"obsolete":"' + this.obsolete + '"';
	       		output += '"description":"' + this.description + '", ';
	       		output += '"radiogroup_id":"' + this.radiogroup_id + '", ';
	       		output += '}';
	       	return output;
       	},

		parameterizeFor: function (action) {
			var result;
			switch (action) {
				case 'schedules':
					result = parameterize('group/schedules', { radiogroup_id: this.radiogroup_id });
					break;				
				case 'delete':
					result = parameterize('group/delete', { radiogroup_id: this.radiogroup_id });
					break;
				case 'update':
					result = parameterize('group/update', { radiogroup_id: this.radiogroup_id, description: this.description, note: this.note, obsolete: this.obsolete });
					break;
			}
			return result;
		}, 

        prepareFor: function (action, row, data, inputs) {

        	this.reset();
            this.row = row;

            switch (action) {
            	case 'delete':
		            this.radiogroup_id = data[2].trim();
		            this.description = data[3].trim();
		            this.message = 'Are you sure you want to delete the group "<b>' + this.description + '</b>"?';
		            this.callback = 'javascript:deleteGroup()';                  
	               	break;
	            case 'update':
		            this.description = inputs[0].value.trim();
		            this.note = inputs[1].value.trim();
		            this.radiogroup_id = data[2].trim();            	
	            	break;
            }
        }

	};        

}();