<?php

	session_start();
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/UserModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/CredentialModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/configs/site-config.php');

	$filename = '';
	$parameters = array();
	$credentials = array('username'=>$_SESSION['username'], 'password'=>$_SESSION['password']);

	if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) || in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])) {
		$filename = 'credential-export-'. date('Ymd') . '.csv';
		$response = CredentialModel::Fetch($parameters, $credentials);
	} else {
		$filename = 'user-export-'. date('Ymd') . '.csv';
		$response = UserModel::Fetch($parameters, $credentials);
	}

	if(!empty($response['status']) && $response['status'] != 'success') 
	{
		ob_start();
		print_r($response);
		$buffer = ob_get_clean();
		echo($buffer); die;
	} else 
	{	
		header("Content-type: text/csv");
		header("Cache-Control: no-store, no-cache");
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		 
		$outstream = fopen("php://output", "w");

		if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) || in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])) {
			// print headers 
			fputcsv($outstream, array_keys($response[0]), ',', '"');
			// print data
			foreach($response as $credential) {
		    	fputcsv($outstream, $credential, ',', '"');
			}		
		} else {
			/* PARENT */
			fputcsv($outstream, array('parent'));
			// print headers 
			fputcsv($outstream, array_keys($response['users'][0]), ',', '"');
			// print data
			foreach($response['users'] as $childuser) {
		    	fputcsv($outstream, $childuser, ',', '"');
			}		
			fputcsv($outstream, array(''));

			/* CHILDREN */
			if(!empty($response['childusers'][0])) {
				fputcsv($outstream, array('children'));
				// print headers 
				fputcsv($outstream, array_keys($response['childusers'][0]), ',', '"');
				// print data
				foreach($response['childusers'] as $childuser) {
			    	fputcsv($outstream, $childuser, ',', '"');
				}
			}
			
		}

		fclose($outstream);
	}

?>