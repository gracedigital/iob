<?php

	include_once $_SERVER['DOCUMENT_ROOT'].'/application/configs/site-config.php';

	class MetaHelper {

		static function GetDomain() {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$parsed = parse_url($url);
			$domain = $parsed['host'];
			return $domain;
		}

		static function isSirius() {
			$domain = self::GetDomain();
			return (stripos($domain, NAME_SIRIUS) !== false) || (stripos($domain, NAME_SXM) !== false);
		}

		static function GetTitle() {
			return SITE_TITLE;
		}

		static function GetDescription() {
			return META_DESCRIPTION;
		}

		static function GetImageAlt($image) {
			if (self::isSirius()) {
				switch (strtolower($image)) {
					case 'logo':
						$result = META_LOGO_IMG_ALT_SXM;
						break;
					case 'product':
						$result = META_PRODUCT_IMG_ALT_SXM;
						break;
				}
			} else {
				switch (strtolower($image)) {
					case 'logo':
						$result = META_LOGO_IMG_ALT_GDI;
						break;
					case 'product':
						$result = META_PRODUCT_IMG_ALT_GDI;
						break;
				}				
			}
			return $result;
		}

	}
?>
