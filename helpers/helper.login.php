<?php

	include_once $_SERVER['DOCUMENT_ROOT'].'/application/configs/site-config.php';
	
	class LoginHelper {


		static function GetDomain() {
			$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
			$parsed = parse_url($url);
			$domain = $parsed['host'];
			return $domain;
		}

		static function isSirius() {
			$domain = self::GetDomain();
			return (stripos($domain, NAME_SIRIUS) !== false) || (stripos($domain, NAME_SXM) !== false);
		}

		static function GetLogo() {
			return self::isSirius() ? LOGO_SXM : LOGO_GDI;    		
		}		

		static function GetCaption() {
			return self::isSirius() ? CAPTION_LOGIN_SXM : CAPTION_LOGIN_GDI;    		
		}

		static function GetTitle(){
		    return self::isSirius() ? TITLE_LOGIN_SXM : TITLE_LOGIN_GDI;
		}		

		static function GetTerms(){
		    return self::isSirius() ? LABEL_LOGIN_TERMS_SXM : LABEL_LOGIN_TERMS_GDI;
		}	
	}

?>
		