<?php

	/*
		Add this rule into httpd.conf file so that *.ini (or *.inc) in my case can't be sent to a browser
		<Files *.inc>  
		    Order deny,allow
		    Deny from all
		</Files>
	*/
	class UserHelper {

		static function GetParent($code) {	
			$ini_array = parse_ini_file('../../reciva/config.ini.php', TRUE);
			return $ini_array[$code];
		}
 
		static function GetParentCredentials($code) {
			$parent = self::GetParent($code);
			$credentials = array('username'=>key($parent), 'password'=>reset($parent));
			return $credentials;
		}

	}

?>