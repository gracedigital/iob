<?php

	class EmailHelper {

		static function SendTestEmail() {

			$to = "christia.a.carlson@gmail.com";
			$subject = "Email test from {$_SERVER['HTTP_HOST']}";
			$body = "The email test script was successful. Mail is being sent from {$_SERVER['HTTP_HOST']} with no issue.";
			$headers = "From: email@{$_SERVER['HTTP_HOST']}";

			if (mail($to,$subject,$body,$headers)) {
			 echo "An e-mail was sent to $to with the subject $subject ";
			} else {
			 echo "—FAILED TO SEND EMAIL FROM {$_SERVER['HTTP_HOST']}.";
			} 
		}

	}

?>