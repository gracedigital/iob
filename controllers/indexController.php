<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/DashboardModel.php');

	class indexController Extends baseController {

		public function doAction($action) {
			$parameters = array();
			$this->registry->template->result = DashboardModel::GetStats($parameters, $this->registry->credentials);	
		    $this->registry->template->show('index');
		}
	}

?>
