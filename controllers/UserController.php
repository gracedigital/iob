<?php
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/UserModel.php');

	class UserController Extends baseController {

		private $view;  

		public function doAction($action) {

			$this->registry->template->attributes = '@attributes';
			 
			switch ($action) {

				case 'manage':
					$this->view = $action;	
					$parameters = array('username_id'=>$_SESSION['username']);
 					$this->registry->template->data = UserModel::Fetch($parameters, $this->registry->credentials); 	
 					break;				

 				case 'add':			
	 				$data = UserModel::Add($this->registry->params, $this->registry->credentials);
	 				return $data;

	 			case 'delete':
	 				$data = UserModel::DeleteGroups($this->registry->params, $this->registry->credentials);
	 				return $data;

				case 'details': 
					$this->view = $action;
					$this->registry->template->data = UserModel::Fetch(array(), $this->registry->credentials);
					break;

				case 'update':
					$data = UserModel::Update($this->registry->params, $this->registry->credentials);
					return $data;				

 				default:
	 				$this->view = $action;	
	 				break; 				

			}

			$this->registry->template->show($this->view, 'user');
		}	
	}
?>