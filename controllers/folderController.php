<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/FolderModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/FolderAccessModel.php');

	class FolderController Extends baseController {

		private $view;

		public function doAction($action) {

			switch ($action) {

				case 'new':
					$this->view = $action;
					$this->registry->template->folders = FolderModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);		
					break;

				case 'add_access':
					$data = FolderAccessModel::Add($this->registry->params, $this->registry->credentials);
					return $data;
				
				case 'add':
					$data = FolderModel::Add($this->registry->params, $this->registry->credentials);
					return $data;

				case 'delete':
					$result = FolderModel::Delete($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;

				case 'update':
					$data = FolderModel::Update($this->registry->params, $this->registry->credentials);
					return $data;

				case 'radios': 
					// Get a list of radios assigned to a folder
					$this->view = $action; 
					$this->registry->template->radios = null;
					$this->registry->template->radios = FolderModel::Assigned($this->registry->params, $this->registry->credentials);
					break;

				default:
					$this->view = $action;
					$this->registry->template->folders = FolderModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_ARRAY);	
					break;				
			}

			$this->registry->template->show($this->view, 'lineup');
		}						
	}		
?>