<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/DashboardModel.php');

	class DashboardController Extends baseController {

		private $view;  

		public function doAction($action) {

			switch ($action) {
	 			case 'view':
	 				$this->view = $_SESSION['view'];
	 				$this->registry->params = empty($this->registry->params) ? array() : $this->registry->params;
	 				$this->registry->template->result = DashboardModel::GetStats($this->registry->params, $this->registry->credentials);
	 				break;
			}
			
			$this->registry->template->show($this->view, 'dashboard');
		}	

	}
?>