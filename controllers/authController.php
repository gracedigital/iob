<?php
	
	if(!isset($_SESSION)) session_start();

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/UserModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/configs/site-config.php');

	class authController {
		
		public function authenticate($tokens) {

			$parameters = array();
	    	parse_str($tokens, $credentials);

	    	$response = UserModel::Login($parameters, $credentials);

	    	if(empty($response['error'])) {

				if(array_key_exists('remember', $credentials)) {
					// issue a cookie to keep username 
					$year = time() + 31536000;
					setcookie('remember_me', $credentials['username'], $year, '/');
				} else {
					if(isset($_COOKIE['remember_me'])) {
						$past = time() - 100;
						if(setcookie('remember_me', '', $past, '/')) {
							// success
						}
					}
				}

				// Authentication Succeeded
				$_SESSION['username'] = $credentials['username'];
				$_SESSION['password'] = $credentials['password'];
				$_SESSION['privileges'] = $response['privileges'];
				$_SESSION['code'] = $response['stats']['info1'];
				$_SESSION['view'] = in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) ? $_SESSION['code'] : 'gdi';
				session_regenerate_id(true);

				// Timeout Management
				$_SESSION['created'] = time(); 								// set creation time
				$_SESSION['last_activity'] = $_SESSION['created'];			// set activity start time
				$_SESSION['expire'] = $_SESSION['created'] + (60 * 60);		// set expiration time 30 minutes			
			} 		
			echo json_encode($response);	
		}
	}

	$controller = new authController();
	$controller->authenticate($_REQUEST['tokens']);
?>