<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/GroupModel.php');		
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/CredentialModel.php');

	class CredentialController Extends baseController {

		private $view;

		public function doAction($action) {

			switch ($action) {
				
				case 'manage':
					$this->view = $action;		
					$this->registry->template->result = CredentialModel::Fetch($this->registry->params, $this->registry->credentials);	
					break;				

				case 'view_move':
					$parameters = array();
					$this->view = 'move';
	 				$this->registry->template->groups = GroupModel::Fetch($parameters, $this->registry->credentials, FORMAT_LIST); 					
					break;

				case 'provision':
					$this->view = $action;
					$this->registry->template->result = GroupModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);
					break;	

				case 'list':
					$filter = array('sirius_userdetail_id', 'siriususername', 'description');
					$result = CredentialModel::FetchFiltered($this->registry->params, $this->registry->credentials, FORMAT_TABLE, $filter);
					echo json_encode($result);
					return;

				case 'register':
					$result = CredentialModel::Register($this->registry->params, $this->registry->credentials);					
					echo json_encode($result);
					return;

	 			case 'move':
	 				$result = CredentialModel::Move($this->registry->params, $this->registry->credentials); 				
					echo json_encode($result);
					return;	 			

				case 'update':
	 				$result = CredentialModel::Update($this->registry->params, $this->registry->credentials); 				
					echo json_encode($result);
					return;
 			 	
 			 	case 'delete':
	 				$result = CredentialModel::Delete($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;			

				default:
					$this->view = $action;
					break;			
			}

			$this->registry->template->show($this->view, 'credential');

		}							
	}		
?>