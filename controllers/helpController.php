<?php

	class HelpController Extends baseController {

		private $view;

		public function doAction($action) {
			switch ($action) {
				default:
					$this->view = $action;
					break;				
			}
			$this->registry->template->show($this->view, 'help');
		}						
	}		
?>