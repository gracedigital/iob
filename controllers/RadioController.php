<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/FolderModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/RadioModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/GroupModel.php');		
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ChannelModel.php');	
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/RadioFolderModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/RadioGroupModel.php');

	class RadioController Extends baseController {

		private $view;

		public function doAction($action) {

			switch ($action) {

				case 'activate':
					$result = RadioModel::Activate($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'deactivate':
					$result = RadioModel::Deactivate($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'rename':
					$result = RadioModel::Rename($this->registry->params, $this->registry->credentials);				
					echo json_encode($result);
					return;

	 			case 'update':
	 				$result = RadioModel::Update($this->registry->params, $this->registry->credentials); 				
					echo json_encode($result);
					return;
 								
	 			case 'delete':
	 				$result = RadioModel::Delete($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;

				case 'new':
					$parameters = array();
					$this->view = $action;
					$this->registry->template->groups = GroupModel::Fetch($parameters, $this->registry->credentials, FORMAT_LIST);			
					$this->registry->template->folders = FolderModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);					
					break;				

				case 'provision':
					$result = RadioModel::Provision($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;

				case 'unregister':
					$result = RadioModel::Unregister($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;

				case 'move':
					$parameters = array();
					$this->view = $action;
	 				$this->registry->template->groups = GroupModel::Fetch($parameters, $this->registry->credentials, FORMAT_LIST); 					
					break;

				case 'remove':
					$parameters = array();
					$this->view = $action;	
	 				$this->registry->template->groups = GroupModel::Fetch($parameters, $this->registry->credentials, FORMAT_LIST); 					
					break;

				case 'manage':
					$this->view = $action;
					$this->registry->params = array('expand'=>'radiogroup');	
					$this->registry->template->result = RadioModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_ARRAY);
					break;	

				case 'list':
					$result = RadioGroupModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_TABLE);
					echo json_encode($result);
					return;

				case 'list_all':
					$data = RadioModel::GetAllRadios($this->registry->params, $this->registry->credentials);
					return $data;

				case 'list_owner':
					$data = RadioModel::GetUserRadios($this->registry->params, $this->registry->credentials);			
					return $data;

				case 'associate_group':	
					$result = RadioGroupModel::Associate($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'disassociate_group':			
					$result = RadioGroupModel::Disassociate($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'unassociated_groups':
					$result = RadioGroupModel::Unassociated($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'associate_folder':
					$result = RadioFolderModel::Associate($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);		
					return;

				case 'disassociate_folder':
					$result = RadioFolderModel::Disassociate($this->registry->params, $this->registry->credentials);			
					echo json_encode($result);
					return;

				case 'unassociated_folders':
					$result = RadioFolderModel::Unassociated($this->registry->params, $this->registry->credentials);		
					echo json_encode($result);
					return;

				case 'folders':
					$this->view = $action;
					$this->registry->template->folders = RadioFolderModel::Associated($this->registry->params, $this->registry->credentials);
					break;				

				default:
					$this->view = $action;
					break;
			}
				$this->registry->template->show($this->view, 'radio');
		}							
	}		
?>