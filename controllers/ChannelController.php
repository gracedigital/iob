<?php

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/FolderModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ChannelModel.php');

	class ChannelController Extends baseController {

		private $view;

		public function doAction($action) {

			switch ($action) {

				case 'add':
					$result = ChannelModel::Add($this->registry->params, $this->registry->credentials);	
					echo json_encode($result);
					return;

				case 'update':
					$result = ChannelModel::Update($this->registry->params, $this->registry->credentials);
					echo json_encode($result);					
					return;
					
				case 'unlink':
					$result = ChannelModel::Unlink($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;

				case 'select':
					$this->view = $action;
					$parameters = array('list_id'=>'-1', 'filter'=>'1');
					$this->registry->template->channels = ChannelModel::Fetch($parameters, $this->registry->credentials, FORMAT_LIST);
					break;

				case 'channels':
					$this->view = $action;
					$this->registry->template->channels = ChannelModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_ARRAY);
					break;				

				default:
					$this->view = $action;
					$parameters = array('list_id'=>'-1', 'filter'=>'1');
					$this->registry->template->channels = ChannelModel::Fetch($parameters, $this->registry->credentials, FORMAT_ARRAY);
					break;
			}

			$this->registry->template->show($this->view, 'lineup');
		}						
	}		
?>