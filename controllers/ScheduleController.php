<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/GroupModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ChannelModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/CredentialModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ScheduleModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ScheduleGroupModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ScheduleEventsModel.php');

	class ScheduleController Extends baseController {

		private $view;

		public function doAction($action) {
			switch ($action) {
	 			// ajax output //
				case 'update':
	 				$result = ScheduleModel::Update($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;	

	 			case 'delete':
	 				$result = ScheduleModel::Delete($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;

				case 'associated':
					$result = ScheduleGroupModel::Associated($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;					

				case 'unassociated':
					$result = ScheduleGroupModel::Unassociated($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;		 				

	 			case 'associate':
	 				$result = ScheduleGroupModel::Associate($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
					return;	

	 			case 'disassociate':
	 				$result = ScheduleGroupModel::Disassociate($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
					return;

	 			case 'add_events':
	 				$result = ScheduleEventsModel::Add($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
					return;

				case 'update_events':
	 				$result = ScheduleEventsModel::Update($this->registry->params, $this->registry->credentials);
					echo json_encode($result);
					return;	

				// standard output //
				case 'new':
					$this->view = $action;
					$parameters = array('list_id'=>'-1');
		 			$this->registry->template->groups = GroupModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);
		 			if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges'])) {
		 				$this->registry->template->channels = ChannelModel::FetchSXM($parameters, $this->registry->credentials);
		 			} else {
		 				$this->registry->template->channels = ChannelModel::Nested($parameters, $this->registry->credentials);
		 			} 
					break;

				case 'edit':
					$this->view = $action;
	 				$this->registry->template->result = ScheduleModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);				
		 			break;

	 			case 'manage':
	 				$this->view = $action;
		 			$this->registry->template->result = ScheduleModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_ARRAY);
		 			break;

	 			case 'groups':
	 				$this->view = $action;
					$this->registry->template->result = ScheduleGroupModel::Associated($this->registry->params, $this->registry->credentials);						
	 				break;	 				

				case 'edit_events':
					$this->view = 'events';
	 				$this->registry->template->days = ScheduleEventsModel::Fetch($this->registry->params, $this->registry->credentials);

		 			if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges'])) {
		 				$this->registry->template->channels = ChannelModel::FetchSXM($this->registry->params, $this->registry->credentials);
		 			} else {
		 				$this->registry->template->channels = ChannelModel::Nested($this->registry->params, $this->registry->credentials);
		 			} 
		 			break;
		 		default:
		 			$this->view = $action;
		 			break;
	 		}		

			$this->registry->template->show($this->view, 'schedule');
	 	}
	}		

?>	