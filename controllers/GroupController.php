<?php
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/GroupModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/models/ScheduleGroupModel.php');

	class GroupController Extends baseController {

		private $view;  

		public function doAction($action) {

			switch ($action) {

	 			// ajax output
				case 'unassociated':
	 				$result = GroupModel::Unssociated($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
					return;	

	 			case 'associate':
	 				$result = GroupModel::Associate($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
					return;	
						 			
				case 'update':
					$result = GroupModel::Update($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;
	 				
	 			case 'delete':
	 				$result = GroupModel::Delete($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;

				case 'add':			
	 				$result = GroupModel::Add($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;

	 			case 'delete_member':
	 				$result = GroupModel::DeleteMember($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;

	 			case 'add_access':
	 				$result = GroupModel::AddMember($this->registry->params, $this->registry->credentials);
	 				echo json_encode($result);
	 				return;

	 			case 'list':	
					$result = GroupModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_LIST);
					echo json_encode($result);
					return;

	 			// standard output	
	 			case 'manage':	
	 				$this->view = $action;
	 				$this->registry->template->result = GroupModel::Fetch($this->registry->params, $this->registry->credentials, FORMAT_ARRAY);
	 				break;

	 			case 'schedules':
	 				$this->view = $action;
					$this->registry->template->result = GroupModel::Associated($this->registry->params, $this->registry->credentials);						
	 				break;	 

				default:
					// $parameters = array();
	 				$this->view = $action;
					// $this->registry->template->result = GroupModel::Fetch($parameters, $this->registry->credentials, FORMAT_ARRAY);	 				
	 				break;				
			}

			$this->registry->template->show($this->view, 'group');
		}	
	}
?>