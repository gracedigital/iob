<?php

	if(!isset($_SESSION)) session_start();

	require_once($_SERVER['DOCUMENT_ROOT'].'/models/AccountModel.php');

	class accountController {
		
		public function register($data) {

	    	parse_str($data, $parameters);
	    	$response = AccountModel::Register($parameters);

			if(!empty($response['userAdd']['username_id']) || $response['error']['code'] == 1002) {
				$response = AccountModel::Permit($parameters);	
			}

			echo json_encode($response);
			return;

		}
	}

	$controller = new accountController();
	$controller->register($_REQUEST['data']);
	
?>