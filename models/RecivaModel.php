<?php
	
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/configs/api-config.php');
	require_once('Model.php');

	/**
	  * RecivaModel Class 
	  * 
	  * CRUD operations for a Reciva Objects
	*/

	class RecivaModel extends Model {

		static function DoSchedulerAction($action, $parameters, $credentials) {			
			$response = self::DoRecivaAction(API_SCHEDULER, $action, $credentials, $parameters);
			$result = self::XMLToArray($response);
			return $result['scheduler'];
		}

		static function DoTailoredAction($action, $parameters, $credentials) {			
			$response = self::DoRecivaAction(API_TAILORED, $action, $credentials, $parameters);		
			$result = self::XMLToArray($response);
			return $result['tailoredList'];
		}

		static function DoRegisterAction($action, $parameters, $credentials) {	
			$response = self::DoRecivaAction(API_REGISTER, $action, $credentials, $parameters);
			$result = self::XMLToArray($response);
			return $result['registration'];
		}

		static function DoSchedulerXMLAction($action, $xml, $parameters, $credentials) {			
			$response = self::DoRecivaAction(API_SCHEDULER, $action, $credentials, $parameters, $xml);	
			$result = self::XMLToArray($response);
			return $result['scheduler'];
		}

		static function DoSimpleTailoredAction($action, $parameters, $credentials) {			
			return self::DoRecivaAction(API_TAILORED, $action, $credentials, $parameters);		
		}

		static function DoSimpleSchedulerAction($action, $parameters, $credentials) {			
			return self::DoRecivaAction(API_SCHEDULER, $action, $credentials, $parameters);		
		}

		static function XMLToArray($input, $callback = null, $recurse = false) {

		    // Get input, loading an xml string with simplexml if its the top level of recursion
		    $data = ((!$recurse) && is_string($input))? simplexml_load_string($input): $input;
		    
		    // Convert SimpleXMLElements to array
		    if ($data instanceof SimpleXMLElement) $data = (array) $data;
		    
		    // Recurse into arrays
		    if (is_array($data)) {
		    	foreach ($data as $key=>&$item) {		    		
					if($key == '@attributes' && is_array($item)) {	
						foreach ($item as $subkey => $value) {
							$data[$subkey] = $value;
							unset($data['@attributes']);
						}
					} 
		    		$item = self::XMLToArray($item, $callback, true);
		    	}
		    }
		    return $data;
		}

		static function DoRecivaAction($api, $action, $credentials, $parameters, $xml=null) {
			$response = Model::DoAction($api, $action, $credentials, $parameters, $xml);
			return $response;	
		}	
	}

?>
