<?php

	require_once('RecivaModel.php');

	class RadioModel {

	    /***
			SET RADIO to not be obsolete
	    ***/
		static function Activate($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_unobsolete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}
					
			return $result;
		}

	    /***
			SET RADIO to be obsolete
	    ***/
		static function Deactivate($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_obsolete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}
			
			return $result;
		}

	    /***
			SET RADIO name
	    ***/
		static function Rename($parameters, $credentials) {
		
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_update_name', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}
						
			return $result;
		}

	    /***
			UPDATE RADIOS
	    ***/
		static function Update($parameters, $credentials) {

			$result = array();
			$list = $parameters['list'];
			unset( $parameters['list'] );

			foreach ($list as $radio) {
				$parameters['serial'] = $radio['serial'];
				$parameters['obsolete'] = 'false';
				$parameters['radiogroup_member_id'] = $radio['id'];

				if($radio['id'] == 0) {
					$response = RecivaModel::DoSchedulerAction('radiogroup_member_add', $parameters, $credentials);
				} else {
					$response = RecivaModel::DoSchedulerAction('radiogroup_member_update', $parameters, $credentials);
				}
				
				if($response['status'] != 'success') {
					$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
				}
			}

			return $result;
		}

	    /***
			REMOVE RADIOs from a group
	    ***/
		static function Delete($parameters, $credentials) {

			$result = array();

			$members = $parameters['members'];

			if(count($members) > 0) {
				foreach ($members as $member) {
					$parameters = array('radiogroup_id'=>$parameters['radiogroup_id'], 'radiogroup_member_id'=>$member);

					$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_delete', $parameters, $credentials);

					$xml = simplexml_load_string ($response);

					if($xml->scheduler->status != 'success') {
						$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
						break;
					}
				}
			}

			return $result;
		}

	    /***
			Unregister radio
	    ***/
		static function Unregister($parameters, $credentials) {

			$result = array();

			// $response = RecivaModel::DoSchedulerAction('user_radio_registration_delete', $parameters, $credentials);
			$response = RecivaModel::DoSchedulerAction('user_radio_reset', $parameters, $credentials);
			
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}
					
			return $result;
		}

	    /***
			REGISTER RADIO
	    ***/
		static function Provision($parameters, $credentials) {

			$result = self::Register($parameters, $credentials);
			
			if(!empty($result['serial'])) {
				$parameters['serial'] = $result['serial'];
				$result = self::Associate($parameters, $credentials);
				if(empty($result['error'])) {
					$result = array('serial'=>$parameters['serial']);
				}
			}

			return $result;
		}

	    /***
			REGISTER RADIO
	    ***/
		static function Register($parameters, $credentials) {

			$parameters['username'] = API_REGISTER_USERNAME;
			$parameters['password'] = API_REGISTER_PASSWORD;
			$parameters['password'] = API_REGISTER_PASSWORD;
			$parameters['dealer_key'] = API_REGISTER_DEALERKEY;
			$parameters['dealer_password'] = API_REGISTER_DEALERPWD;

			$response = RecivaModel::DoRegisterAction('register_businessio_add', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('serial'=>$response['registrationAdd']['radioserial']);
			}
					
			return $result;
		}

	    /***
			ADD RADIO to a group and folder
	    ***/
		static function Associate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_update_name', $parameters, $credentials);
			$xml = simplexml_load_string ($response);
			
			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string) $xml->scheduler->errorCode, 'message'=> (string) $xml->scheduler->message);
			} else {

				$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_add', $parameters, $credentials);
				
				$xml = simplexml_load_string ($response);

				if($xml->scheduler->status != 'success') {
					$result['error'] = array('code'=> (string) $xml->scheduler->errorCode, 'message'=> (string) $xml->scheduler->message);
				} else {
					$result['radiogroup_member_id'] = (string) $xml->scheduler->radiogroupMemberAdd['radiogroup_member_id'];

					// Do folder assignments
					$response = RecivaModel::DoSimpleTailoredAction('list_access_add', $parameters, $credentials);

					$xml = simplexml_load_string ($response);

					if(!empty($xml->tailoredList)) {
						if($xml->tailoredList->status != 'success') {
							$result = array();
							$result['error'] = array('code'=> (string) $xml->tailoredList->errorCode, 'message'=> (string) $xml->tailoredList->message);
						} else {
							$result['list_id'] = (string) $xml->tailoredList->addListAccess['listId'];
						}
					}
				}
			}

			return $result;
		}

		static function Fetch($parameters, $credentials, $format) {

			$result = null;
			
			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);			
			} else {

				switch ($format) {
					case FORMAT_LIST:
						$result = '';
						foreach($xml->scheduler->radioserialList->radio as $element) {	
							$result .= '<option value="'.$element['radiogroup_id'].'">'.urldecode(stripslashes($element['description'])).'</option>';
						}	
						break;
					case FORMAT_TABLE:
						$total = $xml->scheduler->radioserialList['radioserial_count'];
						$result = array("sEcho" => intval(1), "iTotalRecords" => intval($total), "iTotalDisplayRecords" => intval($total), "aaData" => array());
						foreach($xml->scheduler->radioserialList->radio as $element) {	
							$result['aaData'][] = array(null,  (string)$element['radioserial'],  (string)$element['description']);
						}

					default:
						$tmp1 = array();
						$tmp2 = array();
						$result = array();
						

						foreach($xml->scheduler->xpath('//scheduler/radioserialList/radio') as $radio) {	
							$tmp1 = array('radioserial'=>(string)$radio['radioserial'], 'name'=>(string)$radio['name'], 'obsolete'=>(string)$radio['obsolete']);
							if(!is_null($radio->radiogroupList->radiogroup['description'])) {								
								$tmp2 = array('description'=>$radio->radiogroupList->radiogroup['description'], 'radiogroup_id'=>$radio->radiogroupList->radiogroup['description']);
							}
							$result[] = array_merge($tmp1, $tmp2);
							$tmp2 = array();
						}
				}
			}
			return $result;
		}


	    /***
			GET RADIOS for a specific group
	    ***/
		// static function GetRadios($parameters, $credentials) {

		// 	$result = null;

		// 	$response = RecivaModel::DoSchedulerAction('radiogroup_member_list', $parameters, $credentials);

		// 	if($response['status'] != 'success') {
		// 		$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
		// 	} else {

		// 		$iTotal = $response['radiogroupMemberList']['count'];
		// 		$result = array(
		// 			"sEcho" => intval(1),
		// 			"iTotalRecords" => intval($iTotal),
		// 			"iTotalDisplayRecords" => intval($iTotal),
		// 			"aaData" => array()
		// 		);

		// 		if($iTotal > 1) {
		// 			foreach($response['radiogroupMemberList']['radiogroup_member'] as $listEntry) {
		// 				$result['aaData'][] = array(null, $listEntry['radiogroup_member_id'], $listEntry['radioserial']);
		// 			}
		// 		} elseif ($iTotal == 1) {
		// 			$listEntry = $response['radiogroupMemberList']['radiogroup_member'];
		// 			$result['aaData'][] = array(null, $listEntry['radiogroup_member_id'], $listEntry['radioserial']);	
		// 		}
		// 	}			

		// 	echo json_encode($result);
		// }

	    /***
			GET RADIOS for all groups
	    ***/
		static function GetAllRadios($parameters, $credentials) {

			$result = null;

			$response = RecivaModel::DoSchedulerAction('radiogroup_list', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {

				$iTotal = $response['radiogroupList']['count'];
				$result = array(
					"sEcho" => intval(1),
					"iTotalRecords" => intval($iTotal),
					"iTotalDisplayRecords" => intval($iTotal),
					"aaData" => array()
				);

				if($iTotal > 0) {
					foreach($response['radiogroupList']['radiogroup'] as $radiogroup) {
						$groupName = empty($radiogroup['description']) ? 'All Groups' : $radiogroup['description'];
						if(!empty($radiogroup['radiogroupMemberList'])) {	
							if(iTotal > 1) {				
								foreach($radiogroup['radiogroupMemberList']['radiogroup_member'] as $member) {
									$result['aaData'][] = array(null, $member['radioserial'], $groupName, $member['id'], $member['radiogroupId']);
								}
							} elseif (iTotal == 1) {
								$member = $radiogroup['radiogroupMemberList']['radiogroup_member'];
								$result['aaData'][] = array(null, $member['radioserial'], $groupName, $member['id'], $member['radiogroupId']);
							}
						}
					}
				}	
			}		

			echo json_encode($result);
		}

	    /***
			GET ALL RADIOS assigned and unassigned to groups
	    ***/
		static function GetUserRadios($parameters, $credentials) {

			$result = null;

			$response = RecivaModel::DoSchedulerAction('user_radio_list', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {

				$iTotal = $response['radioList']['count'];
				$result = array(
					"sEcho" => intval(1),
					"iTotalRecords" => intval($iTotal),
					"iTotalDisplayRecords" => intval($iTotal),
					"aaData" => array()
				);

				if($iTotal > 0) {
					foreach($response['radioList']['radio'] as $radio) {
						
						if(!empty($radio['radioGroupList'])) {				
							foreach($radio['radioGroupList']['radiogroup'] as $radiogroup) {
								$result['aaData'][] = array(null, $radio['radioserial'], $radiogroup['description'], $radiogroup['ownerId'], $radiogroup['id']);
							}
						} else {
							$result['aaData'][] = array(null, $radio['radioserial'], 'Unassigned', 0, 0);
						}

					}
				} 
			}		

			echo json_encode($result);
		}
		

	}
?>