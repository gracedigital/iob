<?php

class Model {

 	static function DoAction($api, $action, $credentials, $parameters=null, $xml=null) {

		$handle = curl_init();
		$url = BASE_URL."/{$api}?";
		$url .= "username=".$credentials['username'];
		$url .= "&password=".$credentials['password'];
		$url .= "&action=$action";
		
		if($parameters) {
			if(is_array($parameters)) {
				foreach ($parameters as $parameter => $value) {
					$url .= "&{$parameter}=".rawurlencode(trim($value));
				}
			} else if(is_string($parameters)) {
				$url .= rawurlencode(trim($parameters));
			}
		}
		
		// file_put_contents('log.xml', '<request>'.$url.'</request>', FILE_APPEND);
		
	    $options = array
	    (
	    	CURLOPT_URL            => $url,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER         => false,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false, 
			CURLOPT_VERBOSE        => false,
			CURLOPT_SSLCERTTYPE    => SSL_CERTTYPE,
			CURLOPT_SSLCERT        => SSL_CERT,
			CURLOPT_SSLKEY		   => SSL_KEY,
			CURLOPT_SSLKEYPASSWD   => SSL_KEYPASS
	    );
	 
		if(!empty($xml)) {
			// $options[CURLOPT_VERBOSE] = true;
			// file_put_contents('events.xml', $xml, FILE_APPEND);
			$options[CURLOPT_POST] = true;
			$options[CURLOPT_POSTFIELDS] = "xml_data_load=".urlencode($xml);
		}

	    curl_setopt_array($handle, $options);
	    $result = curl_exec($handle);			

		// file_put_contents('result.xml', $result, FILE_APPEND);

 		if(curl_errno($handle)) {
 			throw new Exception(curl_error($handle), curl_errno($handle));
	    }

	    return $result;
 	}
}

?>