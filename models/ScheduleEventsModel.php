<?php

	require_once('RecivaModel.php');
	require_once('ScheduleModel.php');
	require_once('ScheduleGroupModel.php');

	class ScheduleEventsModel {

		static function Fetch($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_event_list_v2', $parameters, $credentials);	

			$xml = simplexml_load_string ($response);

			if ($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} 
			else {
				$events = array();
				foreach($xml->scheduler->xpath('//scheduler//eventList/event') as $event) {
					$events[] = current($event->attributes());
				}

				self::ArraySortByColumn($events, 'sort');

				$data = array();
				foreach($events as $event) {
					$military_time = str_pad($event['start_hr'], 2, '0', STR_PAD_LEFT)  . ':' . str_pad($event['start_min'], 2, '0', STR_PAD_LEFT) . ':00';
					$event['event_time'] = date('h:i A', strtotime($military_time));
					$data[] = $event;
				}

				foreach($data as $row) {
				    $result[$row['day_of_week']][] = $row;
				}
			}

			return $result;
		}

		static function Add($parameters, $credentials) {
			
			$result = array();
			$xml = '<combinedList>';
			$parameters = get_magic_quotes_gpc() ? stripslashes($parameters) : $parameters;
			$schedule = json_decode($parameters);

			$properties = array('description'=>$schedule->description, 'note'=>$schedule->note, 'obsolete'=>$schedule->obsolete);
			$response = self::GetScheduleId($properties, $credentials);

			if(!empty($response['schedule_id'])) {

				$schedule->id = $response['schedule_id'];

				// Associate group to schedule
				if(!empty($schedule->radiogroup_id) && $schedule->radiogroup_id != 0) {
					error_log("MY SCHEDULE ID IS: ". $schedule->radiogroup_id);
					$parameters = array('schedule_id'=>$schedule->id, 'radiogroup_id'=>$schedule->radiogroup_id);
					$response = ScheduleGroupModel::Associate($parameters, $credentials);
					if(!empty($response['id'])){
						$result['associated'] = $response['id'];
					}
				}

				if(!empty($schedule->additions)) {
					$events = self::Format($schedule, 'additions');
					$xml .= self::GetEventsXML($events, 'add');
				}
				
				$xml .= '</combinedList>';
				$parameters = array('schedule_id'=>$schedule->id);

				$response = RecivaModel::DoSchedulerXMLAction('schedule_event_batch_v2', $xml, $parameters, $credentials);	

				if($response['status'] === 'success') {
					$result['added'] = $response['eventAdd']['count'];
				} else {
					$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
				}
			}

			return $result;
		}

		static function Update($parameters, $credentials) {
			
			$result = null;
			$xml = '<combinedList>';
			$parameters = get_magic_quotes_gpc() ? stripslashes($parameters) : $parameters;
			$schedule = json_decode($parameters);

			/* DELETES */
			if(!empty($schedule->deletions)) {
				foreach ($schedule->deletions as $deletion) {
					$events[] = array('schedule_id'=>$schedule->id, 'event_id'=>$deletion);			
				}
				$xml .= self::GetEventsXML($events, 'delete');		
			}

			/* UPDATES */
			if(!empty($schedule->updates)) {
				$events = self::Format($schedule, 'updates');
				$xml .= self::GetEventsXML($events, 'update');			
			}

			/* ADDITIONS */
			if(!empty($schedule->additions)) {
				$events = self::Format($schedule, 'additions');			
				$xml .= self::GetEventsXML($events, 'add');
			}

			$xml .= '</combinedList>';

			$parameters = array('schedule_id'=>$schedule->id);
			
			$response = RecivaModel::DoSchedulerXMLAction('schedule_event_batch_v2', $xml, $parameters, $credentials);		

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('deleted'=>$response['eventDelete']['count'], 'added'=>$response['eventAdd']['count'], 'updated'=>$response['eventUpdate']['count']);
			}

			return $result;
		}

		static function Format($schedule, $type) {

			for($dow=0; $dow<count($schedule->{$type}); $dow++) {

				if(!empty($schedule->{$type}[$dow])) {

					foreach ($schedule->{$type}[$dow] as $index => $entry) {
						$event = new stdClass();
						// set properties
						$event->sort = $entry->sort;
						$event->day_of_week = $dow;	
						$event->schedule_id = $schedule->id;
						$event->obsolete = $schedule->obsolete;
						$event->interval_type = $schedule->interval_type;
						// set the event id if updating
						if($type == 'updates') {
							$event->event_id = $entry->event_id;		
						}
						// event times
						$event->start_hr = substr($entry->event_time, 0, 2);
						$event->start_min =  substr($entry->event_time, 2, 2);
						// event commands

						if(empty($entry->stn) || empty($entry->vol)) {
							$event->event_cmd1 = 'CMD|STANDBY|ON||2|';
						} else {
							$event->event_cmd1 = 'CMD|VOL|'. $entry->vol .'||2|';	
							$pieces = explode("|", $entry->stn);

							if(count($pieces) > 1) {
								$event->event_cmd2 = 'TLI|'.$pieces[0].'|'.$pieces[1].'||1|';		
							} else {
								$event->event_cmd2 = 'SXM|'.$entry->stn.'|||1|';						
							}
						}

						$events[] = $event;
					}
				}
			}

			return $events;
		}

		static function GetScheduleId($parameters, $credentials) {
			return ScheduleModel::Add($parameters, $credentials);
		}

		static function GetEventsXML($events, $type) {
			$xml = '';	
			foreach ($events as $event) {
				$xml .= "<${type}Event";
				foreach ($event as $key=>$value) {
					$xml .= " $key=\"$value\"";
				}
				$xml .= '/>';
			}
			return $xml;
		}

		static function ArraySortByColumn(&$array, $col, $dir = SORT_ASC) {
		    $sort_col = array();
		    foreach ($array as $key=> $row) {
		        $sort_col[$key] = $row[$col];
		    }
		    array_multisort($sort_col, $dir, $array);
		}		

	}

?>