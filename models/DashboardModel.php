<?php

	require_once('RecivaModel.php');

	class DashboardModel {

		/***
			FOLDERS: LISTS
		***/

		static function AssignRadioByStaus($radio, &$activeRadios, &$inactiveRadios) {
	    	if($radio['obsolete'] === 'false') {
	    		array_push($activeRadios, $radio['radioserial']);
	    	} else {
				array_push($inactiveRadios, $radio['radioserial']);
	    	}				
		}

		static function GetStats($parameters, $credentials) {
			if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) || in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])) {
				return self::GetSXMStats($parameters, $credentials);
			} else {
				return self::GetGDIStats($parameters, $credentials);
			}
		}

		static function GetGDIStats($parameters, $credentials) {

			$result = array();

			// Get User Privileges - Radio Counts
 			$response = RecivaModel::DoSimpleSchedulerAction('user_subaccount_check_login', $parameters, $credentials);

			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=>(string)$xml->scheduler->errorCode, 'message'=>(string)$xml->scheduler->message);
			} else {	
				$result = current($xml->scheduler->userPrivilegeList->attributes());
			}		

			return $result;
		}


		static function GetSXMStats($parameters, $credentials) {

			$result = array();
			$result['total'] = 0;
			$result['active'] = 0;
			$result['inactive'] = 0;
			$serial = 'radioserial';
			$count = 'count';

			// Get Radio Count
 			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_list', $parameters, $credentials);
	
			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=>(string)$xml->scheduler->errorCode, 'message'=>(string)$xml->scheduler->message);
			} else {	
				foreach ($xml->scheduler->siriusUserdetailList->siriusUser as $user) {
					is_null($user->attributes()->$serial) ? $result['inactive']++ : $result['active']++;
				}			
				$result['total'] = $xml->scheduler->siriusUserdetailList->attributes()->$count;
			}		
			return $result;
		}


	}
?>