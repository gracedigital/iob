<?php

	include_once 'RecivaModel.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/helpers/helper.user.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/helpers/helper.meta.php';
	include_once $_SERVER['DOCUMENT_ROOT'].'/application/configs/site-config.php';

	class AccountModel {

		static function AddUser($parameters, $credentials) {
		
			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_add', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('id'=>$response['userAdd']['username_id']);
			}
		
			echo json_encode($result);
		}

		static function GetUsers($credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_list', array(), $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {

				$iTotal = $response['userList']['usercount'];
				$result = array(
					"sEcho" => intval(1),
					"iTotalRecords" => intval($iTotal),
					"iTotalDisplayRecords" => intval($iTotal),
					"aaData" => array()
				);

				if($iTotal > 1) {
					foreach($response['userList']['user'] as $listEntry) {
						$result['aaData'][] = array(null, $listEntry['username_id'], $listEntry['username'], null, $listEntry['notes'], null, null, null);
					}
				} elseif ($iTotal == 1) {
					$listEntry = $response['userList']['user'];
					$result['aaData'][] = array(null, $listEntry['username_id'], $listEntry['username'], null, $listEntry['notes'], null, null, null);
				}
				
			}			
			echo json_encode($result);
		}

		static function GetChildUsers($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_list', $parameters, $credentials);	
			
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = $response;
			}			
			return $result;
		}

		static function UpdateUser($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_update', $parameters, $credentials);	

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			echo json_encode($result);
		}

		static function Permit($parameters) {

			$result = array();

			// use input parameters to do a login verification - if valid then assign the user privlilages
			$account = array('username'=>$parameters['subaccount_username'], 'password'=>$parameters['subaccount_password']);
			$response = RecivaModel::DoSchedulerAction('user_subaccount_check_login', $account, $account);

			if($response['status'] == 'success') {
				
				// can only assign privilages under an account with 'BUS_IO_REGISTRATION' privilage, e.g. iob, elm, sxm 
				$credentials = UserHelper::GetParentCredentials($parameters['info1']);

				// if parent is siriusxm
				if(strrpos($credentials['username'], NAME_SXM) !== false) {
					$privileges = array("SIRIUSUSERS_LIST", "SCH_ALL", "RG_ALL", "USERS_ALL", "TL_ALL", "TLA_ALL", "TLE_ALL");
				} else {
					$privileges = array("SCH_ALL", "RG_ALL", "USERS_ALL", "TL_ALL", "TLA_ALL", "TLE_ALL");
				}
				
				foreach ($privileges as $privilege) {
					$parameters['access_level'] = $privilege;
					$response = RecivaModel::DoSchedulerAction('user_subaccount_priv_add', $parameters, $credentials);
					if($response['status'] != 'success') {
						$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
						break;
					}
				}
	
				$result = $account;

			}

			return $result;		
		}

		static function Register($parameters) {

			$result = array();
			$credentials = UserHelper::GetParentCredentials($parameters['info1']);
			$response = RecivaModel::DoSchedulerAction('user_subaccount_add', $parameters, $credentials);	

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = $response;
			}

			return $result;			
		}
	}

?>