<?php

	require_once('RecivaModel.php');

	class CredentialModel {
		/***
			GET ALL CREDENTIALS for a SXM dealer
				@param 	mixed 	serial (optional) 			- define if you wish the details for just one radio serial
				@param 	mixed 	radiogroup_id (optional) 	- only looked for if 'serial' is not defined
				@param 	mixed 	expand (optional) 			- if defined and set to true then will return the description associated with the radio group			
	    ***/
		static function Fetch($parameters, $credentials) {

			$result = array();
			
			$parameters = array('expand'=>'true');

			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				foreach($xml->scheduler->siriusUserdetailList->siriusUser as $user) {
					$result[] = current($user->attributes());
				}
			}
			
			return $result;
		}		

		static function FetchFiltered($parameters, $credentials, $format, $filter) {
			$result = null;
			
			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);			
			} else {
				switch ($format) {
					case FORMAT_LIST:
						$result = '';
						foreach($xml->scheduler->siriusUserdetailList->siriusUser as $element) {	
							$result .= '<option value="'.$element['radiogroup_id'].'">'.urldecode(stripslashes($element['description'])).'</option>';
						}	
						break;
					case FORMAT_TABLE:
						$cols = array(null);
						$total = (string)$xml->scheduler->siriusUserdetailList['count'];
						$result = array("sEcho" => intval(1), "iTotalRecords" => intval($total), "iTotalDisplayRecords" => intval($total), "aaData" => array());

						foreach($xml->scheduler->siriusUserdetailList->siriusUser as $user) {	
							foreach ($user->attributes() as $key => $value) {
								if (in_array($key, $filter) ) {
									$cols[] = (string)$value;
								}
							}
							// xxx-hack - reciva doesn't return the description field if its empty
							if(count($cols) < 4) $cols[] = '';

							$result['aaData'][] = $cols;
							$cols = array(null);
						}
					break;
				}
			}
			return $result;
		}

		/***
			ASSOCIATE credentials with a device
				@param 	mixed 	sirius_userdetail_username (required)
				@param 	mixed 	sirius_userdetail_password (required)
				@param 	mixed 	radiogroup_id (required)
				@param 	mixed 	description (optional)
	    ***/
		static function Register($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}

	    /***
			MOVE credential
				@param mixed sirius_userdetail_id (required)
				@param mixed sirius_userdetail_username (required)
				@param mixed new radiogroup_id (required)
	    ***/
		static function Move($parameters, $credentials) {
			$result = array();

			foreach ($parameters['list'] as $parameter) {
				$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_update', $parameter, $credentials);

				$xml = simplexml_load_string ($response);

				if($xml->scheduler->status != 'success') {
					$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
				} else {
					$result['success'][] = $parameter['sirius_userdetail_username'];
				}
			}

			return $result;
		}

	    /***
			UPDATE credential
				@param mixed sirius_userdetail_id (required)
				@param mixed sirius_userdetail_username (required)
				@param mixed new radiogroup_id (required)
				@param mixed description (optional)
	    ***/
		static function Update($parameters, $credentials) {
			
			$result = array();
			
			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_update', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}

		/***
			DELETE credential
				@param mixed sirius_userdetail_username
				@param mixed sirius_userdetail_id
	    ***/
		static function Delete($parameters, $credentials) {
			
			$result = array();
			
			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_delete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}
	}

?>