<?php

	require_once('RecivaModel.php');

	class GroupModel {

		static function Associate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);	
			} else {
				$result['data'] = (string)$xml->scheduler->scheduleRadiogroupAdd['id'];
			}

			return $result;
		}		

		static function Associated($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);	
			} else {
				foreach ($xml->scheduler->scheduleRadiogroupList->schedule_radiogroup as $node) {
					$result[] = current($node->attributes());
				}
			}

			return $result;
		}		

		static function Unssociated($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_list_free_schedules', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);	
			} else {
				$data = '';
				foreach ($xml->scheduler->scheduleRadiogroupFreeList->schedule as $schedule) {
					$data .= '<option value="'.(string)$schedule['schedule_id'].'">'.(string)$schedule['description'].'</option>';
				}
				$result['data'] = $data;
			}

			return $result;
		}

		static function Add($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);	
			}

			return $result;
		}

		static function Fetch($parameters, $credentials, $format) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				switch ($format) {
					case FORMAT_LIST:
						$data = '';
						foreach($xml->scheduler->radiogroupList->radiogroup as $element) {	
							$data .= '<option value="'.$element['radiogroup_id'].'">'.urldecode(stripslashes($element['description'])).'</option>';
						}
						if (strlen($data) == 0) $data = '<option value=""></option>';
						break;
					
					default:
						$data = array();
						foreach($xml->scheduler->radiogroupList->radiogroup as $element) {	
							$data[] = current($element->attributes());
						}		
				}
				$result = $data; // array('data'=>$data);
			}

			return $result;
		}


		static function Delete($parameters, $credentials) {
			
			$result = array();

			$parameters['check'] = 'true';
	
			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_delete', $parameters, $credentials);
			
			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}


		static function Update($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_update', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}


		static function DeleteMember($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_delete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}
			
			return $result;
		}

		static function AddMember($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('radiogroup_member_add', $parameters, $credentials);	

			$id = 'radiogroup_member_id';

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				$result['radiogroup_member_id'] = (string)$xml->scheduler->radiogroupMemberAdd->attributes()->$id;
			}
			
			return $result;


		}

	}

?>