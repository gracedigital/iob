<?php

	require_once('RecivaModel.php');
	require_once($_SERVER['DOCUMENT_ROOT'].'/application/configs/app-config.php');

	class UserModel {

		static function Add($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_add', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('id'=>$response['userAdd']['username_id']);
			}
		
			echo json_encode($result);
		}

		static function Delete($parameters, $credentials) {
			
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('user_subaccount_delete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}
			
			return $result;
		
			echo json_encode($result);
		}

		static function GetChildUsers($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_list', $parameters, $credentials);	

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = $response;
			}

			return $result;
		}

		static function Update($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('user_subaccount_update', $parameters, $credentials);	
			
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}
		
			echo json_encode($result);
		}

		static function Login($parameters, $credentials) {

			$result = array();

 			$response = RecivaModel::DoSimpleSchedulerAction('user_subaccount_check_login', $parameters, $credentials);

			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=>(string)$xml->scheduler->errorCode, 'message'=>(string)$xml->scheduler->message);
			} else {	
				
				$info = (string)$xml->scheduler->userPrivilegeList['info1'];
				
				$stats = array();
				$privileges = array();

				if(in_array($info, unserialize(COMPANY_CODES))) {
					foreach ($xml->scheduler->userPrivilegeList->privilege as $privilege) {
						$privileges[] = (string)$privilege;
					}
					$result['privileges'] = $privileges;
					$result['stats'] = current($xml->scheduler->userPrivilegeList->attributes());
				} else {
					$result['error'] = array('code' => 666, 'message' => 'You do not have privileges to login to this site!');
				}
			}

			return $result;			
		}

		static function Fetch($parameters, $credentials) {

			$result = array();
			$users = array();
			$childusers = array();
			$response = RecivaModel::DoSchedulerAction('user_subaccount_list', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$count = $response['userList']['usercount'];
				$childcount = $response['userList']['childcount'];

				if($count == 1) {
					$users[] = $response['userList']['user'];
				} elseif ($count == 1) {
					$users = $response['userList']['user'];
				}
				$result['users'] = $users;

				if($childcount == 1) {
					$childusers[] = $response['userList']['childuser'];
				} elseif ($childcount > 1) {
					$childusers = $response['userList']['childuser'];
				}
				$result['childusers'] = $childusers;				

			}

			return $result;
		}


	}
?>