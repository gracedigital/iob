<?php

	require_once(MODELS.'/RecivaModel.php');

	class ChannelModel {

		static function Add($parameters, $credentials) {

		    $result = array();

			$response = RecivaModel::DoSimpleTailoredAction('list_entry_add', $parameters, $credentials);
			
			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			}

			return $result;
		}


		static function Unlink($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleTailoredAction('list_entry_delete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			}
			
			return $result;
		}


		static function Update($parameters, $credentials) {
			
		    $result = array();

			$response = RecivaModel::DoSimpleTailoredAction('list_entry_update', $parameters, $credentials);
			
			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {				
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			}

			return $result;
		}


		static function Nested($parameters, $credentials) {

			$result = null;

			$response = RecivaModel::DoSimpleTailoredAction('list_entry_list', $parameters, $credentials);
		
			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			} else {

				$result = '<option></option>';
				foreach ($xml->tailoredList->listListEntry as $listListEntry) 
				{		

					$list_id = $listListEntry->attributes()->list_id;
					$title = $listListEntry->attributes()->title;
					$result .= '<optgroup label="'. $title.'">';

					foreach ($listListEntry->listEntry as $listEntry) 
					{
						if(empty($listEntry->subEntryList)) {
							$result .= '<option value="' . $list_id .'|'. urldecode($listEntry->attributes()->list_entry_id).'">'. urldecode($listEntry->attributes()->title) .'</option>';
						} else {
							$title = $listEntry->attributes()->title;
							$result .= '<optgroup label="'. $title.'">';

							if(!is_null($listEntry->subEntryList->listEntry)) 
							{
								foreach ($listEntry->subEntryList->listEntry as $listEntry) 
								{
									$result .= '<option value="' . $list_id .'|'. urldecode($listEntry->attributes()->list_entry_id).'">'. urldecode($listEntry->attributes()->title) .'</option>';
								}
							}

							$result .= "</optgroup>";
						}
					}

					$result .= "</optgroup>";	
				}
			}

			return $result;
		}


		static function FetchSXM($parameters, $credentials) {

			$result = null;

			$parameters = array('sirius_userdetail_username'=>SXM_USERNAME, 'sirius_userdetail_password'=>SXM_PASSWORD);
			
			$response = RecivaModel::DoSimpleSchedulerAction('sirius_userdetail_list_channels', $parameters, $credentials);

			$xml = simplexml_load_string($response);
		
			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				
				$result = '<option></option>';

				foreach($xml->scheduler->stationlist as $stationlist) 
				{
					$result .= '<optgroup label="'. urldecode($stationlist->attributes()->name).'">';

						$node = $stationlist->children();
						if($node[0]->getName() == 'station') 
						{
							foreach ($stationlist->children() as $station) {
								$result .= '<option value="'. urldecode($station->attributes()->channel_no).'">'. urldecode($station->attributes()->title) .'</option>';
							}
						} else {
							foreach ($stationlist->children() as $stationlist) 
							{
								$result .= '<optgroup label="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. urldecode($stationlist->attributes()->name).'">';

								foreach ($stationlist->station as $station) 
								{
									$result .= '<option value="'. urldecode($station->attributes()->channel_no).'">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'. urldecode($station->attributes()->title) .'</option>';
								}

								$result .= '<optgroup>';
							}						
						}
				
					$result .= "</optgroup>";
				}
			}

			return $result;
		}

		static function Fetch($parameters, $credentials, $format) {

			$result = null;

			$response = RecivaModel::DoSimpleTailoredAction('list_entry_list', $parameters, $credentials);
			
			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			} else {				
				switch ($format) {
					case FORMAT_LIST:
						$result = '';
						if(!is_null($xml->tailoredList->listListEntry->listEntry)) {
							$list_id = $xml->tailoredList->listListEntry['list_id'];
							foreach($xml->tailoredList->listListEntry->listEntry as $element) {		
								$result .= '<option value="'.$list_id.'|'.$element['list_entry_id'].'">'.urldecode(stripslashes($element['title'])).'</option>';
							}
						}
						break;
						
					default:
						$result = array();
						foreach($xml->tailoredList->listListEntry->listEntry as $element) {		
							$result[] = current($element->attributes());
						}
				}

			}
		
			return $result;
		}
	}
?>