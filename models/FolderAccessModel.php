<?php

	require_once('RecivaModel.php');

	class FolderAccessModel {

		static function Add($parameters, $credentials) {

			$result = array();
			$response = RecivaModel::DoTailoredAction('list_access_add', $parameters, $credentials);				

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			echo json_encode($result);
		}

		static function Delete($parameters, $credentials) {

			$result = array();
			$response = RecivaModel::DoTailoredAction('list_access_delete', $parameters, $credentials);				
		
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} 

			echo json_encode($result);
		}
	}
?>