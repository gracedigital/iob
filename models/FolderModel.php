<?php

	require_once('RecivaModel.php');

	class FolderModel {


		// AJAX METHODS //
		static function Add($parameters, $credentials) {

		    $result = array();
			$response = RecivaModel::DoTailoredAction('list_add', $parameters, $credentials);				
			
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('list_id'=>$response['addList']['list_id']);
			}

			echo json_encode($result);
		}

		static function Update($parameters, $credentials) {

		    $result = array();
			$response = RecivaModel::DoTailoredAction('list_update', $parameters, $credentials);				
	
			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			echo json_encode($result);
		}

		static function Delete($parameters, $credentials) {

		    $result = array();
			$response = RecivaModel::DoTailoredAction('list_delete', $parameters, $credentials);				

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			return $result;
		}
		// END AJAX METHODS //


		static function Fetch($parameters, $credentials, $format) {

			$result = null;

			$response = RecivaModel::DoSimpleTailoredAction('list_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->tailoredList->errorCode, 'message'=> (string)$xml->tailoredList->message);
			} else {
				switch ($format) {
					case FORMAT_LIST:
						$result = '';
						foreach($xml->tailoredList->listList->list as $element) {		
							$result .= '<option value="' . $element->attributes()->list_id .'">' . urldecode(stripslashes($element->attributes()->title)) . '</option>';
						}							
						break;
					default:
						$result = array();
						foreach($xml->tailoredList->listList->list as $element) {		
							$result[] = current($element->attributes());
						}
				}

			}
			return $result;
		}


		/***
			GET ALL RADIOS assigned to tailored lists folder
	    ***/
		static function Assigned($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleTailoredAction('list_access_list', $parameters, $credentials);	
			$xml = simplexml_load_string ($response);

			if($xml->tailoredList->status != 'success') {
				throw new Exception($xml->tailoredList->message, $xml->tailoredList->errorCode);
			} else {
				foreach($xml->tailoredList->listAccessList->listAccess as $element) {	
					$result[] = current($element->attributes());
				}	
			}

			return $result;
		}	

	}
?>