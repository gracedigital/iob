<?php

	require_once('RecivaModel.php');

	class ScheduleGroupModel {

		/*
			Get groups associated to a schedule
			- Returns array
		*/
		static function Associated($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_list', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				foreach($xml->scheduler->scheduleRadiogroupList->schedule_radiogroup as $element) {	
					$result[] = current($element->attributes());
				}	
			}
			
			return $result;
		}

		/*
			Get groups not associated to a schedule
			- Returns html option list
		*/
		static function Unassociated($parameters, $credentials) {		

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_list', $parameters, $credentials);

			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {

				$myGroups = array();
				$allGroups = array();
				$data = '<option></option>';
				
				foreach($xml->scheduler->radiogroupList->radiogroup as $element) {	
					$allGroups[(int)$element->attributes()->radiogroup_id] = $element->attributes()->description;
				}
						
				$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_list', $parameters, $credentials);

				$xml = simplexml_load_string ($response);

				if($xml->scheduler->status != 'success') {
					$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
				} else {
					foreach($xml->scheduler->scheduleRadiogroupList->schedule_radiogroup as $element) {	
						$myGroups[(int)$element->attributes()->radiogroup_id] = $element->attributes()->radiogroup_description;
					}	

					$array = array_diff_assoc($allGroups, $myGroups);	

					foreach ($array as $key => $value) {
						$data .= "<option value='$key'>$value</option>";
					}

					$result['data'] = $data;					
				}
			}

			return $result;
		}
		
		/*
			Associate a group to a schedule
		*/
		static function Associate($parameters, $credentials) {
			
			$id = 'id';
			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_add', $parameters, $credentials);
	
			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				$result['id'] = (string)$xml->scheduler->scheduleRadiogroupAdd->attributes()->$id;
			}

			return $result;
		}

		/*
			Disassocaiate a group from a schedule
		*/
		static function Disassociate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_radiogroup_delete', $parameters, $credentials);
			
			$xml = simplexml_load_string($response);
			
			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}



	}
?>