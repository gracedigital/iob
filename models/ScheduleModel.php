<?php

	require_once('RecivaModel.php');

	class ScheduleModel {

		static function Add($parameters, $credentials) {

			$result = array();			

			$id = 'schedule_id';
			
			$response = RecivaModel::DoSimpleSchedulerAction('schedule_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				$result['schedule_id'] = (string)$xml->scheduler->scheduleAdd->attributes()->$id;
			}

			return $result;
		}

		static function Delete($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_delete', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}

		static function Update($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_update', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			}

			return $result;
		}		

		static function Fetch($parameters, $credentials, $format) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('schedule_list', $parameters, $credentials);

			$xml = simplexml_load_string($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				switch ($format) {
					case FORMAT_LIST:
						$data = '';
						foreach($xml->scheduler->scheduleList->schedule as $schedule) {	
							$data .= '<option value="' . $schedule['schedule_id']	. '">' . $schedule['description'] . '</option>';
						}
						break;
					default:
						$data = array();
						foreach($xml->scheduler->scheduleList->schedule as $schedule) {	
							$data[] = current($schedule->attributes());
						}	
				}

				$result = $data; // array('data'=>$data);
			}

			return $result;
		}
		
	}
?>