<?php

	require_once('RecivaModel.php');

	class RadioGroupModel {

	    /*
			Add a radio to a group
	    */
		static function Associate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_add', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				$result['data'] = (string)$xml->scheduler->radiogroupMemberAdd['radiogroup_member_id'];
			}

			return $result;
		}		


	    /*
			Remove a radio from a group
	    */
		static function Disassociate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoSchedulerAction('radiogroup_member_delete', $parameters, $credentials);		

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			return $result;
		}


	    /*
			Get radios assocated to a group
	    */
		static function Associated($parameters, $credentials) {

			$result = array();
			$serial = $parameters['serial'];
  			$parameters = array('serial'=>$serial,'expand'=>'radiogroup');			
			
			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_list', $parameters, $credentials);				
			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {	
				if(!is_null($xml->scheduler->radioserialList->radio->radiogroupList->radiogroup)) {	
					foreach($xml->scheduler->radioserialList->radio->radiogroupList->radiogroup as $element) {
						$result[] = current($element->attributes());		
					}
				}
			}

			return $result;
		}


	    /*
			Get groups not associated to a radio serial#
	    */
		static function Unassociated($parameters, $credentials) {		

 			$result = null;

 			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_list_free_groups', $parameters, $credentials);

			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {	
				$data = '<option></option>';
				if(!is_null($xml->scheduler->radiogroupList->radiogroup)) {
	 				foreach($xml->scheduler->radiogroupList->radiogroup as $element) {
	 					$data .= '<option value="'.(string)$element['radiogroup_id'].'">'.urldecode(stripslashes($element['description'])).'</option>';
	 				}
	 			}
	 			$result['data'] = $data;
 			}

 			return $result;
		}	

		static function Fetch($parameters, $credentials, $format) {

			$result = null;

			$response = RecivaModel::DoSimpleSchedulerAction('radiogroup_member_list', $parameters, $credentials);

			$string = str_replace('&', '&amp;', $response); 
			$xml = simplexml_load_string($string);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);			
			} else {

				switch ($format) {
					case FORMAT_LIST:
						$list = '';
						foreach($xml->scheduler->radiogroupMemberList->radiogroup_member as $element) {	
							$list .= '<option value="'.$element['radiogroup_id'].'">'.urldecode(stripslashes($element['name'])).'</option>';
						}	
						$result = (array)$list;
						break;

					case FORMAT_TABLE:
						$total = $xml->scheduler->radiogroupMemberList['count'];
						$result = array("sEcho" => intval(1), "iTotalRecords" => intval($total), "iTotalDisplayRecords" => intval($total), "aaData" => array());
						foreach($xml->scheduler->radiogroupMemberList->radiogroup_member as $element) {	
							$result['aaData'][] = array(null, (string)$element['radiogroup_member_id'], (string)$element['radioserial'], (string)$element['name']);
						}
						break;

					default:
						$result = array();
						foreach($xml->scheduler->radiogroupMemberList->radiogroup_member as $element) {	
							$result[] = current($element->attributes());
						}
				}
			}
			return $result;
		}

	}
?>