<?php

	require_once('RecivaModel.php');
	require_once('FolderModel.php');

	class RadioFolderModel {

		/*
			Get all lists associted to a radio serial#
		*/       
		static function Associated($parameters, $credentials) {

			$result = array();
			$serial = $parameters['serial'];
  			$parameters = array('serial'=>$serial,'expand'=>'tailoredlist');			
		
			$response = RecivaModel::DoSimpleSchedulerAction('user_radio_list', $parameters, $credentials);		
			$xml = simplexml_load_string ($response);

			if($xml->scheduler->status != 'success') {
				$result['error'] = array('code'=> (string)$xml->scheduler->errorCode, 'message'=> (string)$xml->scheduler->message);
			} else {
				foreach($xml->scheduler->xpath('//scheduler/radioserialList/radio/tailoredListList/list') as $list) {
					$result[] = array('serial'=>$serial, 'list_id'=>(string)$list['list_id'], 'title'=>stripslashes((string)$list['title']));		
				}
			}

			return $result;
		}


		/*
			Get folders not associated to a radio serial#
	    */
		static function Unassociated($parameters, $credentials) {


			$result = null;
			$my_lists = array();
			$all_lists_titles = array();

			// Sort my lists
			$response = self::Associated($parameters, $credentials);
			if(empty($response['error'])) {
				foreach ($response as $list) {	
					$my_lists[$list['list_id']] = '';
				}
				ksort($my_lists);

				// Sort all lists
				$response = FolderModel::Fetch($parameters, $credentials, FORMAT_ARRAY);
				if(empty($response['error'])) {


					foreach($response as $list) {
						$all_lists_titles[$list['list_id']] = $list['title'];
					}

					ksort($all_lists_titles);

					$result = '';
					// Return the ids and titles of the lists this serial# doesn't belong to
					if(!empty($my_lists)) {
						$array = array_diff_key($all_lists_titles, $my_lists);	
						foreach ($array as $key=>$value) {
							$result .= '<option value="'.$key.'">'.urldecode(stripslashes($value)).'</option>';
						}
					} else {
						foreach ($all_lists_titles as $key=>$value) {
							$result .= '<option value="'.$key.'">'.urldecode(stripslashes($value)).'</option>';
						}				
					}

					if(is_null($result) || strlen($result) == 0) {
						$result = 'There are no unlinked lineups!';
					}
				}
			}

			return $result;
		}	


	    /***
			ADD RADIO to a specific folder
	    ***/
		static function Associate($parameters, $credentials) {
		
			$result = array();

			$response = RecivaModel::DoTailoredAction('list_access_add', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			} else {
				$result = array('status'=>0, 'list_id'=>$response['addListAccess']['listId']);
			}

			return $result;
		}


		static function Disassociate($parameters, $credentials) {

			$result = array();

			$response = RecivaModel::DoTailoredAction('list_access_delete', $parameters, $credentials);

			if($response['status'] != 'success') {
				$result['error'] = array('code' => $response['errorCode'], 'message' => $response['message']);
			}

			return $result;
		}


	}
?>