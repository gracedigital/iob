<?php
	
	// NAMES
	DEFINE("NAME_GDI", "gdi");
	DEFINE("NAME_SXM", "sxm");
	DEFINE("NAME_SIRIUS", "sirius");

	// META
	DEFINE("LOGO_PATH", "assets/img/logo/");
	DEFINE("LOGO_GDI", "assets/img/logo/gdi-logo-big.png");
	DEFINE("LOGO_SXM", "assets/img/logo/sxm-logo-big.png");
	DEFINE("SITE_TITLE", "Grace Digital Music Portal"); 
	DEFINE("SITE_COPYRIGHT", date("Y")." &copy; Grace Digital Inc.");
	DEFINE("SITE_URL", "http://$_SERVER[SERVER_NAME]:$_SERVER[SERVER_PORT]");

	// LABELS
	DEFINE("LABEL_START", "Get Started");
	DEFINE("LABEL_MONDAY", "Monday");
	DEFINE("LABEL_TUESDAY", "Tuesday");
	DEFINE("LABEL_WEDNESDAY", "Wednesday");
	DEFINE("LABEL_THURSDAY", "Thursday");
	DEFINE("LABEL_FRIDAY", "Friday");
	DEFINE("LABEL_SATURDAY", "Saturday");
	DEFINE("LABEL_SUNDAY", "Sunday");
	DEFINE("LABEL_URL", "URL");
	DEFINE("LABEL_HOME", "Home");
	DEFINE("LABEL_NAME", "Name");
	DEFINE("LABEL_NOTE", "Note");
	DEFINE("LABEL_DASHBOARD", "Dashboard");
	DEFINE("LABEL_DB_RADIO_TOTAL", "Total Radios");
	DEFINE("LABEL_DB_RADIO_ACTIVE", "Active Radios");
	DEFINE("LABEL_DB_RADIO_INACTIVE", "Inactive Radios");
	DEFINE("LABEL_DESCRIPTION", "Description");
	DEFINE("LABEL_ACTIVE", "Active");
	DEFINE("LABEL_INACTIVE", "Inactive");
	DEFINE("LABEL_SCHEDULES", "Schedules");
	DEFINE("LABEL_RADIO", "Radio");
	DEFINE("LABEL_RADIOS", "Radios");
	DEFINE("LABEL_SCHEDULE", "Schedule");
	DEFINE("LABEL_USERNAME", "Username");
	DEFINE("LABEL_PASSWORD", "Password");
	DEFINE("LABEL_CREDENTIALS", "Credentials");	
	DEFINE("LABEL_CHANNELS", "Channels");
	DEFINE("LABEL_CHANNEL", "Channel");
	DEFINE("LABEL_LINEUP", "Lineup");
	DEFINE("LABEL_LINEUPS", "Lineups");
	DEFINE("LABEL_LINEUP_ADD", "Add Lineup");
	DEFINE("LABEL_GROUP", "Group");	
	DEFINE("LABEL_GROUPS", "Groups");	
	DEFINE("LABEL_GROUP_ADD", "Add Group");	
	DEFINE("LABEL_GROUP_NAME", "Group Name");	
	DEFINE("LABEL_FOLDERS", "Folders");
	DEFINE("LABEL_FOLDER", "Folder");
	DEFINE("LABEL_USERS", "Users");
	DEFINE("LABEL_ADD", "Add");
	DEFINE("LABEL_EDIT", "Edit");
	DEFINE("LABEL_HELP", "Help");
	DEFINE("LABEL_EXPAND", "Expand");
	DEFINE("LABEL_MOVE", "Move");
	DEFINE("LABEL_VIEW", "View");
	DEFINE("LABEL_DELETE", "Delete");
	DEFINE("LABEL_REMOVE", "Remove");
	DEFINE("LABEL_REGISTER", "Register");
	DEFINE("LABEL_MANAGE", "Manage");
	DEFINE("LABEL_PROVISION", "Provision");
	DEFINE("LABEL_STATUS", "Status:");
	DEFINE("LABEL_RADIO_GROUP", "Radio Group");
	DEFINE("LABEL_SERIAL_NUMBER", "Serial Number");
	DEFINE("LABEL_REGISTRATION_KEY", "Registration Key");
	DEFINE("LABEL_NO_DATA", "No data available in table");
	DEFINE("LABEL_VIEW_MORE", "View More");
	DEFINE("LABEL_EDIT_SCHEDULE", "Schedule Editing");
	DEFINE("LABEL_COPY_SCHEDULE", "Copy this schedule to the entire week");
	DEFINE("LABEL_NOEVENTS_SCHEDULE", "No existing events were found for this day.");
	DEFINE("LABEL_DISABLE_SCHEDULE", "Disable schedule today.");
	DEFINE("LABEL_SCHEDULE_TIP", "*Stops the music until the next Opening");
	DEFINE("LABEL_BTN_COPY_SCHEDULE", "Copy this schedule to the entire week");
	DEFINE("LABEL_LOGIN_TERMS_GDI", "For use only with the Grace Digital Business radio model: GDI-IRBM20");
	DEFINE("LABEL_LOGIN_TERMS_SXM", "For use only with the Grace Digital SiriusXM Business radio model: GDI-SXMBR1");
	DEFINE("LABEL_SOURCE_GROUP", "Select a source group");
	DEFINE("LABEL_TARGET_GROUP", "Select a target group");

	// META TAGS
	DEFINE("META_DESCRIPTION", "Grace Digital Business Radio Management Portal.  For use only with the Grace Digital Business radio");
	DEFINE("META_LOGO_IMG_ALT_GDI", "Grace Digital Business Radio Management Portal.  For use only with the Grace Digital Business radio model: GDI-IRBM20");
	DEFINE("META_LOGO_IMG_ALT_SXM", "Grace Digital Business Radio Management Portal.  For use only with the Grace Digital Business radio model: GDI-SXMBR1");
	DEFINE("META_PRODUCT_IMG_ALT_GDI", "Grace Digital Business Radio Model: GDI-IRBM20");
	DEFINE("META_PRODUCT_IMG_ALT_SXM", "Grace Digital Business Radio Model: GDI-SXMBR1");


	// TITLES
	DEFINE("TITLE_DASHBOARD", "Dashboard <small>statistics and more</small>");
	DEFINE("TITLE_CHANNEL_ADD", "Add <small>a new channel or lineup</small>");
	DEFINE("TITLE_LINEUP_MANAGE", "Manage <small>your channels</small>");
	DEFINE("TITLE_LINEUP_SUBSCRIBED", "View <small>lineup associations to radios</small>");
	DEFINE("TITLE_USER_MANAGE", "Manage <small>users</small>");
	DEFINE("TITLE_SCHEDULE_ADD", "Add <small>a daily event schedule</small>");
	DEFINE("TITLE_SCHEDULE_EDIT", "Edit <small>a daily event schedule</small>");	
	DEFINE("TITLE_SCHEDULE_MANAGE", "Manage <small>a daily event schedule</small>");
	DEFINE("TITLE_GROUP_MANAGE", "Manage <small>groups</small>");
	DEFINE("TITLE_GROUP_ADD", "Add <small>a radio group</small>");
	DEFINE("TITLE_RADIO_PROVISION", "Provision <small>a new radio</small>");
	DEFINE("TITLE_RADIO_MANAGE", "Manage <small>radios</small>");
	DEFINE("TITLE_RADIO_MOVE", "Move <small>radios</small>");
	DEFINE("TITLE_CREDENTIAL_MANAGE", "Manage <small>SiriusXM credentials</small>");
	DEFINE("TITLE_CREDENTIAL_PROVISION", "Provision <small>SiriusXM credentials</small>");
	DEFINE("TITLE_LOGIN_SXM", "SiriusXM Business Radio Management Portal");
	DEFINE("TITLE_LOGIN_GDI", "Gracd Digital Business Radio Management Portal");

	// CAPTIONS
	DEFINE("CAPTION_USER_REGISTER", "Register User");
	DEFINE("CAPTION_USER_SUBACCOUNTS", "Child User Accounts");
	DEFINE("CAPTION_LINEUP_RELATED", "Lineups and related channels");
	DEFINE("CAPTION_LINEUP_LINKED", "Lineups and related radios");
	DEFINE("CAPTION_LINEUP_RADIOS", "Radios in this lineup");
	DEFINE("CAPTION_LINEUP_CHANNELS", "Channels in this lineup");
	DEFINE("CAPTION_SCHEDULE_DETAILS", "Schedule Details");
	DEFINE("CAPTION_SCHEDULE_GROUPS", "Groups");
	DEFINE("CAPTION_SCHEDULE_MANAGE", "Schedules");
	DEFINE("CAPTION_GROUP_MANAGE", "Groups");
	DEFINE("CAPTION_GROUP_DETAILS", "Group Details");
	DEFINE("CAPTION_GROUP_SCHEDULES", "Schedules");
	DEFINE("CAPTION_RADIO_DETAILS", "Radio Details");
	DEFINE("CAPTION_RADIO_MANAGE", "Radios");
	DEFINE("CAPTION_RADIO_LINEUP", "Lineups on this radio");
	DEFINE("CAPTION_RADIO_MOVE", "Move Radios Between Groups");
	DEFINE("CAPTION_CHANNEL_ADD", "Add New Channel");
	DEFINE("CAPTION_CREDENTIALS", "Credentials");
	DEFINE("CAPTION_CREDENTIALS_MOVE", "Move Credentials between Groups");
	DEFINE("CAPTION_PROVISION_DETAILS", "Provisioning Details");
	DEFINE("CAPTION_CREDENTIALS_TOTAL", "Total Credentials");
	DEFINE("CAPTION_CREDENTIALS_ACTIVE", "Active Credentials");
	DEFINE("CAPTION_CREDENTIALS_INACTIVE", "Inactive Credentials");
	DEFINE("CAPTION_LOGIN_GDI", "Business Radio Management Portal");
	DEFINE("CAPTION_LOGIN_SXM", "SiriusXM Business Radio Management Portal");


	// TABLE HEADERS
	DEFINE("TH_ID", "ID");
	DEFINE("TH_URL", "URL");
	DEFINE("TH_NAME", "Name");
	DEFINE("TH_DAYPART", "Day Part");
	DEFINE("TH_TIME", "Time");
	DEFINE("TH_GROUP", "Group");
	DEFINE("TH_CHANNEL", "Channel");
	DEFINE("TH_VOLUME", "Volume");
	DEFINE("TH_EDIT", "Edit");
	DEFINE("TH_UNLINK", "Unlink");
	DEFINE("TH_DELETE", "Delete");
	DEFINE("TH_SERIAL", "Serial");
	DEFINE("TH_STATUS", "Status");
	DEFINE("TH_TITLE", "Title");
	DEFINE("TH_REMOVE", "Remove");
	DEFINE("TH_LINEUP", "Lineup");
	DEFINE("TH_MODIFIED", "Modified");
	DEFINE("TH_USERNAME", "Username");
	DEFINE("TH_PASSWORD", "Password");
	DEFINE("TH_CHILDREN", "Children");			
	DEFINE("TH_DETAILS", "Details");
	DEFINE("TH_DESCRIPTION", "Description");

	// BUTTONS
	DEFINE("LABEL_BTN_ANOTHER", "Register Another");
	DEFINE("LABEL_BTN_CONTINUE", "Continue");
	DEFINE("LABEL_BTN_SUBMIT", "Submit");
	DEFINE("LABEL_BTN_CANCEL", "Cancel");
	DEFINE("LABEL_BTN_SAVE", "Save");
	DEFINE("LABEL_BTN_MOVE", "Move");
	DEFINE("LABEL_BTN_OK", "OK");

	// PLACEHOLDERS
	DEFINE("PH_OPEN", "Open");
	DEFINE("PH_CLOSE", "Close");
	DEFINE("PH_VOLUME", "Volume");
	DEFINE("PH_RADIO_KEY", "Get the key from the radio");
	DEFINE("PH_RADIO_NAME", "Enter a radio name");
	DEFINE("PH_SELECT_GROUP", "Choose a group...");
	DEFINE("PH_SELECT_LINEUP", "Choose a lineup...");
	DEFINE("PH_SELECT_LINEUPS", "Choose one or more lineups...");
	DEFINE("PH_SELECT_CHANNEL", "Choose a channel...");
	DEFINE("PH_SELECT_SCHEDULE", "Choose a schedule...");
	DEFINE("PH_CHANNEL_ENTER_URL", "Enter a URL for this channel");
	DEFINE("PH_CHANNEL_ENTER_NAME", "Enter a name for this channel");
	DEFINE("PH_LINEUP_ENTER_NAME", "Enter a name for this lineup");
	DEFINE("PH_SCHEDULE_ENTER_NAME", "Enter a name for this schedule");
	DEFINE("PH_SOURCE_GROUP", "Source Group");
	DEFINE("PH_TARGET_GROUP", "Target Group");
	DEFINE("PH_CREDENTIAL_USERNAME", "A SiriusXM Username");
	
	// TIPS
	DEFINE("TIP_RADIO_RENAME", "Rename this radio");
	DEFINE("TIP_RADIO_DELETE", "Delete this radio");
	DEFINE("TIP_EDIT_GROUP", "Edit this group");
	DEFINE("TIP_DELETE_GROUP", "Delete this group");
	DEFINE("TIP_ENTER_REGKEY", "Enter the registration key from the radio");

	// LEGEND
	DEFINE("LEGEND_TABLE", "Table Legend");
	DEFINE("LEGEND_VIEW", "View Radio Details");
	DEFINE("LEGEND_ACTIVATE", "Activate Radio");
	DEFINE("LEGEND_DEACTIVATE", "Deactivate Radio");
	DEFINE("LEGEND_EDIT", "Edit Radio Name");
	DEFINE("LEGEND_DELETE", "Delete Radio");
	DEFINE("LEGEND_NOTE", "Search is cAsE sensative!");
	DEFINE("LEGEND_ASSOCIATE", "Assocate Group or Lineup");

	// FORMATS
	DEFINE("FORMAT_LIST", "list");
	DEFINE("FORMAT_ARRAY", "array");
	DEFINE("FORMAT_TABLE", "table");

	// PRIVILEGES
	DEFINE ("RG_ALL", "RG_ALL");
	DEFINE ("TL_ALL", "TL_ALL");
	DEFINE ("SCH_ALL", "SCH_ALL");
	DEFINE ("TLA_ALL", "TLA_ALL");
	DEFINE ("TLE_ALL", "TLE_ALL");
	DEFINE ("USERS_ALL", "USERS_ALL");
	DEFINE ("SIRIUSUSERS_ALL", "SIRIUSUSERS_ALL");
	DEFINE ("SIRIUSUSERS_LIST", "SIRIUSUSERS_LIST");
	DEFINE ("BUS_IO_REGISTRATION", "BUS_IO_REGISTRATION");
?>