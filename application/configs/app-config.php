<?php
	// CORE SETTINGS
	DEFINE("DOC_ROOT", $_SERVER['DOCUMENT_ROOT']);
	DEFINE("CONTROLLERS", DOC_ROOT.'/controllers');
	DEFINE("APP_ROOT", DOC_ROOT.'/application');
	DEFINE("INCLUDES", DOC_ROOT.'/includes');
	DEFINE("HELPERS", DOC_ROOT.'/helpers');
	DEFINE("CONFIGS", APP_ROOT.'/configs');
	DEFINE("MODELS", DOC_ROOT.'/models');
	DEFINE("VIEWS", DOC_ROOT.'/views');
		
	// CORE INCLUDES
	DEFINE("INC_META", INCLUDES . "/meta.php");
	DEFINE("INC_STYLES", INCLUDES . "/styles.php");
	DEFINE("INC_SCRIPTS", INCLUDES . "/scripts.php");
	DEFINE("INC_DIALOGS", INCLUDES . "/dialogs.php");
	DEFINE("INC_LOCK", INCLUDES . "/lock.php");
	DEFINE("INC_LOGOUT", INCLUDES . "/logout.php");
	DEFINE("INC_HEADER", INCLUDES . "/header.php");
	DEFINE("INC_NAVIGATION", INCLUDES . "/navigation.php");
	
	// PAGE HELPERS
	DEFINE("HELPER_META", HELPERS . "/helper.meta.php");
	DEFINE("HELPER_LOGIN", HELPERS . "/helper.login.php");

	// PAGE INCLUDES	
	DEFINE("INC_PAGE_ALERTS", VIEWS . "/common/pagealerts.php");

	// SITE RESTRICTIONS
	DEFINE('COMPANY_CODES', SERIALIZE(ARRAY("iob", "elm", "gdi", "sxm", "sandbox")));
	// DEFINE('COMPANY_CODES', SERIALIZE(ARRAY("sxm", "sandbox")));

?>