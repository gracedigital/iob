<?php
	
	require_once 'configs/app-config.php';
	require_once 'configs/api-config.php';
	require_once 'configs/site-config.php';
	require 'router.class.php';	
	require 'template.class.php';
	require 'registry.class.php';
	require 'controller.class.php';

	/*** session ***/
	if(!isset($_SESSION)) session_start();

	/*** a new registry object ***/
	$registry = new registry;

	/*** load the router ***/
	$registry->router = new router($registry);

	/*** set the controller path ***/
	$registry->router->setPath(CONTROLLERS);

	/*** load up the template ***/
	$registry->template = new template($registry);

	/*** load the controller ***/
	$registry->router->route();

?>