<?php

class Router {

	/*
	* @the registry
	*/
	private $registry;

	/*
	* @the controller path
	*/
	private $path;
	public $file;
	public $controller;
	public $action; 
	public $filter;

	function __construct($registry) {
		$this->registry = $registry;
	}

	private function setCredentials() {
		if(empty($this->registry->credentials)) {
			$this->registry->credentials = array('username'=>$_SESSION['username'], 'password'=>$_SESSION['password']);
		}
	}

	private function getParameters() {
		if (!empty($_REQUEST['params'])) {
			$this->registry->params = $_REQUEST['params'];	
		} 
	}

	 /**
	 *
	 * @set controller directory path
	 *
	 * @param string $path
	 *
	 * @return void
	 *
	 */
	 function setPath($path) {
		/*** check if path is a directory ***/
		if (is_dir($path) == false)
		{
			throw new Exception ('Invalid controller path: `' . $path . '`');
		}
		/*** set the path ***/
	 	$this->path = $path;
	}


	 /**
	 *
	 * @load the controller
	 *
	 * @access public
	 *
	 * @return void
	 *
	 */
	 public function route()
	 {
	 	$action = null;
	 	$controller = null;

		// Validate the session user
		if(!empty($_SESSION['username'])) {

			/*** get the parameters ***/
			$this->getParameters();
			/*** check the route ***/
			$this->getController();

			$this->setCredentials();

			/*** if the file is not there diaf ***/
			if (is_readable($this->file) === false)
			{
				// handle refresh with this so we don't bomb
		  		header("Location: /index.php");
			}

			/*** include the controller ***/
			require $this->file;

			/*** a new controller class instance ***/
			$class = $this->controller . 'Controller';
			$controller = new $class($this->registry);

			/*** check if the action is callable ***/
			if (is_callable(array($controller, $this->action)) == false)
			{
				$action = 'index';
			}
			else
			{
				$action = $this->action;
			}

		} else {
			$controller = $this->getLoginController();
			$action = 'login';
		}

		/*** run the action ***/
		$controller->doAction($this->action);
	 }

	private function getController() {
		
		$route = (empty($_REQUEST['route'])) ? '' : $_REQUEST['route'];	

		if (empty($route)) {
			$route = 'index';
		} else {
			/*** get the parts of the route ***/
			$parts = explode('/', $route);
			$this->controller = $parts[0];
			if(isset($parts[1])) {
				$this->action = $parts[1];
			}	
			if(isset($parts[2])) {
				$this->filter = $parts[2];
			}				
		}

		if (empty($this->controller)) {
			$this->controller = 'index';
		}

		/*** get action ***/
		if (empty($this->action)) {
			$this->action = 'index';
		}

		/*** set the file path ***/
		$this->file = CONTROLLERS .'/'. $this->controller . 'Controller.php';
	}

	private function getLoginController() { 
		require $this->path . '/loginController.php';
		$class = 'loginController';
		return new $class($this->registry);
	}


}

?>
