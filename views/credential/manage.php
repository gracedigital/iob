<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->				
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-cogs"></i>&nbsp;<?php echo TITLE_CREDENTIAL_MANAGE; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_CREDENTIALS; ?></a>
					<span class="icon-angle-right"></span>
				</li>							
				<li>
					<a href="#"><?php echo LABEL_MANAGE; ?></a>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->

	<?php include_once INC_PAGE_ALERTS ?>

	<!-- BEGIN PAGE CONTENT-->			
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i><?php echo CAPTION_CREDENTIALS; ?>
					</div>
					<div class="actions">
						<a id="btn-download" href="/helpers/helper.download.php" class="btn green"><i class="icon-cloud-download"></i> Download</a>
					</div>						
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
							<div class="span12">
								<table class="table table-striped table-hover condensed" id="data_table">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th><i class="icon-key"></i> <?php echo TH_USERNAME; ?></th>
											<th><?php echo TH_DESCRIPTION; ?></th>
											<th><?php echo TH_GROUP; ?></th>
											<th><?php echo TH_SERIAL; ?></th>
											<th><?php echo TH_EDIT; ?></th>
											<th id="th_credential_delete"><?php echo TH_DELETE; ?></th>
											<th></th>
											<th hidden></th>
										</tr>
									</thead>
									<tbody>
										<?php $row = 1; ?>
										<?php foreach($result as $credential): ?>
											<tr id="<?php echo $credential['sirius_userdetail_id']; ?>">
												<td style="border-right:solid 1px #E5E5E5;text-align:center;">
													<?php echo $row++; ?>
												</td>
												<td>
													<?php echo $credential['siriususername']; ?>
												</td>
												<td>
													<?php echo empty($credential['description']) ? '' : $credential['description']; ?>
												</td>													
												<td>
													<?php echo $credential['radiogroup_description']; ?>
												</td>													
												<td>
													<?php echo empty($credential['radioserial']) ? 'No Radio Associated' : $credential['radioserial']; ?>
												</td>
												<td>
													<a class="edit" href="javascript:;"><i class="icon-bolt"></i></a>
												</td>
												<td>
													<a class="delete" href="javascript:;"><i class="icon-remove" title="Delete this SiriusXM credential"></i></a>
												</td>													
												<td></td>
												<td hidden><?php echo $credential['radiogroup_id']; ?></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>	
							</div>															
						</div>	
					</div>						
				</div>
			</div>
			<div>
				<fieldset class="table-legend">
					<span>&nbsp;Table Legend</span><i class="icon-bolt"></i>&nbsp;Edit <?php echo LABEL_CREDENTIALS; ?>
					<i class="icon-remove"></i>&nbsp;Delete <?php echo LABEL_CREDENTIALS; ?>
				</fieldset>
			</div>
		</div>
	</div>
</div>

<script src="assets/scripts/pages/credential-manage-outer-table.min.js"></script>
<script src="assets/scripts/pages/credential-manage.min.js"></script>