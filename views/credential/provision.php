<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->           
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->     
         <h3 class="page-title">
            <i class="icon-sitemap"></i><?php echo TITLE_CREDENTIAL_PROVISION; ?></small>
         </h3>
         <ul class="breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.php">Home</a> 
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_CREDENTIALS; ?></a>
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_PROVISION; ?></a>
            </li>                   
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->

   <?php include_once INC_PAGE_ALERTS ?>

   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN VALIDATION STATES-->
         <div class="portlet box purple">
            <div class="portlet-title">
              <div class="caption"><i class="icon-reorder"></i> <?php echo CAPTION_PROVISION_DETAILS; ?></div>
            </div>
            <div class="portlet-body form">
               <!-- BEGIN FORM-->
               <form id="frm_provision" class="form-horizontal">
                  <div>
                     <div class="control-group" style="margin-top:10px;">
                        <label class="control-label pull-left" for="name"><span class="required">* </span><?php echo LABEL_GROUP; ?>&nbsp;</label>
                        <div class="controls">
                           <select id="lst-group" class="select2 span4" tabindex="4">
                              <option></option>
                              <?php echo $result; ?>
                           </select>
                        </div>
                        <!-- <span style="float:left;padding:8px;color:#888;"> * All credentials must be assigned to a group</span> -->
                     </div>                      
                     <div class="control-group">
                        <label class="control-label pull-left" for="name"><span class="required">* </span><?php echo LABEL_USERNAME; ?>&nbsp;</label>
                        <div class="controls">
                           <input type="text" id="username" name="username" class="m-wrap large" autocomplete="on" autofocus="autofocus" placeholder="<?php echo PH_CREDENTIAL_USERNAME; ?>" tabindex="1"/>
                        </div>
                     </div>
                     <div class="control-group">
                        <label class="control-label pull-left" for="name"><?php echo LABEL_DESCRIPTION; ?>&nbsp;</label>
                        <div class="controls">
                           <input type="text" id="description" name="description" class="m-wrap large" placeholder="Example: Store name or number or location" tabindex="3"/>
                        </div>
                     </div>      
                  </div>            
                  <div class="form-actions">
                     <div class="pull-left" style="margin-top:5px;"><span style="color:rgb(224, 34, 34);">*</span>&nbsp;Indicates a required field</div>
                     <div class="control-group pull-right">
                        <button type="button" class="btn green" id="btn-save">Save <i class="icon-save"></i></button>
                        <button type="button" class="btn" id="btn-cancel">Cancel</button>
                     </div>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
         <!-- END VALIDATION STATES-->
      </div>
   </div>
   <!-- END PAGE CONTENT-->         
</div>

<script src="assets/scripts/pages/credential-provision.min.js"></script>