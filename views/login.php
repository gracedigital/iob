<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <?php include INC_META; ?>
  <?php include INC_STYLES; ?>
  <?php include HELPER_LOGIN; ?>
  <link href="assets/css/pages/login.css" rel="stylesheet" type="text/css"/>  
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="login">
  <!-- BEGIN LOGO -->
  <div class="logo">
      <img src="<?php echo LoginHelper::GetLogo(); ?>" alt="<?php echo MetaHelper::GetImageAlt('logo'); ?>"/>
  </div>
  <!-- END LOGO -->
  <!-- BEGIN LOGIN -->
  <div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form">
      <center>
          <span class="caption"><?php echo LoginHelper::GetCaption(); ?></span><br>
          <span class="terms"><?php echo LoginHelper::GetTerms(); ?></span><br>          
          <img class="product" src="assets/img/irbm20.gif" alt="<?php echo MetaHelper::GetImageAlt('product'); ?>" title="<?php echo MetaHelper::GetImageAlt('product'); ?>">
      </center>
      <h3 class="form-title" style="margin-bottom:5px;">Login to your account</h3>
      <div class="alert alert-error hide">
        <button class="close" data-dismiss="alert" style="top:5px;"></button>
        <span id="errorMessage">Please enter a valid username and password.</span>
      </div>      
      <div class="alert alert-success hide">
        <button class="close" data-dismiss="alert" style="top:5px;"></button>
        <span id="successMessage"></span>
      </div>
      <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" id="username" name="username" autofocus="autofocus" autocomplete="on" value="<?php echo empty($_COOKIE['remember_me']) ? '' : $_COOKIE['remember_me']; ?>"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="password" autocomplete="off" value=""/>
          </div>
        </div>
      </div>
      <div class="form-actions" style="margin-left:-10px;">
        <label class="checkbox">
          <input type="checkbox" name="remember" id="remember" <?php echo empty($_COOKIE['remember_me']) ? "unchecked" : "checked"; ?> /> Remember me
        </label>
        <button id="login-btn" type="submit" class="btn green pull-right">
        Login <i id="login-icon" class="m-icon-swapright m-icon-white"></i>
        <i id="login-load" class="icon-spinner icon-mirrored icon-spin" style="display:none;"></i>
        </button>            
      </div>
      <div class="forget-password">
        <h4>Forgot your password ?</h4>
        <p>
          no worries, click <a href="javascript:;" class="" id="forget-password">here</a>
          to reset your password.
        </p>
      </div>
      <div class="create-account">
        <p>
          Don't have an account yet ?&nbsp; 
          <a href="javascript:;" id="register-btn" class="">Create an account</a>
        </p>
      </div>
    </form>
    <!-- END LOGIN FORM -->        
    <!-- BEGIN FORGOT PASSWORD FORM -->
    <form class="form-vertical forget-form" action="#">
      <h3 class="">Forget Password ?</h3>
      <p>Enter your e-mail address below to reset your password.</p>
      <div class="control-group">
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email" />
          </div>
        </div>
      </div>
      <div class="form-actions">
        <button type="button" id="back-btn" class="btn">
        <i class="m-icon-swapleft"></i> Back
        </button>
        <button type="submit" class="btn green pull-right">
        Submit <i class="m-icon-swapright m-icon-white"></i>
        </button>            
      </div>
    </form>
    <!-- END FORGOT PASSWORD FORM -->
    <!-- BEGIN REGISTRATION FORM -->
    <form class="form-vertical register-form" action="#">
      <h3 class="">Sign Up</h3>
      <p>Enter your account details below:</p>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Username</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-user"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="subaccount_username"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-lock"></i>
            <input class="m-wrap placeholder-no-fix" type="password" id="register_password" placeholder="Password" name="subaccount_password"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <label class="control-label visible-ie8 visible-ie9">Re-type Your Password</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-ok"></i>
            <input class="m-wrap placeholder-no-fix" type="password" placeholder="Re-type Your Password" name="subaccount_password_confirm"/>
          </div>
        </div>
      </div>
      <div class="control-group">
        <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
        <label class="control-label visible-ie8 visible-ie9">Email</label>
        <div class="controls">
          <div class="input-icon left">
            <i class="icon-envelope"></i>
            <input class="m-wrap placeholder-no-fix" type="text" placeholder="Email" name="email"/>
          </div>
        </div>
      </div>      
      <?php if(!LoginHelper::isSirius()): ?>
      <div class="control-group">
        <div class="controls">
          <select id="list-companies" class="select2">
            <option></option>
            <option value="iob">IO Business Music</option>
            <option value="elm">El Media Group</option>
          </select>
        </div>
      </div>
      <?php endif; ?>
      <div class="control-group">
        <div class="controls" style="margin-left:20px;">
          <label class="checkbox" style="font-size:85%;">
          <input type="checkbox" name="tnc"/> I agree to the <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a>
          </label>  
          <div id="register_tnc_error"></div>
        </div>
      </div>
      <div class="form-actions">
        <button id="register-back-btn" type="button" class="btn">
        <i class="m-icon-swapleft"></i>  Back
        </button>
        <button type="submit" id="register-submit-btn" class="btn green pull-right">
        Sign Up <i id="register-icon" class="m-icon-swapright m-icon-white"></i>
        <i id="register-load" class="icon-spinner icon-mirrored icon-spin" style="display:none;"></i>
        </button>            
      </div>
    </form>
    <!-- END REGISTRATION FORM -->
  </div>
  <!-- END LOGIN -->

  <!-- BEGIN COPYRIGHT -->
  <div class="copyright">
    <?php echo SITE_COPYRIGHT; ?>
  </div>
  <!-- END COPYRIGHT -->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
  <script src="assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="assets/plugins/select2/select2.min.js"></script>
  <script src="assets/plugins/jquery.pulsate.min.js"></script>
  <script src="assets/plugins/underscore.min.js"></script>
  <script src="assets/scripts/webapp.min.js"></script>  
  <script src="assets/scripts/login.min.js"></script>  
  <script>
    $(document).ready(function() {
      Login.init();
    });
  </script>
</body>
</html>