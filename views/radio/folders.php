<?php ob_start("ob_gzhandler"); ?>
<div class="innerDetails">
	<div class="portlet box grey tabbable">
    	<div class="portlet-title">
           <div class="caption">
              <i class="icon-reorder"></i><?php echo CAPTION_RADIO_LINEUP; ?>
           </div>
			<span class="link-folder pull-right" style="cursor:pointer;color:#FFF;font-size:115%;">
				<i class="icon-plus"></i> Add </a>           
			</span>           
    	</div>
    	<div class="portlet-body" style="padding:10px;">
			<table class="table table-condensed table-hover" id="data_table_inner">
				<thead>
					<th><?php echo TH_ID; ?></th>			
					<th><?php echo TH_TITLE; ?></th>
					<th><?php echo TH_UNLINK; ?></th>
					<th></th>
				</thead>
				<tbody>		
				<?php foreach($folders as $folder): ?>
					<tr>
						<td>
							<?php echo $folder['list_id']; ?>
						</td>
						<td>
							<?php echo urldecode(stripslashes($folder['title'])); ?>
						</td>
						<td>
							<span class="unlink-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-unlink"></i></span>	
						</td>
						<td></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>	
</div>