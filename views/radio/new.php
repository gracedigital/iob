<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>

<div class="container-fluid">

	<!-- BEGIN PAGE HEADER-->   
	<div class="row-fluid"> 
	    <div class="span12">
	        <h3 class="page-title">
	           <i class="icon-tasks"></i>&nbsp;<?php echo TITLE_RADIO_PROVISION; ?>
	        </h3>
	        <ul class="breadcrumb">
	           <li>
	              <i class="icon-home"></i>
	              <a href="index.php"><?php echo LABEL_HOME; ?></a> 
	              <span class="icon-angle-right"></span>
	           </li>
	           <li>
	              <a href="#"><?php echo LABEL_RADIOS; ?></a>
	              <span class="icon-angle-right"></span>
	           </li>
	           <li><a href="#"><?php echo LABEL_PROVISION; ?></a></li>
	        </ul>
	    </div>
	</div>  
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT -->
   <div class="row-fluid">
      <div class="span12">
         <div class="portlet box blue">
            <div class="portlet-title">
               <div class="caption">
               		<i class="icon-reorder"></i><?php echo CAPTION_RADIO_DETAILS; ?>
               	</div>
            </div>
            <div class="portlet-body form">
            <!-- BEGIN FORM-->	    	
			<form action="#" class="form-horizontal" id="form-associate">
				<div class="row-fluid" id="div-associate">
					<div class="span12">
						<!-- <div class="control-group">
						 	<label class="control-label pull-left" for="serial"><?php echo LABEL_SERIAL_NUMBER; ?>&nbsp;</label>
							<div class="controls">
							    <div class="input-icon left">
							       <i class="icon-barcode"></i>
       								<input id="serial" type="text" class="m-wrap" readonly autofocus="autofocus" autocomplete="on"/>
							    </div>                                          
							</div>
						</div> -->
						<div class="control-group">
						 	<label class="control-label pull-left" for="reg_code"><span class="required">*</span><?php echo LABEL_REGISTRATION_KEY; ?>&nbsp;</label>
							<div class="controls">
							    <div class="input-icon left">
							       <i class="icon-key"></i>
       								<input id="reg_code" name="reg_code" type="text" class="m-wrap" autofocus="autofocus" placeholder="<?php echo PH_RADIO_KEY; ?>" />
							    </div>                                          
							</div>
						</div>												
						<div class="control-group">
						 	<label class="control-label pull-left" for="name"><span class="required">*</span><?php echo LABEL_NAME; ?>&nbsp;</label>
							<div class="controls">
								<div class="input-icon left">
									<i class="icon-font"></i>
       								<input id="name" type="text" class="m-wrap pull-left" autocomplete="on" placeholder="<?php echo PH_RADIO_NAME; ?>" />
       							</div>
							</div>
						</div>												
						<div class="control-group">
						 	<label class="control-label pull-left" for="reg_code"><span class="required">*</span><?php echo LABEL_GROUP; ?>&nbsp;</label>
							<div class="controls">
                              	<select id="lst-groups" class="select2" placeholder="<?php echo PH_SELECT_GROUP; ?>">
                              		<option value=''></option>
                                	<?php echo $groups; ?>
                              	</select>
							</div>
						</div>																									
						<div class="control-group">
							<label class="control-label pull-left"><span class="required">*</span><?php echo LABEL_LINEUP; ?>&nbsp;</label>
                            <div class="controls">
                              	<select id="lst-folders" name="list_id" class="select2" placeholder="<?php echo PH_SELECT_LINEUPS; ?>">
                                	<option value=''></option>
                                	<?php echo $folders; ?>
                              	</select>
                            </div>
						</div>												
					</div>
				</div>
				<div class="form-actions">
					<button type="button" class="btn" id="btn-cancel"><?php echo LABEL_BTN_CANCEL; ?></button>
					<button type="button" class="btn green" id="btn-submit"><?php echo LABEL_BTN_SUBMIT; ?>&nbsp;<i class="m-icon-swapright m-icon-white"></i></button>
					<span class="pull-right" style="margin-top:5px;"><span style="color:rgb(224, 34, 34);">*</span>&nbsp;Indicates a required field</span>
				</div>					                	
			</form>							
            </div>
         </div>
      </div>
   </div>
   <!-- END PAGE CONTENT-->   
</div>
<script src="assets/scripts/pages/radio-new.min.js"></script>