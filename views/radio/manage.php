<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->				
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-tasks"></i>&nbsp;<?php echo TITLE_RADIO_MANAGE; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_RADIOS; ?></a>
					<span class="icon-angle-right"></span>
				</li>							
				<li>
					<a href="javascript:;" id="lnk-manage"><?php echo LABEL_MANAGE; ?></a>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT-->			
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i>&nbsp;<?php echo CAPTION_RADIO_MANAGE; ?>
					</div>
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
							<div class="span12">
								<table class="table table-striped table-hover condensed" id="data_table">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th style="text-align:center;"><?php echo TH_DETAILS; ?></th>
											<th style="text-align:center;"><?php echo TH_STATUS; ?></th>
											<th><i class="icon-key"></i> <?php echo TH_SERIAL; ?></th>
											<th><?php echo TH_NAME; ?></th>
											<th><?php echo TH_GROUP; ?></th>
											<th><?php echo TH_EDIT; ?></th>
											<th id="th_radio_delete"><?php echo TH_DELETE; ?></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<?php $row = 1; ?>		
											<?php foreach($result as $radio): ?>
												<tr>
													<td style="border-right:solid 1px #E5E5E5;text-align:center;">
														<?php echo $row++ ?>
													</td>
													<td style="text-align:center;">
														<i class="icon-plus"></i>
														<a class="simple expand" href="javascript:;">&nbsp;<?php echo LABEL_EXPAND; ?></a>
													</td>
													<td style="text-align:center;">
														<?php if($radio['obsolete'] === 'true'): ?>
															<i class="icon-lock"><a class="simple openfont" href="javascript:;">&nbsp;<?php echo LABEL_INACTIVE; ?></a></i>
														<?php else: ?>
															<i class="icon-unlock"><a class="simple openfont" href="javascript:;">&nbsp;<?php echo LABEL_ACTIVE; ?></a></i>
														<?php endif; ?>
													</td>														
													<td>
														<?php echo $radio['radioserial']; ?>
													</td>
													<td>
														<?php echo empty($radio['name']) ? '' : urldecode(stripslashes($radio['name'])); ?>													
													</td>
													<td>
														<?php echo empty($radio['description']) ? '' : urldecode(stripslashes($radio['description'])); ?>													
													</td>													
													<td>
														<span class="edit-radio" style="cursor:pointer;color:#0D638F;">
															<i class="icon-bolt" style="width:100%;" title="<?php echo TIP_RADIO_RENAME; ?>"></i>
														</span>
													</td>
													<td>
														<span class="delete-radio" style="cursor:pointer;color:#0D638F;">
															<i class="icon-remove" style="width:100%;" title="<?php echo TIP_RADIO_DELETE; ?>"></i>
														</span>
													</td>													
													<td></td>
												</tr>
											<?php endforeach; ?>
									</tbody>
								</table>	
							</div>															
						</div>	
					</div>				
				</div>
				<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
					<button class="close" data-dismiss="alert" style="top:5px;"></button>
					<span id="status-msg" class="pull-right status"></span>
					<span class="status"><i class="icon-info-sign">&nbsp;</i><?php echo LABEL_STATUS; ?></span>
				</div>					
			</div>
			<div>
				<fieldset class="table-legend">
					<span>&nbsp;<?php echo LEGEND_TABLE; ?></span><i class="icon-plus"></i>&nbsp;<?php echo LEGEND_VIEW; ?>
					<i class="icon-lock"></i>&nbsp;<?php echo LEGEND_DEACTIVATE; ?>
					<i class="icon-unlock"></i>&nbsp;<?php echo LEGEND_ACTIVATE; ?>
					<i class="icon-bolt"></i>&nbsp;<?php echo LEGEND_EDIT; ?>
					<i class="icon-remove"></i>&nbsp;<?php echo LEGEND_DELETE; ?>
					<i class="icon-plus-sign-alt"></i>&nbsp;<?php echo LEGEND_ASSOCIATE; ?>
					<div class="pull-right"><i class="icon-text-height"></i>&nbsp;<b>&nbsp;<?php echo LEGEND_NOTE; ?></b></div>
				</fieldset>
			</div>
		</div>
	</div>
</div>
<script src="assets/scripts/pages/radio-manage-outer-table.min.js"></script>
<script src="assets/scripts/pages/radio-manage-activation.min.js"></script>
<script src="assets/scripts/pages/radio-manage.min.js"></script>