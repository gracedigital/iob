<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->				
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-tasks"></i>&nbsp;<?php echo TITLE_RADIO_MOVE; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_RADIOS; ?></a>
					<span class="icon-angle-right"></span>
				</li>							
				<li>
					<a href="#"><?php echo LABEL_MOVE; ?></a>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT-->			
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box purple">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i>&nbsp;<?php echo CAPTION_RADIO_MOVE; ?>
					</div>
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
		                	<div class="span6">			
			                     <h4>&nbsp;<i class="icon-map-marker" style="color:grey;"></i>&nbsp;<?php echo LABEL_SOURCE_GROUP; ?></h4>
			                     <div class="controls" style="width:100%;">
			                        <select id="sourceList" class="select2 span10" data-placeholder="<?php echo PH_SOURCE_GROUP; ?>">
			                        	<option value=''></option>
			                        	<?php echo $groups; ?>
			                        </select>
			                     </div>							
			                </div>
 						</div>
 						<br>
						<div class="row-fluid">
							<div class="span12">
								<table class="table table-striped table-bordered table-hover" id="data_table">
									<thead>
										<tr>
											<th style="width:18px !important;max-width:18px;">
												<input type="checkbox" class="group-checkable" data-set="#data_table .checkboxes" style="width=1px;" />
											</th>
											<th>
												<i class="icon-key"></i> <?php echo TH_ID; ?>
											</th>
											<th><?php echo TH_SERIAL; ?></th>
											<th><?php echo TH_NAME; ?></th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>	
							</div>															
						</div>
						*<span style="font-size:90%;color:#555555;display:inline-block;margin-bottom:5px;">&nbsp;Remember to select a radio from the list!</span>
						<div class="row-fluid">	
							<div class="span12">  								
								<div class="span6">
									<h4><i class="icon-flag-alt" style="color:grey;"></i>&nbsp;<?php echo LABEL_TARGET_GROUP; ?></h4>
									<div class="controls">
										<select id="targetList" class="select2 span6" data-placeholder="<?php echo PH_TARGET_GROUP; ?>">
											<option value=''></option>
											<?php echo empty($groups) ? '<option></option>' : $groups; ?>
										</select>
									</div>		
								</div>
								<div class="span6">
									<h4>&nbsp;</h4>
									<div class="btn-group pull-right">
										<button class="btn green pull-down move-radio" data-toggle="modal" disabled>
											<?php echo LABEL_BTN_MOVE; ?>&nbsp;<i class="m-icon-swapright m-icon-white" style="margin-top:2px;"></i>
										</button>
									</div>		
								</div>		
							</div>					
						</div>	
					</div>						
				</div>
			</div>
		</div>
	</div>
</div>
<script src="assets/scripts/pages/radio-move.min.js"></script>