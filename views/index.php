<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<?php include INC_META; ?>
	<?php include INC_STYLES; ?>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
</head>
<body class="page-header-fixed">
	<!-- BEGIN HEADER -->
	<?php include INC_HEADER; ?>
	<!-- END HEADER -->

	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<?php include INC_NAVIGATION; ?>
		<!-- END SIDEBAR -->

		<!-- BEGIN PAGE -->
		<div id="content" class="page-content">
			<?php if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) || in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])): ?>
				<?php include(VIEWS . "/dashboard/sxm.php"); ?>
			<?php else: ?>
				<?php include(VIEWS . "/dashboard/gdi.php"); ?>
			<?php endif; ?>	
		</div>
		<!-- END PAGE -->
	</div>
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>	
	<?php include INC_SCRIPTS; ?>
	<?php include INC_DIALOGS; ?>
</body>
</html>	