<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->				
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-sitemap"></i>&nbsp;<?php echo TITLE_USER_MANAGE; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_USERS; ?></a>
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_MANAGE; ?></a>
				</li>							
			</ul>
		</div>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT-->
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box yellow">
				<div class="portlet-title">
					<div class="caption">
					   <i class="icon-user"></i>&nbsp;<?php echo LABEL_USERS; ?>
					</div>				
					<div class="actions">
						<a id="btn-download" href="/helpers/helper.download.php" class="btn yellow"><i class="icon-cloud-download"></i> Download</a>
					</div>					
				</div>					
				<div class="portlet-body">
					<table class="table table-striped table-hover" id="data_table_outer">
						<thead>
							<tr>
								<th><?php echo TH_ID; ?></th>
								<th><?php echo TH_DETAILS; ?></th>								
								<th><i class="icon-key"></i> <?php echo TH_USERNAME; ?></th>
								<th><i class="icon-lock"></i> <?php echo TH_PASSWORD; ?></th>
								<th><?php echo TH_DESCRIPTION; ?></th>
								<th><?php echo TH_CHILDREN; ?></th>
								<th><?php echo TH_EDIT; ?></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($data['users'] as $user): ?>	
								<tr>
									<td style="border-right:solid 1px #E5E5E5;text-align:center;">
										<?php echo $user['username_id']; ?>
									</td>											
									<td>
										<i class="icon-plus" title="View User Details"></i>
										<a class="simple expand-users" href="javascript:;">&nbsp;Expand</a>
									</td>
									<td><?php echo urldecode($user['username']); ?></td>
									<td><?php echo '********'; ?></td>
									<td><?php echo empty($user['notes']) ? '' : urldecode($user['notes']); ?></td>
									<td><?php echo count($data['childusers']); ?></td>
									<td style="width:50px;"><a class="edit" href="javascript:;"><i class="icon-bolt" style="width:100%;"></i></a></td>
									<td></td>
								</tr>
							<?php endforeach; ?>							
						</tbody>
					</table>
				</div>
			</div>										
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>

<script src="assets/scripts/pages/user-manage-inner-table.js"></script>
<script src="assets/scripts/pages/user-manage-outer-table.js"></script>
<script src="assets/scripts/pages/user-manage.min.js"></script>