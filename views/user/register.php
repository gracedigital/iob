<?php ob_start("ob_gzhandler"); ?>
<div class="page-content">
   <!-- BEGIN PAGE CONTAINER-->        
   <div class="container-fluid">
      <!-- BEGIN PAGE HEADER-->           
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN PAGE TITLE & BREADCRUMB-->     
            <h3 class="page-title">
               Register <small>your SiriusXM credentials</small>
            </h3>
            <ul class="breadcrumb">
               <li>
                  <i class="icon-home"></i>
                  <a href="index.php">Home</a> 
                  <span class="icon-angle-right"></span>
               </li>
               <li>
                  <a href="#">Users</a>
                  <span class="icon-angle-right"></span>
               </li>
               <li>
                  <a href="#">Register</a>
               </li>                   
            </ul>
            <!-- END PAGE TITLE & BREADCRUMB-->
         </div>
      </div>
      <!-- END PAGE HEADER-->

      <!-- BEGIN PAGE ALERTS -->       
      <div class="alert alert-error hide">
        <i class="icon-exclamation-sign" style="float:right;margin-top:5px;"></i>
        <span id="error-msg">You have error</span>
      </div>
      <div class="alert alert-success hide">
         <i class="icon-ok"></i>
         <button class="close" data-dismiss="alert" style="top:5px;"></button>
        <span id="success-msg"></span>
      </div>
      <!-- END PAGE ALERTS -->

      <!-- BEGIN PAGE CONTENT-->
      <div class="row-fluid">
         <div class="span12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet box blue">
               <div class="portlet-title">
                  <div class="caption">
                     <i class="icon-reorder"></i><?php echo CAPTION_USER_REGISTER; ?>
                  </div>
                  <div class="tools">
                     <a href="javascript:;" class="collapse"></a>
                     <a href="#portlet-config" data-toggle="modal" class="config"></a>
                     <a href="javascript:;" class="reload"></a>
                  </div>
               </div>
               <div class="portlet-body form">
                  <!-- BEGIN FORM-->
                  <form id="form_register_user" class="form-horizontal" action='#'>
                     <br>
                     <label class="control-label">Username<span class="required">*</span></label>
                     <div class="controls">       
                        <div class="input-icon left">                                          
                           <i class="icon-user"></i>
                           <input type="text" name="username" class="m-wrap" style="width:250px !important;" placeholder="SiriusXM Username">
                        </div>
                     </div>
                     <br>                    
                     <label class="control-label">Password<span class="required">*</span></label>
                     <div class="controls">       
                        <div class="input-icon left">                                          
                           <i class="icon-lock"></i>
                           <input type="password" name="password" class="m-wrap" style="width:250px !important;" placeholder="SiriusXM Password">
                        </div>
                     </div>
                     <br>                  
                     <div class="form-actions">
                        <button type="button" class="btn green" id="btn-save">Save <i class="icon-save"></i></button>
                        <button type="button" class="btn" id="btn-cancel">Cancel</button>
                     </div>
                  </form>
                  <!-- END FORM-->
               </div>
            </div>
            <!-- END VALIDATION STATES-->
         </div>
      </div>
      <!-- END PAGE CONTENT-->         
        
   </div>
   <!-- END PAGE CONTAINER-->
</div>

<script>

   $(document).ready(function() {   

      $('#btn-save').click(function () {

         if(!validateForm($('#form_register_user'))) {
            return false;
         }
         
         var params = $.param({ route: 'user/save',  params: $('form').toJSON() });    
         $.get('/application/proxy.php', params)
            .success(function(response) {
               var result = JSON.parse(response);
               if(result.error) {            
                  showError(result.error.message);                    
               } else {
                 showSuccess("You have successfully authenticated your SiriusXM credentials.&nbsp;&nbsp;They are associated to radio serial # " + result.serial + ".");
                 $('form').trigger('reset');          
               }
               return false;
            })
            .always( function() {
               // cleanup
            }
         );
      });
   });

</script>
