<?php ob_start("ob_gzhandler"); ?>
<div class="innerDetails">
	<div class="portlet box grey tabbable">
    	<div class="portlet-title">
           <div class="caption">
              <i class="icon-group"></i><?php echo CAPTION_USER_SUBACCOUNTS; ?>
           </div>
    	</div>
    	<div class="portlet-body">
			<table class="table table-condensed table-hover" id="data_table_inner">
				<thead>
					<th style="padding-left:10px;">ID</th>			
					<th><i class="icon-key"></i> Username</th>
					<th><i class="icon-lock"></i> Password</th>
					<th>Notes</th>
					<th>Edit</th>
					<th>Delete</th>
				</thead>
				<tbody>
					<?php foreach($data['childusers'] as $childuser): ?>
						<tr>
							<td style="padding-left:10px;"><?php echo $childuser['username_id']; ?></td>
							<td><?php echo $childuser['username']; ?></td>
							<td><?php echo "********" ?></td>
							<td><?php echo empty($childuser['notes']) ? '' : urldecode($childuser['notes']); ?></td>
							<td style="width:50px;"><a id="edit-user" href="javascript:;"><i class="icon-bolt"></i></a></td>
							<td style="width:50px;"><a id="delete-user" href="javascript:;"><i class="icon-remove"></i></a></td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>	
</div>