<!-- BEGIN PAGE ALERTS -->       
<div class="alert alert-error hide" id="alertError">
 <button class="close" data-dismiss="alert" style="top:5px;"></button>
 <i class="icon-thumbs-down">&nbsp;</i><span id="errorMessage"></span>
</div>
<div class="alert alert-success hide" id="alertSuccess">
 <button class="close" data-dismiss="alert" style="top:5px;"></button>
 <i class="icon-thumbs-up">&nbsp;</i><span id="successMessage"></span>
</div>
<div class="alert hide" id="alertWarning">
 <button class="close" data-dismiss="alert" style="top:5px;"></button>
 <i class="icon-bullhorn">&nbsp;</i><span id="warningMessage"></span>
</div>
<!-- END PAGE ALERTS -->