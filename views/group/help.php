<script>
	$('#content').load('/views/help/overview.php');
</script>
<!-- BEGIN PAGE CONTAINER-->
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title">
				HELP <small><?php echo strtolower(LABEL_GROUPS); ?></small>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.html">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#"><?php echo LABEL_HELP; ?></a>
					<i class="icon-angle-right"></i>
				</li>
				<li><a href="#"><?php echo LABEL_GROUPS; ?></a></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	<!-- BEGIN PAGE CONTENT-->
	<div class="row-fluid">
		<div class="span12">
			<div class="span3">
				<ul class="ver-inline-menu tabbable margin-bottom-10">
					<li class="active">
						<a href="#tab_1" data-toggle="tab">
						<i class="icon-briefcase"></i> 
						Overview
						</a> 
						<span class="after"></span>                                    
					</li>
					<li><a href="#tab_1" data-toggle="tab"><i class="icon-plus"></i> Create a new group</a></li>
					<li><a href="#tab_1" data-toggle="tab"><i class="icon-leaf"></i> Edit a group</a></li>
					<li><a href="#tab_1" data-toggle="tab"><i class="icon-info-sign"></i> Rename a group</a></li>
					<li><a href="#tab_1" data-toggle="tab"><i class="icon-tint"></i> Delete a group</a></li>
				</ul>
			</div>
			<div class="span9">
				<div class="tab-content">
					<div class="tab-pane active" id="tab_1">
						<div class="accordion in collapse" id="accordion1" style="height: auto;">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1">
									What are groups?
									</a>
								</div>
								<div id="collapse_1" class="accordion-body collapse in">
									<div class="accordion-inner">
										Groups provide a way to organize one or more radios into a logical group so that they can be acted upon as a set. Groups are tyically created to organize radios by business type, business location or business hours of operation. Primarily groups are used for assigning a set of radios the same schedule. In terms of relationships, a radio can be associated to one or more groups. Additionally a group can be a associated to one or more schedules.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_2">
									How do I create a new group?
									</a>
								</div>
								<div id="collapse_2" class="accordion-body collapse">
									<div class="accordion-inner">
										To create a group select Groups/Add from the navigation menu on the left side of the screen and complete the 'name' field.  Additionally you can add a description to help identify the makeup of the group.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_3">
									How do I edit an existing group?
									</a>
								</div>
								<div id="collapse_3" class="accordion-body collapse">
									<div class="accordion-inner">
										1. Select Groups/Manage from the navigation menu on the left side of the screen.<br>  
										2. Click on the icon in the edit column to activate the editable fields.<br>
										3. Type the confirmation word in the dialog box and select OK to confirm the action.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_4">
									High life accusamus terry richardson ad ?
									</a>
								</div>
								<div id="collapse_4" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_5">
									Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_5" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_6">
									Wolf moon officia aute non cupidatat skateboard dolor brunch ?
									</a>
								</div>
								<div id="collapse_6" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab_2">
						<div class="accordion in collapse" id="accordion2" style="height: auto;">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_1">
									Cliche reprehenderit, enim eiusmod high life accusamus enim eiusmod ?
									</a>
								</div>
								<div id="collapse_2_1" class="accordion-body collapse in">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_2">
									Pariatur cliche reprehenderit enim eiusmod high life non cupidatat skateboard dolor brunch ?
									</a>
								</div>
								<div id="collapse_2_2" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_3">
									Food truck quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_2_3" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_4">
									High life accusamus terry richardson ad squid enim eiusmod high ?
									</a>
								</div>
								<div id="collapse_2_4" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_5">
									Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_2_5" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
										</p>
										<p> 
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor
										</p>
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_6">
									Wolf moon officia aute non cupidatat skateboard dolor brunch ?
									</a>
								</div>
								<div id="collapse_2_6" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion2" href="#collapse_2_7">
									Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_2_7" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
										</p>
										<p> 
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="tab-pane" id="tab_3">
						<div class="accordion in collapse" id="accordion3" style="height: auto;">
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_1">
									Cliche reprehenderit, enim eiusmod ?
									</a>
								</div>
								<div id="collapse_3_1" class="accordion-body collapse in">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_2">
									Pariatur skateboard dolor brunch ?
									</a>
								</div>
								<div id="collapse_3_2" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_3">
									Food truck quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_3_3" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_4">
									High life accusamus terry richardson ad squid enim eiusmod high ?
									</a>
								</div>
								<div id="collapse_3_4" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_5">
									Reprehenderit enim eiusmod high  eiusmod ?
									</a>
								</div>
								<div id="collapse_3_5" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
										</p>
										<p> 
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor
										</p>
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_6">
									Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ?
									</a>
								</div>
								<div id="collapse_3_6" class="accordion-body collapse">
									<div class="accordion-inner">
										Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_7">
									Reprehenderit enim eiusmod high life accusamus aborum eiusmod ?
									</a>
								</div>
								<div id="collapse_3_7" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
										</p>
										<p> 
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor
										</p>
									</div>
								</div>
							</div>
							<div class="accordion-group">
								<div class="accordion-heading">
									<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion3" href="#collapse_3_8">
									Reprehenderit enim eiusmod high life accusamus terry quinoa nesciunt laborum eiusmod ?
									</a>
								</div>
								<div id="collapse_3_8" class="accordion-body collapse">
									<div class="accordion-inner">
										<p>
											Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor.
										</p>
										<p> 
											moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmodBrunch 3 wolf moon tempor
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--end span9-->                                   
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<script>
	$(document).ready(function() {
		var section = window.location.href.split("#");
		console.log(section);
	});
</script>