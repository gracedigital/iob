<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->           
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->     
         <h3 class="page-title">
            <i class="icon-sitemap"></i>&nbsp;<?php echo TITLE_GROUP_ADD; ?>
         </h3>
         <ul class="breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.php"><?php echo LABEL_HOME; ?></a> 
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_GROUPS; ?></a>
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_ADD; ?></a>
            </li>                   
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->
   <?php include INC_PAGE_ALERTS; ?>
   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <div class="portlet box purple">
            <div class="portlet-title">
               <div class="caption"><i class="icon-reorder"></i><?php echo CAPTION_GROUP_DETAILS; ?></div>
            </div>
            <div class="portlet-body form">
               <!-- BEGIN FORM-->
               <form id="form_add_object" class="form-horizontal" action='/'>
                  <div class="control-group">
                     <label class="control-label"><?php echo LABEL_NAME; ?><span class="required">*</span></label>
                     <div class="controls">
                        <input type="text" name="description" id="desc" data-required="1" autocomplete="on" autofocus="autofocus" class="span6 m-wrap" placeholder="Enter a name for this group"/>
                     </div>
                  </div>
                  <div class="control-group">
                     <label class="control-label"><?php echo LABEL_DESCRIPTION; ?></span></label>
                     <div class="controls">
                        <textarea class="span6 m-wrap" rows="3" name="note" id="note" maxlength="255" autocomplete="on"></textarea>
                     </div>
                  </div>
                  <div class="form-actions">
                     <button type="button" class="btn green" id="btn-save"><?php echo LABEL_BTN_SAVE; ?> <i class="icon-save"></i></button>
                     <button type="button" class="btn" id="btn-cancel"><?php echo LABEL_BTN_CANCEL; ?></button>
                     <span class="pull-right" style="margin-top:5px;"><span style="color:rgb(224, 34, 34);">*</span>&nbsp;Indicates a required field</span>
                  </div>
               </form>
               <!-- END FORM-->
            </div>
         </div>
      </div>
   </div>
   <!-- END PAGE CONTENT-->         
</div>
<script src="assets/scripts/pages/group-new.min.js"></script>