<?php ob_start("ob_gzhandler"); ?>
<style>
.dataTable .details tr:nth-child(odd) td, .dataTable .details tr:nth-child(odd) th {
	background-color: #fff;
}
</style>
<div class="innerDetails">
	<div class="portlet box blue">
    	<div class="portlet-title">
            <div class="caption"><i class="icon-reorder"></i><?php echo CAPTION_GROUP_SCHEDULES; ?></div>
			<span class="link-schedule pull-right" style="cursor:pointer;color:#FFF;font-size:115%;">
				<i class="icon-plus"></i> Add </a>           
			</span>           
    	</div>
    	<div class="portlet-body" style="padding:10px;">
			<table id="data_table_inner" class="table table-striped table-bordered" data-rows="<?php echo count($result);?>">
				<thead>
					<tr>
						<th class="span2">ID</th>
						<th class="span11">Name</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($result as $schedule): ?>
						<tr>
							<td style="background-color: #fff;"><?php echo $schedule['schedule_id']; ?></td>
							<td style="background-color: #fff;"><?php echo empty($schedule['schedule_description']) ? '' : urldecode(stripslashes($schedule['schedule_description'])); ?></td>
						</tr>
					<?php endforeach; ?>
					<?php if(count($result) == 0): ?>		
						<tr><td valign="top" colspan="7" class="dataTables_empty"><? echo LABEL_NO_DATA; ?></td></tr>										
					<?php endif; ?>
				</tbody>
			</table>
		</div>
	</div>	
</div>