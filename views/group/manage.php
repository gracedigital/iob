<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->				
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-sitemap"></i>&nbsp;<?php echo TITLE_GROUP_MANAGE; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_GROUPS; ?></a>
					<span class="icon-angle-right"></span>
				</li>
				<li>
					<a href="#"><?php echo LABEL_MANAGE; ?></a>
				</li>							
			</ul>
		</div>
		<!-- END PAGE TITLE & BREADCRUMB-->
	</div>
	<!-- END PAGE HEADER-->

	<?php include_once INC_PAGE_ALERTS ?>

	<!-- BEGIN PAGE CONTENT --> 
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box yellow">
				<div class="portlet-title">
					<div class="caption"><?php echo CAPTION_GROUP_MANAGE; ?></div>
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
							<div class="span12">
								<table class="table table-striped table-hover condensed" id="data_table">
									<thead>
										<tr>
											<th></th>
											<th><?php echo TH_DETAILS; ?></th>
											<th><i class="icon-key"></i> <?php echo TH_ID; ?></th>
											<th><?php echo TH_NAME; ?></th>
											<th><?php echo TH_DESCRIPTION; ?></th>
											<th><?php echo TH_EDIT; ?></th>
											<th id="th_group_delete"><?php echo TH_DELETE; ?></th>
											<th></th>								
										</tr>
									</thead>
									<tbody>
										<?php $row = 1; ?>
										<?php foreach($result as $group): ?>
											<tr>
												<td class="td-counter-border">
													<?php echo $row++ ?>
												</td>
												<td>
													<i class="icon-plus" title="View Details"></i>
													<a class="simple expand" href="javascript:;">&nbsp;<?php echo LABEL_EXPAND; ?></a>
												</td>												
												<td>
													<?php echo $group['radiogroup_id']; ?>
												</td>
												<td>
													<?php echo urldecode(stripslashes($group['description'])); ?>
												</td>	
												<td>
													<?php echo empty($group['note']) ? '' : urldecode(stripslashes($group['note'])); ?>
												</td>													
												<td>
													<span class="edit-group" style="cursor:pointer;color:#0D638F;">
														<i class="icon-bolt" title="<?php echo TIP_EDIT_GROUP; ?>"></i>
													</span>
												</td>	
												<td>
													<span class="delete-group" style="cursor:pointer;color:#0D638F;">
														<i class="icon-remove" title="<?php echo TIP_DELETE_GROUP; ?>"></i>
													</span>
												</td>																													
												<td></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>
							</div>															
						</div>	
					</div>				
				</div>
				<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
					<button class="close" data-dismiss="alert" style="top:5px;"></button>
					<span id="status-msg" class="pull-right status"></span>
					<span class="status"><i class="icon-info-sign">&nbsp;</i><?php echo LABEL_STATUS; ?></span>
				</div>					
			</div>
		</div>	        
	</div>
	<!-- END PAGE CONTENT -->
</div>
<script src="assets/scripts/pages/group-manage-outer-table.min.js"></script>
<script src="assets/scripts/pages/group-manage.min.js"></script>