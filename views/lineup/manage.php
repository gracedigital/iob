<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
   	<!-- BEGIN PAGE HEADER-->           
   	<div class="row-fluid">
      	<div class="span12">
         	<!-- BEGIN PAGE TITLE & BREADCRUMB-->     
	        <h3 class="page-title">
	            <i class="icon-random"></i>&nbsp;<?php echo TITLE_LINEUP_MANAGE; ?>
	        </h3>
			<ul class="breadcrumb">
		        <li>
		           <i class="icon-home"></i>
		           <a href="index.php"><?php echo LABEL_HOME; ?></a> 
		           <span class="icon-angle-right"></span>
		        </li>
		        <li>
		           <a href="#"><?php echo LABEL_CHANNEL; ?></a>
		           <span class="icon-angle-right"></span>
		        </li>
		        <li>
		           <a href="#"><?php echo LABEL_LINEUP; ?></a>
		        </li>            
		    </ul>
		    <!-- END PAGE TITLE & BREADCRUMB-->	
	  	</div>
	</div>	
	<!-- END PAGE HEADER-->	    

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT --> 
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i><?php echo CAPTION_LINEUP_RELATED; ?>
					</div>
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
							<div class="span12">
							<table class="table table-striped table-hover condensed" id="data_table">
								<thead>
									<tr>
										<th></th>
										<th><?php echo TH_DETAILS; ?></th>
										<th><i class="icon-key"></i>&nbsp;ID</th>
										<th><?php echo TH_LINEUP; ?></th>
										<th><?php echo TH_DESCRIPTION; ?></th>
										<th><?php echo TH_EDIT; ?></th>
										<th id="th_folder_delete"><?php echo TH_DELETE; ?></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<?php $row = 1; ?>					
									<?php foreach($folders as $folder): ?>								
										<tr>
											<td style="border-right:solid 1px #E5E5E5;text-align:center;">
												<?php echo $row++ ?>
											</td>												
											<td>
												<i class="icon-plus" title="View Details"></i>
												<a class="simple expand" href="javascript:;">&nbsp;Expand</a>
											</td>
											<td>
												<?php echo $folder['list_id']; ?>
											</td>
											<td>
												<?php echo urldecode(stripslashes($folder['title'])); ?>
											</td>
											<td>
												<?php echo empty($folder['notes']) || $folder['notes'] == 'null' ? '' : urldecode(stripslashes($folder['notes'])); ?>
											</td>
											<td>
												<span class="edit-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-bolt"></i></a>
											</td>
											<td>
												<span class="delete-folder" style="cursor:pointer;color:#0D638F;"><i class="icon-remove"></i></a>	
											</td>
											<td></td>
										</tr>
									<?php endforeach; ?>				
								</tbody>
							</table>		      		
							</div>															
						</div>	
					</div>				
				</div>
				<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
					<button class="close" data-dismiss="alert" style="top:5px;"></button>
					<span id="status-msg" class="pull-right status"></span>
					<span class="status"><i class="icon-info-sign">&nbsp;</i>Status:</span>
				</div>					
			</div>
		</div>	        
	</div>
	<!-- END PAGE CONTENT -->
</div>	
<script src="assets/scripts/pages/lineup-manage-outer-table.min.js"></script>	
<script src="assets/scripts/pages/lineup-manage-inner-table.min.js"></script>
<script src="assets/scripts/pages/lineup-manage.min.js"></script>