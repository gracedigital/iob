<?php ob_start("ob_gzhandler"); ?>
<div class="innerDetails">
	<div class="portlet box grey tabbable">
    	<div class="portlet-title">
           <div class="caption">
              <i class="icon-reorder"></i><?php echo CAPTION_LINEUP_CHANNELS; ?>
           </div>
    	</div>
    	<div class="portlet-body" style="padding:10px;">
			<table class="table table-condensed table-hover" id="data_table_inner">
				<thead>
					<th hidden>LIST_ID</th>
					<th><?php echo TH_ID; ?></th>			
					<th><?php echo TH_TITLE; ?></th>
					<th><?php echo TH_URL; ?></th>
					<th><?php echo TH_EDIT; ?></th>
					<th id="th_channel_delete"><?php echo TH_DELETE; ?></th>
					<th></th>
				</thead>
				<tbody>		
				<?php foreach($channels as $channel): ?>
					<tr>
						<td hidden>
							<?php echo $channel['list_id']; ?>
						</td>
						<td>
							<?php echo $channel['list_entry_id']; ?>
						</td>
						<td>
							<?php echo urldecode(stripslashes($channel['title'])); ?>
						</td>
						<td>
							<?php echo empty($channel['streamUrl']) ? 'Undefined' : urldecode(stripslashes($channel['streamUrl'])); ?>
						</td>
						<td>
							<span class="edit-channel" style="cursor:pointer;color:#0D638F;">
								<i class="icon-bolt"></i>
							</span>
						</td>
						<td>
							<span class="delete-channel" style="cursor:pointer;color:#0D638F;">
								<i class="icon-remove"></i>
							</span>	
						</td>
						<td></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>	
</div>