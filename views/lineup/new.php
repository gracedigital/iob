<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
   <!-- BEGIN PAGE HEADER-->           
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN PAGE TITLE & BREADCRUMB-->     
         <h3 class="page-title">
            <i class="icon-random"></i>&nbsp;<?php echo TITLE_CHANNEL_ADD; ?>
         </h3>
         <ul class="breadcrumb">
            <li>
               <i class="icon-home"></i>
               <a href="index.php"><?php echo LABEL_HOME; ?></a> 
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_CHANNELS; ?></a>
               <span class="icon-angle-right"></span>
            </li>
            <li>
               <a href="#"><?php echo LABEL_ADD; ?></a>
            </li>                   
         </ul>
         <!-- END PAGE TITLE & BREADCRUMB-->
      </div>
   </div>
   <!-- END PAGE HEADER-->

  <?php include INC_PAGE_ALERTS; ?>

   <!-- BEGIN PAGE CONTENT-->
   <div class="row-fluid">
      <div class="span12">
         <!-- BEGIN TABS -->
         <div class="portlet box blue tabbable">
            <div class="portlet-title">
               <div class="caption">
                  <i class="icon-reorder"></i><?php echo CAPTION_CHANNEL_ADD; ?>
               </div>
            </div>
            <div class="portlet-body">
               <div class="tabbable portlet-tabs">
                  <ul class="nav nav-tabs">
                     <li><a id="tab-folder" href="#portlet_tab2" data-toggle="tab" style="color:#fff;"><?php echo LABEL_LINEUP; ?></a></li>
                     <li class="active"><a id="tab-channel" href="#portlet_tab1" data-toggle="tab"><?php echo LABEL_CHANNEL; ?></a></li>
                  </ul>
                  <div class="tab-content">
                     <!-- BEGIN NEW CHANNEL TAB -->
                     <div class="tab-pane active" id="portlet_tab1">
                        <form id="add_channel" class="form-horizontal">
                           <div class="control-group">
                              <label class="control-label"><?php echo LABEL_NAME; ?><span class="required">*</span></label>
                              <div class="controls">
                                 <input type="text" id="chnl-title" name="title" data-required="1" class="span6 m-wrap" tabindex="1" autofocus="autofocus" placeholder="<?php echo PH_CHANNEL_ENTER_NAME; ?>"/>
                              </div>
                           </div>                   
                           <div class="control-group">
                              <label class="control-label"><?php echo LABEL_URL; ?><span class="required">*</span></label>
                              <div class="controls">
                                 <input type="text" id="chnl-url" name="stream_url" data-required="1" class="span6 m-wrap" tabindex="2" autocomplete="on" placeholder="<?php echo PH_CHANNEL_ENTER_URL; ?>"/>
                              </div>
                           </div>
                           <div class="control-group">
                              <label class="control-label"><?php echo LABEL_LINEUP; ?><span class="required">*</span></label>
                              <div class="controls">
                                 <select id="lst-folders" name="list_id" class="span6 select2" data-required="1" tabindex="3" placeholder="<?php echo PH_SELECT_LINEUP; ?>">
                                    <option value=""></option>
                                    <?php echo urldecode($folders); ?> 
                                 </select>
                                 <input type="hidden" id="chnl-folder" name="folder" value="false" />
                                 <input type="hidden" id="chnl-parentid" name="parent_id"/>
                              </div>                              
                           </div>  
                           <div class="control-group">
                              <label class="control-label"><?php echo LABEL_NOTE; ?></span></label>
                              <div class="controls">
                                 <textarea class="span6 m-wrap" rows="3" name="note" maxlength="255" id="chnl-note" tabindex="4"></textarea>
                              </div>
                           </div>
                           <div class="form-actions">
                              <button id="btn-chnl-save" type="button" class="btn green"><?php echo LABEL_BTN_SAVE; ?> <i class="icon-save"></i></button>
                              <button id="btn-chnl-cancel" type="button" class="btn cancel"><?php echo LABEL_BTN_CANCEL; ?></button>
                           </div>
                        </form>
                     </div> 
                     <!-- END NEW CHANNEL TAB -->

                     <!-- BEGIN NEW LINEUP TAB -->
                     <div class="tab-pane" id="portlet_tab2">
                           <form id="add_folder" class="form-horizontal">
                              <div class="control-group">
                                 <label class="control-label"><?php echo LABEL_NAME; ?><span class="required">*</span></label>
                                 <div class="controls">
                                    <input type="text" id="fldr-title" name="title" data-required="1" class="span6 m-wrap" autofocus="autofocus" placeholder="<?php echo PH_LINEUP_ENTER_NAME; ?>"/>
                                    <input type="hidden" id="fldr-email" value="ar@reciva.com" />
                                 </div>
                              </div>                   
                              <div class="control-group">
                                 <label class="control-label"><?php echo LABEL_NOTE; ?></span></label>
                                 <div class="controls">
                                    <textarea class="span6 m-wrap" rows="3" id="fldr-note" maxlength="255"></textarea>
                                 </div>
                              </div>
                              <div class="form-actions">
                                 <button id="btn-fldr-save" type="button" class="btn green"><?php echo LABEL_BTN_SAVE; ?> <i class="icon-save"></i></button>
                                 <button id="btn-fldr-cancel" type="button" class="btn cancel"><?php echo LABEL_BTN_CANCEL; ?></button>
                              </div>
                           </form>
                     </div>
                     <!-- END NEW LINEUP TAB -->
                  </div>  
               </div>
            </div>  
         </div>
         <!-- END TABS -->
      </div>
   </div>
   <!-- END PAGE CONTENT-->         
</div>

<script src="assets/scripts/pages/lineup-new.min.js"></script>