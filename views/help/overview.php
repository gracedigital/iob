<link href="/assets/css/help.css" rel="stylesheet">
<link href="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.css" rel="stylesheet">
<div class="container" style="margin-left:25px;">

	<div class="row">
		<!-- ====  NAVIGATION  ==== -->
	  	<div class="span3 bs-docs-sidebar" style="margin-top:40px;">
			<ul class="nav nav-list bs-docs-sidenav affix-top">
				<li class="active"><a href="#start"><i class="icon-chevron-right"></i> Overview</a></li>
				<li class=""><a href="#group"><i class="icon-chevron-right"></i> Groups</a></li>
				<li class=""><a href="#credential"><i class="icon-chevron-right"></i> Credentials</a></li>
				<li class=""><a href="#schedule"><i class="icon-chevron-right"></i> Schedules</a></li>
				<li class=""><a href="#layout"><i class="icon-chevron-right"></i> Layout</a></li>
			</ul>
	 	</div>

		<div class="span9" style="color:rgb(51, 51, 51);font-size:14;">

			<!-- ========= S T A R T ========= -->
			<section id="start">
				<div class="page-header">
					<h1>Overview</h1>
				</div>

				<h3 style="line-height:26px;">Business Radio Management Portal</h3>
	            <p>
	              <span style="text-align=middle;padding-left:10px;line-hight:2;">The Business Radio Management Portal is designed to control the Grace Digital Business Radio model: GDI-SXMBR1.</span> 
	            </p>

	            <div class="alert alert-info">
	              <h4>The GDI-SXMBR1 is specifically designed to be used with SiriusXM for Business.</h4>
	              <p style="margin-top:10px;">Follow this link more information about SiriusXM Music for Business <code><a href="http://www.siriusxm.com/siriusxmforbusiness">http://www.siriusxm.com/siriusxmforbusiness</a></code></p>
	            </div>

	            <p>
	              The portal provides a set of features to control 3 main areas of functionality.
	              <ul>
	                <li type="none"><i class="icon-sitemap"></i>&nbsp;Groups</li>
	                <li type="none"><i class="icon-key"></i>&nbsp;Credentials</li>
	                <li type="none"><i class="icon-calendar"></i>&nbsp;Schedules</li>
	              </ul>
	            </p>

	            <br>
	            <h3 id="overiew-groups">Overview: Groups</h3>
	            <p>Groups provide a way to organize one or more radios into a logical group so that they can be acted upon as a set.  Groups are tyically created to organize radios by business type, business location or business hours of operation.  Primarily groups are used for assigning a set of radios the same schedule.  In terms of relationships, a radio can be associated to one or more groups.  Additionally a group can be a associated to one or more schedules.</p>
	         
	            <div class="bs-example">
	                  <center><img src="assets/img/help/group-relationships.png" width="75%" /></center>
	            </div>

	            <h3 id="overview-credentials">Overview: Credentials</h3>
	            <p>Credentials refer to the set of credentials provided by SiriusXM.  Within the context of the Business Radio Management Portal we associate a set of SiriusXM credentials to a specific Grace Digital GDI-SXMBR1 radio.
	            <span>
	              <center><img src="assets/img/help/credentials-irbm20.png" /></center>
	            </span>
	            </p>

	            <br>
	            <h3 id="overview-schedules">Overview: Schedules</h3>
	            <p>Schedules provide a means to control the radios activity throughout the week.  This is done by creating events that instruct the radio what to play, when to play it and for how long.  A schedule can be associated to zero or more groups and likewise a group can be associated to multiple schedules.
		            <span>
		              <center><img src="assets/img/help/schedule-relationships.png" /></center>
		            </span>	            
				</p>

	            <br>
	            <h3 id="overview-start">Where to start?</h3>
	            <div class="bs-callout bs-callout-info">
	              <h4>To get up and running with the Portal you can follow a simple set of steps...</h4>  
	              <ol>
	                <li>First <a href="#group-add">create a group</a> to assocate radios to</li>
	                <li>Next <a href="credential/provision">provision</a> a set of credientials</li>
	                <li>Finally <a href="schedule/new">create a schedule</a> and assign it to a group</li>
	              </ol>
	            </div>
	        </section>

	        <!-- ========= G R O U P S ========= -->
			<section id="group">
				<div class="page-header">
					<h1>Groups <small><i class="icon-home"></i>&nbsp;home/groups</small></h1>
				</div>
				<p>Groups provide a way to organize one or more radios.  Put another way they are simply containers to put radios into so that can then be scheduled together.</p>
	            
				<h4 id="group-add">Create a group</h4>
	            <p>
	              To create a group select Groups/Add from the navigation menu on the left side of the screen and complete the required field.
	            </p>
	            <p>
	            	<img src="assets/img/help/group-add.png" style="border:1px solid #efefef;" />
	            </p>
				<table class="table table-bordered table-striped">
					<thead>
					   <tr>
					     <th style="width: 100px;">Field</th>
					     <th style="width: 50px;">Editable</th>
					     <th>Field Description</th>
					   </tr>
					</thead>
					<tbody>
					   <tr>
					     <td>Name</td>
					     <td>Yes</td>
					     <td>A friendly name for the group that easily identifies it.</td>
					   </tr>                   
					   <tr>
					     <td>Description</td>
					     <td>No</td>
					     <td>Additional information for identifying the group and/or the radios that will be associated with it.</td>
					   </tr>      
					</tbody>  
				</table>

				<br>
				<h4 id="group-add">Add a group</h4>
	            <p>
	              1. Select Groups/Manage from the navigation menu on the left side of the screen.<br>  
	              2. Click on the icon in the edit column to activate the editable fields.<br>
	              3. Type the confirmation word in the dialog box and select OK to confirm the action.
	            </p>
	            <p>
	            	<img src="assets/img/help/group-edit.png" style="border:1px solid #efefef;" />
	            </p>            
				<table class="table table-bordered table-striped"  style="margin-top:20px;margin-bottom:20px !important;">
					<thead>
					   <tr>
					     <th style="width: 100px;">Field</th>
					     <th style="width: 50px;">Editable</th>
					     <th>Field Description</th>
					   </tr>
					</thead>
					<tbody>
					   <tr>
					     <td>ID</td>
					     <td>No</td>
					     <td>System identifier for the group</td>
					   </tr> 						
					   <tr>
					     <td>Name</td>
					     <td>Yes</td>
					     <td>A friendly name for the group that easily identifies it.</td>
					   </tr>                   
					   <tr>
					     <td>Description</td>
					     <td>Yes</td>
					     <td>Additional information for identifying the group and/or the radios that will be associated with it.</td>
					   </tr>      
					</tbody>  
				</table>
	            <p>
	            	<img src="assets/img/help/group-edit-save.png" style="border:1px solid #efefef;" />
	            </p>	

				<br>
				<h4 id="group-add">Delete a group</h4>
	            <p>
	              1. Select Groups/Manage from the navigation menu on the left side of the screen.<br>
	              2. Click on the 'x' icon in the delete column from the row you want to delete.
	            </p>
	            <p>
	            	<img src="assets/img/help/group-delete.png" style="border:1px solid #efefef;" />
	            </p>
				<div class="bs-docs-example tooltip-demo">
					<p>	            	
	            		<center><img src="assets/img/help/group-delete-confirm.png" style="	border:1px solid #efefef;"/></center>
	            	</p>
				</div>
			</section>

	        <!-- ========= C R E D E N T I A L S ========= -->
			<section id="credential">
				<div class="page-header">
					<h1>Credentials <small><i class="icon-home"></i>&nbsp;home/credentials</small></h1>
				</div>
				<p>Credentials provide a way to organize one or more radios.  Put another way they are simply containers to put radios into so that can then be scheduled together.</p>
	            
				<h4 id="group-add">Provision credentials</h4>
	            <p>
	            	Its important to note that credentials are not created in the Business Radio Management Portal they are simply associated to a GDI-SXMBR1 radio.<br>   
	              	1. Select Credentials/Provision from the navigation menu on the left side of the screen and complete the required fields.<br>
	              	2. Select Credentials/Provision from the navigation menu on the left side of the screen and complete the required fields.
	            </p>
	            <p>
	            	<img src="assets/img/help/credential-provision.png" style="border:1px solid #efefef;" />
	            </p>
				<table class="table table-bordered table-striped">
					<thead>
					   <tr>
					     <th style="width: 100px;">Field</th>
					     <th style="width: 50px;">Required</th>
					     <th>Field Description</th>
					   </tr>
					</thead>
					<tbody>
					   <tr>
					     <td>Group</td>
					     <td>Yes</td>
					     <td>The radio group that you want to associate the credentials to.</td>
					   </tr>					   
					   <tr>
					     <td>Username</td>
					     <td>Yes</td>
					     <td>The username provided by SiriusXM</td>
					   </tr>                   
					   <tr>
					     <td>Description</td>
					     <td>No</td>
					     <td>Additional information for uniquely identifying the credential.</td>
					   </tr>      
					</tbody>  
				</table>

				<br>
				<h4 id="group-add">Edit a credential</h4>
	            <p>
	              1. Select Credentials/Manage from the navigation menu on the left side of the screen.<br>  
	              2. Click on the icon in the edit column to activate the editable fields.<br>
	              3. Type the confirmation word in the dialog box and select OK to confirm the action.
	            </p>
	            <p>
	            	<img src="assets/img/help/credential-edit.png" style="border:1px solid #efefef;" />
	            </p>            
				<table class="table table-bordered table-striped"  style="margin-top:20px;margin-bottom:20px !important;">
					<thead>
					   <tr>
					     <th style="width: 100px;">Field</th>
					     <th style="width: 50px;">Editable</th>
					     <th>Field Description</th>
					   </tr>
					</thead>
					<tbody>
					   <tr>
					     <td>Username</td>
					     <td>No</td>
					     <td>A SiriusXM Username</td>
					   </tr> 						
					   <tr>
					     <td>Description</td>
					     <td>Yes</td>
					     <td>A friendly name for the credential that easily identifies it.</td>
					   </tr>                   
					   <tr>
					     <td>Group</td>
					     <td>Yes</td>
					     <td>The new group that the credential will be associated to.</td>
					   </tr>  					   
					   <tr>
					     <td>Serial</td>
					     <td>No</td>
					     <td>The serial number of the radio that the credential is associated to.</td>
					   </tr>      
					</tbody>  
				</table>
	            <p>
	            	<img src="assets/img/help/credential-edit-save.png" style="border:1px solid #efefef;" />
	            </p>	

				<br>
				<h4 id="group-add">Delete a credential</h4>
	            <p>
	              1. Select Credentials/Manage from the navigation menu on the left side of the screen.<br>
	              2. Click on the 'x' icon in the delete column from the row you want to delete.
	            </p>
	            <p>
	            	<img src="assets/img/help/credential-delete.png" style="border:0px solid #efefef;" />
	            </p>
				<div class="bs-docs-example tooltip-demo">
					<p>	            	
	            		<center><img src="assets/img/help/credential-delete-confirm.png" style="	border:1px solid #efefef;"/></center>
	            	</p>
				</div>
			</section>

	        <!-- ========= S C H E D U L E S ========= -->
			<section id="schedule">
				<div class="page-header">
					<h1>Schedules <small><i class="icon-home"></i>&nbsp;home/schedules</small></h1>
				</div>
				<p>Schedules provide a way to organize one or more radios.  Put another way they are simply containers to put radios into so that can then be scheduled together.</p>
	            
				<h4 id="group-add">Add a schedule</h4>
	            <p>
	            	Its important to note that schedules are not created in the Business Radio Management Portal they are simply associated to a GDI-SXMBR1 radio.   
	              	1. Select Schedules/Provision from the navigation menu on the left side of the screen and complete the required fields.
	              	2. Select Schedules/Provision from the navigation menu on the left side of the screen and complete the required fields.
	            </p>
	            <p>
	            	<img src="assets/img/help/schedule-add.png" style="border:1px solid #efefef;" />
	            </p>
				<table class="table table-bordered table-striped">
					<thead>
					   <tr>
					     <th style="width: 100px;">Field</th>
					     <th style="width: 50px;">Required</th>
					     <th>Field Description</th>
					   </tr>
					</thead>
					<tbody>
					   <tr>
					     <td>Name</td>
					     <td>Yes</td>
					     <td>A friendly name to be able to identify the schedule.</td>
					   </tr>					   
					   <tr>
					     <td>Group</td>
					     <td>No</td>
					     <td>The group you want to associate this schedule to.</td>
					   </tr>                   
					   <tr>
					     <td>DayPart</td>
					     <td>Yes</td>
					     <td>
					     	<table
					     		<thead><th style="border-left:0px;">Field</th><th>Description</th></thead></thead>
					     		<tbody>
						     		<tr>
						     			<td style="border-left:0px;">Time</td><td>The time this event should start (AM/PM)</td>
						     		</tr>					     		
						     		<tr>
						     			<td style="border-left:0px;">Channel</td><td>The music channel this event play</td>
						     		</tr>					     		
						     		<tr>
						     			<td style="border-left:0px;">Volume</td><td>The volume this event play at (0% - 100%).  The default is 25%</td>
						     		</tr>
					     		</tbody>
					     	</table>
					     </td>
					   </tr>      
					</tbody>  
				</table>

				<br>
				<h4 id="group-add">Edit a schedule</h4>
	            <p>
	              1. Select Schedules/Edit from the navigation menu on the left side of the screen.<br>  
	              2. Select an existing schedule from the drop-down list
	            </p>
	            <p>
	            	<img src="assets/img/help/schedule-edit-choose.png" style="border:1px solid #efefef;" />
	            </p>            
	            <p>
	            	3. At this point you can add, edit or remove events for each day of the week.
	            </p>
	            <p>
	            	<img src="assets/img/help/schedule-edit.png" style="border:1px solid #efefef;" />
	            </p>
	            <br>
	            <p>  
					<caption class="pull-left">Edit Schedle Event Tasks</caption>
					<table class="table table-bordered table-striped">
						<thead>
						   <tr>
						     <th style="width:100px;">Task</th>
						     <th>Action</th>
						   </tr>
						</thead>
						<tbody>
						   <tr>
						     <td style="vertical-align:middle;">Edit Time</td>
						     <td style="vertical-align:middle;">Click on clock icon <img src="assets/img/help/event-time-clock.png"> in the time field to display the time select controls</td>
						   </tr>					   
						   <tr>
						     <td>Edit Channel</td>
						     <td>Expand the channel drop-down list and select a channel from the list</td>
						   </tr>                   
						   <tr>
						     <td style="vertical-align:middle;">Edit Volume</td>
						     <td style="vertical-align:middle;">Click on volume control arrows <img src="assets/img/help/event-volume-control.png"> in the volume field to adjust the volume level up or down</td>
						   </tr>      
						</tbody>  
					</table>	            	
	            </p>

				<br>
				<h4 id="group-add">Delete a schedule</h4>
	            <p>
	              	1. Select Schedules/Manage from the navigation menu on the left side of the screen.<br>
	              	2. Click on the 'x' icon in the delete column from the row you want to delete.
	            </p>
	            <p>
	            	<img src="assets/img/help/schedule-delete.png" style="border:0px solid #efefef;" />
	            </p>
	            <p>
	            	3. Type the confirmation word in the dialog box and select OK to confirm the action.
	            </p>
				<div class="bs-docs-example tooltip-demo">
					<p>	            	
	            		<center><img src="assets/img/help/schedule-delete-confirm.png" style="border:1px solid #efefef;"/></center>
	            	</p>
				</div>
			</section>


			 <!-- ========= LAYOUT ========= -->
			<section id="layout">
				<div class="page-header">
					<h1>Portal Layout <small> <i class="icon-desktop"></i></small></h1>
				</div>

				<h2>Panels</h2>
				<p>The Portal is basically split into 2 main panels.  The panel on the left shows the navigation links.  The panel on the right shows the content associated with the select link.  By default the portal will show the Dashboard.  The Dashboard shows a some key metrics and navigation tiles that allow you to quickly jump to different pages in the Portal.
				</p>
				<div class="mini-layout fluid">
				<div class="mini-layout-sidebar"><center style="margin-top:60%;"><div style="font-size:150%;color:#7C7C7C;">NAVIGATION</div></center></div>
				<div class="mini-layout-body"><center style="margin-top:15%;"><div style="font-size:150%;color:#858585;">CONTENT</div></center></div>
				</div>
			</section>

		</div>
	</div>
</div>


<script src="views/help/overview_files/holder.js"></script>
<script src="views/help/overview_files/prettify.js"></script>
<script src="views/help/overview_files/application.js"></script>
<script>
	$(document).ready(function() {

		$('.nav li').on('mousedown', function() {
			$('.nav').find('li.active').removeClass('active');
			$(this).addClass('active');
		});

		var route = document.location.href.split('=')[1];
		var entity = route.split('/')[0];
		$('a[href=#'+entity+']').trigger('mousedown');

</script>