<?php ob_start("ob_gzhandler"); ?>
<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div style="width:87032qpx;">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<?php echo TITLE_DASHBOARD; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li><a href="javascript:;"><?php echo LABEL_DASHBOARD; ?></a></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	<div id="dashboard">
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row-fluid">
			<div style="width:285px;display:inline-block;float:left;">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="icon-tasks"></i>
					</div>
					<div class="details">
						<div class="number">
							<?php echo intval($result['activeRadioCount']) + intval($result['inactiveRadioCount']); ?>
						</div>
						<div class="desc">									
							<?php echo LABEL_DB_RADIO_TOTAL; ?>
						</div>
					</div>
					<a class="more view-radios" href="javascript:;" data-route="radio/manage">
					<?php echo LABEL_VIEW_MORE; ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
			<div style="width:285px;display:inline-block;margin-left:10px;">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="icon-check"></i>
					</div>
					<div class="details">
						<div class="number"><?php echo empty($result['activeRadioCount']) ? '0' : $result['activeRadioCount']; ?></div>
						<div class="desc"><?php echo LABEL_DB_RADIO_ACTIVE; ?></div>
					</div>
					<a class="more" href="javascript:;" data-route="radio/manage/active">
					<?php echo LABEL_VIEW_MORE; ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
			<div style="width:285px;display:inline-block;margin-left:5px;">
				<div class="dashboard-stat red">
					<div class="visual">
						<i class="icon-warning-sign"></i>
					</div>
					<div class="details">
						<div class="number"><?php echo empty($result['inactiveRadioCount']) ? '0' : $result['inactiveRadioCount']; ?></div>
						<div class="desc"><?php echo LABEL_DB_RADIO_INACTIVE; ?></div>
					</div>
					<a class="more" href="javascript:;" data-route="radio/manage/inactive">
					<?php echo LABEL_VIEW_MORE; ?> <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
		</div>
		<!-- END DASHBOARD STATS -->
		
		<div class="row-fluid">
			<!-- RADIOS -->
			<div class="span6">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-tasks"></i><?php echo LABEL_RADIOS; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-blue tile-button" href="radio/new">
								<div class="tile-body">
									<i class="icon-magic"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_PROVISION; ?></div>
								</div>
			               	</div>
							<div class="tile bg-green tile-button" href="radio/move">
								<div class="tile-body">
									<i class="icon-exchange"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MOVE; ?></div>
								</div>
			               	</div>  
							<div class="tile bg-grey tile-button" href="radio/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>
			               	<div class="tile bg-yellow tile-button" href="radio/help">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>	 			               	 				               	 			               	             			
						</div>
					</div>
				</div>
			</div>		

			<!-- GROUPS -->
			<div class="">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-sitemap"></i><?php echo LABEL_GROUPS; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-green tile-button" href="group/new">
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_ADD; ?></div>
								</div>
			               	</div>
							<div class="tile bg-blue tile-button" href="group/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div> 
			               	<div class="tile bg-purple tile-button" href="help/overview#groups">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>	 
						</div>
					</div>
				</div>
			</div>
		</div>
		<br>
		<div class="row-fluid">
			<!-- Schedule -->
			<div class="span6">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-calendar"></i><?php echo LABEL_SCHEDULES; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-grey tile-button" href="schedule/new">
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_ADD; ?></div>
								</div>
			               	</div>
							<div class="tile bg-purple tile-button" href="schedule/edit">
								<div class="tile-body">
									<i class="icon-edit"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_EDIT; ?></div>
								</div>
			               	</div>								
			               	<div class="tile bg-green tile-button" href="schedule/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>
			               	<div class="tile bg-yellow tile-button" href="schedule/help">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>					               					               		               	 			               	             			
						</div>
					</div>
				</div>
			</div>			
			<!-- Lineups -->
			<div class="">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-random"></i><?php echo LABEL_LINEUPS; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-blue tile-button" href="folder/new">
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_ADD; ?></div>
								</div>
			               	</div>  			               	
							<div class="tile bg-red tile-button" href="folder/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>
			               	<div class="tile bg-grey tile-button" href="folder/help">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>				               		               	 			               	             			
						</div>
					</div>					
				</div>
			</div>		
		</div>
	</div>
</div>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/index.min.js"></script>	
<!-- END PAGE LEVEL SCRIPTS -->

<script>
	$(document).ready(function() {	
		$('.more').on('click', function (e) {
			var route = $(this).attr('data-route');
			loadContent(route);
			return false;
		});		

		// Handle Tile Click
		$('.tile-button').on('click', function (e) {
			var route = $(this).attr('href');
			loadContent(route);
			return false;
		});
	});
</script>