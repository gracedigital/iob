<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div>
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title"><?php echo TITLE_DASHBOARD; ?></h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<span class="icon-angle-right"></span>
				</li>
				<li><a href="javascript:;"><?php echo LABEL_DASHBOARD; ?></a></li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->
	<div id="dashboard">
		<!-- BEGIN DASHBOARD STATS -->
		<div class="row-fluid">
			<div class="span3">
				<div class="dashboard-stat blue">
					<div class="visual">
						<i class="icon-key"></i>
					</div>
					<div class="details">
						<div class="number">
							<?php echo $result['total']; ?>
						</div>
						<div class="desc"><?php echo CAPTION_CREDENTIALS_TOTAL; ?></div>
					</div>
					<a class="more view-radios" href="javascript:;" data-route="credential/manage">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
			<div class="span3">
				<div class="dashboard-stat green">
					<div class="visual">
						<i class="icon-ok"></i>
					</div>
					<div class="details">
						<div class="number"><?php echo $result['active']; ?></div>
						<div class="desc"><?php echo CAPTION_CREDENTIALS_ACTIVE; ?></div>
					</div>
					<a class="more" href="javascript:;" data-route="credential/manage/000">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
			<div class="span3">
				<div class="dashboard-stat red">
					<div class="visual">
						<i class="icon-warning-sign"></i>
					</div>
					<div class="details" style="white-space:nowrap;overflow:hidden;">
						<div class="number"><?php echo $result['inactive']; ?></div>
						<div class="desc"><?php echo CAPTION_CREDENTIALS_INACTIVE; ?></div>
					</div>
					<a class="more" href="javascript:;" data-route="credential/manage/no">
					View more <i class="m-icon-swapright m-icon-white"></i>
					</a>						
				</div>
			</div>
		</div>
		<!-- END DASHBOARD STATS -->
		
		<!-- GROUPS -->
		<div class="row-fluid">
			<div class="span9">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-sitemap"></i><?php echo LABEL_GROUPS; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-green tile-button" href="group/new">
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_ADD; ?></div>
								</div>
			               	</div>
							<div class="tile bg-yellow tile-button" href="group/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>							
			               	<div class="tile bg-grey tile-button" href="help/overview">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- CREDENTIALS -->
		<div class="row-fluid">
			<div class="span9">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-sitemap"></i><?php echo LABEL_CREDENTIALS; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-purple tile-button" href="credential/provision">
								<div class="tile-body">
									<i class="icon-magic"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_PROVISION; ?></div>
								</div>
			               	</div>
							<div class="tile bg-blue tile-button" href="credential/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>  
			               	<div class="tile bg-green tile-button" href="help/overview">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>			               	
						</div>
					</div>
				</div>
			</div>
		</div>

		<!-- SCHEDULES -->
		<div class="row-fluid">
			<div class="span9">
				<div class="portlet">
					<div class="portlet-title">
						<div class="caption"><i class="icon-calendar"></i><?php echo LABEL_SCHEDULES; ?></div>
					</div>
					<div class="portlet-body">			
						<div class="tiles">
							<div class="tile bg-grey tile-button" href="schedule/new">
								<div class="tile-body">
									<i class="icon-plus"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_ADD; ?></div>
								</div>
			               	</div>
							<div class="tile bg-yellow tile-button" href="schedule/edit">
								<div class="tile-body">
									<i class="icon-edit"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_EDIT; ?></div>
								</div>
			               	</div>		
							<div class="tile bg-blue tile-button" href="schedule/manage">
								<div class="tile-body">
									<i class="icon-cogs"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_MANAGE; ?></div>
								</div>
			               	</div>
			               	<div class="tile bg-purple tile-button" href="help/overview">
								<div class="tile-body">
									<i class="icon-question"></i>
								</div>
								<div class="tile-object">
									<div class="name"><?php echo LABEL_HELP; ?></div>
								</div>
			               	</div>			               					               			               		               	 			               	             			
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</div>
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/scripts/index.min.js"></script>	
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	$(document).ready(function() {	
		$('.more').on('click', function (e) {
			var route = $(this).attr('data-route');
			loadContent(route);
			return false;
		});		

		// Handle Tile Click
		$('.tile-button').on('click', function (e) {
			var route = $(this).attr('href');
			loadContent(route);
			return false;
		});
	});
</script>