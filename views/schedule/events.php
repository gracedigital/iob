<?php foreach ($days as $dow=>$day): ?>
<div class="tab-pane" id="tab_1_<?php echo $dow; ?>">
	<form class="form-horizontal">
		<table id="day-parts" class="table table-hover table-events" style="margin-bottom:0px !important;vertical-align:top !important;">
			<thead>
				<tr>
					<th>Day&nbsp;Part</th>
					<th class="span2">Time</th>
					<th class="span6">Channel</th>
					<th>Volume</th>
					<th><a class="btn mini green" style="margin-top:4%;width:66px;" onclick="$(this).addClone();"><i class="icon-plus"></i> Add Part</a></th>
				</tr>
			</thead>		
			<tbody>		
			<?php $count = count($day); ?>				
				<?php foreach ($day as $key => $event): ?>
					<?php if($key == 0): ?>
					<tr id="<?php echo $event['event_id'];?>">
						<td>
							<div class="input-icon left">
				            	<i class="icon-unlock"></i>
				            	<input class="m-wrap m-ctrl-small daypart" type="text" placeholder="Open" disabled="" />    
					        </div>
						</td>
						<td>
							<div class="control-group">                           
					            <div class="input-append bootstrap-timepicker">
					            	<input type="text" class="timepicker-default m-wrap small" name="event_time" value="<?php echo $event['event_time']; ?>">
					            	<span class="add-on"><i class="icon-time"></i></span>
					            </div>
					        </div>								
					    </td>
						<td>		
						<?php $channel = explode('|', $event['event_cmd2']); ?>
				       		<select class="select2 channel" style="width:405px !important; overflow:hidden;" data-channel="<?php echo $channel[0] == 'SXM' ? $channel[1] : $channel[1].'|'.$channel[2]; ?>" name="stn">
					        	<?php echo $channels; ?>
							</select>					
						</td>
						<td>
							<?php if(!empty($event['event_cmd1'])): ?>
								<?php $parts = explode('|', $event['event_cmd1']); $volume = $parts[2]; ?>
								<!-- <input type="number" step="1" min="0" max="20" value="<?php // echo ($volume/5); ?>" class="m-wrap small" style="width: 30px !important;" name="vol" value="" />
								<span class="volume-percent" style="width: 35px;"><?php // echo $volume . '%'; ?></span> -->
								<input type="text" style="width: 40px !important; display:inline-block;" name="vol" value="<?php echo $volume; ?>">
							<?php endif; ?>
						</td>
						<td></td>			
					</tr>
					<?php elseif($key < $count - 1): ?>
					<tr id="<?php echo $event['event_id'];?>">
						<td >
							<div class="input-icon left">
				            	<i class="icon-table"></i>
				            	<input class="m-wrap m-ctrl-small daypart" type="text" placeholder="<?php echo 'Day Part '.($key) ?>" disabled="" />      
					        </div>
						</td>
						<td>
							<div class="control-group">                           
					            <div class="input-append bootstrap-timepicker">
					            	<input type="text" class="timepicker-default m-wrap small" name="event_time" value="<?php echo $event['event_time'] ?>">
					            	<span class="add-on"><i class="icon-time"></i></span>
					            </div>
					        </div>								
					    </td>
						<td>
							<?php $channel = explode('|', $event['event_cmd2']); ?>
					       	<select class="select2 channel" style="width:405px !important; overflow:hidden;" data-channel="<?php echo $channel[0] == 'SXM' ? $channel[1] : $channel[1].'|'.$channel[2]; ?>" name="stn">
					        	<?php echo $channels; ?>
							</select>					
						</td>
						<td>
							<?php if(!empty($event['event_cmd1'])): ?>
								<?php $parts = explode('|', $event['event_cmd1']); $volume = $parts[2]; ?>
								<!-- <input type="number" step="1" min="0" max="20" value="<?php // echo ($volume/5); ?>" class="m-wrap small" style="width: 30px !important;" name="vol" value=""/>
								<span class="volume-percent"><?php // echo $volume . '%'; ?></span> -->
								<input type="text" style="width: 40px !important; display:inline-block;" name="vol" value="<?php echo $volume; ?>">
							<?php endif; ?>
						</td>
						<td><a class="btn mini red" style="margin-top:7%;" onclick="$(this).killClone()"><i class="icon-trash"></i> Delete Part</a></td>			
					</tr>
					<?php else: ?>
					<tr id="<?php echo $event['event_id'];?>" style="border-bottom:0px;">
						<td>
							<div class="input-icon left">
					            <i class="icon-lock"></i>
					            <input class="m-wrap m-ctrl-small daypart" type="text" placeholder="Close" disabled="" value="Close"/>    
					        </div>
						</td>
						<td>
							<div class="control-group">                           
					            <div class="input-append bootstrap-timepicker">
					            	<input type="text" class="timepicker-default m-wrap small" name="event_time" value="<?php echo $event['event_time'];?>">
					            	<span class="add-on"><i class="icon-time"></i></span>
					            </div>
					        </div>								
					    </td>
						<td><span class="schedule-event-off">*Stops the music until the next Opening</span></td>
					    <td></td>
					    <td></td>
					</tr>
					<?php endif; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
		<div class="hidden">
			<input type="hidden" name="interval_type" value="W">
			<input type="hidden" name="day_of_week" value="<?php echo $dow; ?>">
			<input type="hidden" name="obsolete" value="false">
		</div>	
	</form>	
</div>
<?php endforeach; ?>
<script>
	$(document).ready(function(){

	    // initalize time selector
	    $('.timepicker-default').timepicker({ minuteStep: 5 });

	    // initalize volume selector
    	$("input[name=vol]").pcntspinner({ min: 0, max: 100, step: 1 });    

    	// resize last row for time selection
		var contentHeight = 0;
		$('.bootstrap-timepicker', $('tr:last-of-type')).hover(function () {

		    contentHeight = (contentHeight != 0) ? contentHeight : $(this).closest('tr').height();
		    $(this).closest('tr').animate({
		        height: '185px'
		    }, 300);
		}, function () {
			$('body').mousedown();
		    $(this).closest('tr').animate({
		        height: contentHeight
		    }, 300);
		});

		$('.bootstrap-timepicker', 'table.table-events tbody tr:not(:last)').hover(function () {
			// do nothing
		}, function () {
			$('body').mousedown();
		});	
			
	})
</script>