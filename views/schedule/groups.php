<div class="innerDetails">
	<div class="portlet box light-grey tabbable">
    	<div class="portlet-title">
			<div class="caption">
			  <i class="icon-reorder"></i><?php echo CAPTION_SCHEDULE_GROUPS; ?>
			</div>
			<span class="link-group pull-right" style="cursor:pointer;color:#FFF;font-size:115%;">
				<i class="icon-plus"></i> Add </a>           
			</span>
    	</div>
    	<div class="portlet-body" style="padding:10px;">		
			<table class="table table-condensed table-striped table-hover" id="data_table_inner">
				<thead>
					<tr>
						<th><?php echo TH_ID; ?></th>
						<th><?php echo TH_TITLE; ?></th>
						<th><?php echo TH_UNLINK; ?></th>
					</tr>
				</thead>
				<tbody>		
					<?php foreach($result as $group): ?>
						<tr>
							<td>
								<?php echo $group['radiogroup_id']; ?>
							</td>
							<td>
								<?php echo empty($group['radiogroup_description']) ? '' : urldecode(stripslashes($group['radiogroup_description'])); ?>
							</td>
							<td style="width:50px;">
								<span class="unlink-group" style="cursor:pointer;color:#0D638F;">
									<i class="icon-unlink"></i>
								</span>												
							</td>
						</tr>
					<?php endforeach; ?>	
				</tbody>
			</table>
		</div>
	</div>	
</div>