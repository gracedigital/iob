<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<link href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet">
<style>
	.ui-spinner {
		display: -webkit-flex;
	}

	.ui-spinner-button {
		cursor: pointer;
		margin-top: 5px;
		margin-left: 5px;
	}

	a:link.ui-spinner-button {text-decoration:none;}
	a:visited.ui-spinner-button {text-decoration:none;}
	a:hover.ui-spinner-button {text-decoration:none;}
	a:active.ui-spinner-button {text-decoration:none;}
</style>
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-calendar"></i>&nbsp;<?php echo TITLE_SCHEDULE_EDIT; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php">Home</a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#">Schedule</a>
				</li>
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN INLINE TABS PORTLET-->
			<div class="portlet box red">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i>&nbsp;<?php echo CAPTION_SCHEDULE_DETAILS; ?>
					</div>
				</div>
				<div class="portlet-body form">
					<div class="row-fluid">
						<div class="span9">
							<div class="control-group">
								<div class="controls">
					             	<select id="schedule_list" class="select2 span8" placeholder="<?php echo PH_SELECT_SCHEDULE; ?>">
					             		<option value=""></option><?php echo urldecode($result); ?>
					             	</select>		
									<span class="input-error tooltips" data-original-title="Please enter a name for the schedule"></span>
									<span class="icon-exclamation-sign" style="display:none;"></span>			             
							    </div>
							</div>
						</div>
						<!-- <div class="span3" style="font-size:110%; margin:5px; color:#888;">
							<span id="radiogroup">Group Name:&nbsp;</span><span id="description" style="font-weight:600;"></span>
						</div> -->						
					</div>				
					<div class="tabbable tabbable-custom tabs-left" style="margin-top:15px; position:relative;">
						<ul class="nav nav-tabs tabs-left">
							<li class="active"><a href="#tab_1_0" data-toggle="tab"><?php echo LABEL_SUNDAY; ?></a></li>
							<li><a id="tab_1" href="#tab_1_1" data-toggle="tab"><?php echo LABEL_MONDAY; ?></a></li>
							<li><a id="tab_2" href="#tab_1_2" data-toggle="tab"><?php echo LABEL_TUESDAY; ?></a></li>
							<li><a id="tab_3" href="#tab_1_3" data-toggle="tab"><?php echo LABEL_WEDNESDAY; ?></a></li>
							<li><a id="tab_4" href="#tab_1_4" data-toggle="tab"><?php echo LABEL_THURSDAY; ?></a></li>
							<li><a id="tab_5" href="#tab_1_5" data-toggle="tab"><?php echo LABEL_FRIDAY; ?></a></li>
							<li><a id="tab_6" href="#tab_1_6" data-toggle="tab"><?php echo LABEL_SATURDAY; ?></a></li>
						</ul>					
						<div class="tab-content" style="min-height:300px !important;">
							<div ass="schedule-watermark">
								<div class="schedule-watermark-in">
									<h2 class="schedule-watermark-label"><?php echo LABEL_EDIT_SCHEDULE; ?></h2>
									<div class="schedule-watermark-icon"><i class="icon-calendar icon-4x"></i></div>
								</div>
							</div>
							<!-- TABS INSERTED HERE -->
						</div>
					</div>
					<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
						<button class="close" data-dismiss="alert" style="top:5px;"></button>
						<span id="status-msg" class="pull-right status"></span>
						<span class="status"><i class="icon-info-sign">&nbsp;</i><?php echo LABEL_STATUS; ?></span>
					</div>											
					<div class="form-actions">
						<div class="pull-right">
							<button id="btn-edit-cancel" type="button" class="btn"><?php echo LABEL_BTN_CANCEL; ?></button>
							<button id="btn-edit-submit" type="button" class="btn green"><?php echo LABEL_BTN_SUBMIT; ?>&nbsp;
								<i id="submit-icon" class="m-icon-swapright m-icon-white"></i>
								<i id="loading-icon" class="icon-spinner icon-mirrored icon-spin" style="display:none;"></i>
							</button>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<script src="assets/plugins/date.min.js"></script>
<script src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/scripts/pages/schedule-edit.min.js"></script>
<script src="assets/scripts/pages/schedule-events.min.js"></script>