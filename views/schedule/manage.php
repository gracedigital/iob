<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<div class="container-fluid">
   	<!-- BEGIN PAGE HEADER-->           
   	<div class="row-fluid">
      	<div class="span12">
         	<!-- BEGIN PAGE TITLE & BREADCRUMB-->     
	        <h3 class="page-title">
	            <i class="icon-calendar"></i>&nbsp;<?php echo TITLE_SCHEDULE_MANAGE; ?>
	        </h3>
			<ul class="breadcrumb">
		        <li>
		           <i class="icon-home"></i>
		           <a href="index.php"><?php echo LABEL_HOME; ?></a> 
		           <span class="icon-angle-right"></span>
		        </li>
		        <li>
		           <a href="#"><?php echo LABEL_SCHEDULE; ?></a>
		           <span class="icon-angle-right"></span>
		        </li>
		        <li>
		           <a href="#"><?php echo LABEL_MANAGE; ?></a>
		        </li>            
		    </ul>
		    <!-- END PAGE TITLE & BREADCRUMB-->	
	  	</div>
	</div>	
	<!-- END PAGE HEADER-->	    

	<?php include INC_PAGE_ALERTS; ?>

	<!-- BEGIN PAGE CONTENT --> 
	<div class="row-fluid">
		<div class="span12">
			<div class="portlet box green">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i><?php echo CAPTION_SCHEDULE_MANAGE; ?>
					</div>
				</div>						
				<div class="portlet-body">
					<div class="clearfix">
						<div class="row-fluid">
							<div class="span12">
								<table class="table table-striped table-hover condensed" id="data_table">
									<thead>
										<tr>
											<th></th>
											<th><?php echo TH_DETAILS; ?></th>
											<th hidden><i class="icon-key"></i>&nbsp;<?php echo TH_ID; ?></th>
											<th><?php echo TH_NAME; ?></th>
											<th><?php echo TH_MODIFIED; ?></th>
											<th><?php echo TH_EDIT; ?></th>
											<th id="th_schedule_delete"><?php echo TH_DELETE; ?></th>
											<th></th>								
										</tr>
									</thead>
									<tbody>
										<?php $row = 1; ?>
										<?php foreach($result as $schedule): ?>	
											<tr>
												<td style="border-right:solid 1px #E5E5E5;text-align:center;">
													<?php echo $row++ ?>
												</td>											
												<td>
													<i class="icon-plus" title="View Details"></i>
													<a class="simple expand" href="javascript:;">&nbsp;<?php echo LABEL_EXPAND; ?></a>
												</td>
												<td hidden>
													<?php echo $schedule['schedule_id']; ?>
												</td>
												<td>
													<?php echo urldecode($schedule['description']); ?>
												</td>
												<td>
													<?php echo empty($schedule['note']) ? '' : urldecode($schedule['note']); ?>
												</td>
												<td style="width:50px;">
													<span class="edit-schedule" style="cursor:pointer;color:#0D638F;">
														<i class="icon-bolt" style="width:100%;"></i>
													</span>
												</td>
												<td style="width:50px;">
													<span class="delete-schedule" style="cursor:pointer;color:#0D638F;">
														<i class="icon-remove" style="width:100%;"></i>
													</span>
												</td>
												<td></td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table>		      		
							</div>															
						</div>	
					</div>				
				</div>
				<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
					<button class="close" data-dismiss="alert" style="top:5px;"></button>
					<span id="status-msg" class="pull-right status"></span>
					<span class="status"><i class="icon-info-sign">&nbsp;</i><?php echo LABEL_STATUS; ?></span>
				</div>					
			</div>
		</div>	        
	</div>
	<!-- END PAGE CONTENT -->
</div>

<script src="assets/scripts/pages/schedule-manage-outer-table.min.js"></script>	
<script src="assets/scripts/pages/schedule-manage.min.js"></script>