<?php include INCLUDES.'/lock.php'; ?>
<?php include INCLUDES.'/errorcheck.php'; ?>
<style>
.ui-spinner {
	display: -webkit-flex;
}

.ui-spinner-button {
	cursor: pointer;
	margin-top: 5px;
	margin-left: 5px;
}

a:link.ui-spinner-button {text-decoration:none;}
a:visited.ui-spinner-button {text-decoration:none;}
a:hover.ui-spinner-button {text-decoration:none;}
a:active.ui-spinner-button {text-decoration:none;}

</style>
<link href="assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet">
<div class="container-fluid">
	<!-- BEGIN PAGE HEADER-->
	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->		
			<h3 class="page-title">
				<i class="icon-calendar"></i>&nbsp;<?php echo TITLE_SCHEDULE_ADD; ?>
			</h3>
			<ul class="breadcrumb">
				<li>
					<i class="icon-home"></i>
					<a href="index.php"><?php echo LABEL_HOME; ?></a> 
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#"><?php echo LABEL_SCHEDULE; ?></a>
					<i class="icon-angle-right"></i>
				</li>
				<li>
					<a href="#"><?php echo LABEL_ADD; ?></a>
				</li>				
			</ul>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>
	<!-- END PAGE HEADER-->

	<?php include INC_PAGE_ALERTS; ?>

	<div class="row-fluid">
		<div class="span12">
			<!-- BEGIN INLINE TABS PORTLET-->
			<div class="portlet box purple">
				<div class="portlet-title">
					<div class="caption">
						<i class="icon-reorder"></i>&nbsp;<?php echo CAPTION_SCHEDULE_DETAILS; ?>
					</div>
				</div>
				<div class="portlet-body form">
	
					<div class="row-fluid">
						<div class="span12">
							<div class="control-group pull-left span6" style="margin-bottom:0px; margin-top:10px;">
								<div class="controls input-icon left">
						            <i class="icon-bookmark-empty"></i>
						            <input class="m-wrap span12" type="text" placeholder="<?php echo PH_SCHEDULE_ENTER_NAME; ?>" id="schedule_name" autofocus="autofocus" />   
							    </div>
							</div>
							<div class="control-group pull-right span6" style="margin-bottom:0px; margin-top:10px;">
								<div class="controls">
							       	<select id="groupList" class="select2 span12" name="stn" placeholder="<?php echo PH_SELECT_GROUP; ?>">
							       		<option value=''></option>
							         	<?php echo $groups; ?>
									</select>	 
							    </div>
							</div>
						</div>
					</div>
					<h3 style="border-bottom: 1px solid #eee;"></h3>
					<div class="tabbable tabbable-custom tabs-left" style="margin-top:15px; position:relative;">
						<ul class="nav nav-tabs tabs-left">
							<li class="active"><a id="tab_0" href="#tab_1_0" data-toggle="tab"><?php echo LABEL_SUNDAY; ?></a></li>
							<li><a id="tab_1" href="#tab_1_1" data-toggle="tab"><?php echo LABEL_MONDAY; ?></a></li>
							<li><a id="tab_2" href="#tab_1_2" data-toggle="tab"><?php echo LABEL_TUESDAY; ?></a></li>
							<li><a id="tab_3" href="#tab_1_3" data-toggle="tab"><?php echo LABEL_WEDNESDAY; ?></a></li>
							<li><a id="tab_4" href="#tab_1_4" data-toggle="tab"><?php echo LABEL_THURSDAY; ?></a></li>
							<li><a id="tab_5" href="#tab_1_5" data-toggle="tab"><?php echo LABEL_FRIDAY; ?></a></li>
							<li><a id="tab_6" href="#tab_1_6" data-toggle="tab"><?php echo LABEL_SATURDAY; ?></a></li>
						</ul>	
						<div class="tab-content" style="min-height:300px !important;">
							<?php for($index = 0; $index < 7; $index++): ?>
								<div class="tab-pane" id="tab_1_<?php echo $index; ?>">
									<form class="form-horizontal">
										<table id="day-parts" class="table table-hover" style="margin-bottom:0px !important;">
												<thead>
													<tr>
														<th><?php echo TH_DAYPART; ?></th>
														<th class="span2"><?php echo TH_TIME; ?></th>
														<th class="span6"><?php echo TH_CHANNEL; ?></th>
														<th><?php echo TH_VOLUME; ?></th>
														<th><a class="btn mini green" style="margin-top:4%;width:66px;" onclick="$(this).addClone(<?php echo $index; ?>);"><i class="icon-plus"></i> Add Part</a></th>
													</tr>
												</thead>
												<tbody>
													<!-- DAY PART OPEN -->
													<tr style="vertical-align:top;">
														<td>
															<div class="input-icon left">
							                                    <i class="icon-unlock"></i>
							                                    <input class="m-wrap m-ctrl-small daypart" type="text" placeholder="<?php echo PH_OPEN; ?>" data-minute-step="5" value="Open" name="event_name" />    
							                                </div>
														</td>
														<td>
															<div class="control-group">
																<div class="input-append bootstrap-timepicker">
								                                    <input class="m-wrap small timepicker-default" type="text" name="event_time" value="" />
								                                    <span class="add-on"><i class="icon-time"></i></span>
								                                </div>	                                
							                                </div>								
								                         </td>
														<td>
					                                       	<select class="select2" style="width:405px !important; overflow:hidden;" placeholder="<?php echo PH_SELECT_CHANNEL; ?>" name="stn"> 
					                                          	<?php echo $channels; ?>
															</select>					
														</td>
														<td>
															<!-- <input type="number" step="1" min="0" max="20" placeholder="<?php echo PH_VOLUME; ?>" class="m-wrap small" style="width: 50px !important;" name="vol"/>
															<span class="volume-percent">&nbsp;25%</span> -->
															<input type="text" value="10" style="width: 40px !important; display:inline-block;" name="vol">
														</td>
														<td></td>
													</tr>
													<!-- DAY PART CLOSE -->
													<tr>
														<td>
															<div class="input-icon left">
							                                    <i class="icon-lock"></i>
							                                    <input class="m-wrap m-ctrl-small daypart" type="text" placeholder="<?php echo PH_CLOSE; ?>" disabled="true" value="Close" name="event_name" />    
							                                </div>
														</td>
														<td>
															<div class="control-group">
																<div class="input-append bootstrap-timepicker">
								                                    <input class="m-wrap small timepicker-default" type="text" name="event_time" />
								                                    <span class="add-on"><i class="icon-time"></i></span>
								                                </div>
							                            	</div>
								                        </td>
													    <td><span class="schedule-event-off"><?php echo LABEL_SCHEDULE_TIP; ?></span></td>	
													    <td></td>
													    <td></td>
													</tr>																																																																																					
												</tbody>
										</table>
										<div class="hidden">
											<input type="hidden" name="interval_type" value="W">
											<input type="hidden" name="day_of_week" value="0">
											<input type="hidden" name="obsolete" value="false">
										</div>						
									</form>	
								</div>
							<?php endfor; ?>
							<div>
								<a id="btn-copy-schedule" class="btn">
									<i class="icon-copy icon-white" style="margin-top:2px;">&nbsp;</i>
									<?php echo LABEL_BTN_COPY_SCHEDULE; ?>
								</a>
							</div>	
						</div>
					</div>
					<div id="status-bar" class="alert alert-info hide" style="margin-bottom:5px;margin-top:-5px;background-color: #fff;">
						<button class="close" data-dismiss="alert" style="top:5px;"></button>
						<span id="status-msg" class="pull-right status"></span>
						<span class="status"><i class="icon-info-sign">&nbsp;</i><?php echo LABEL_STATUS; ?></span>
					</div>
					<div class="form-actions">
						<div class="pull-right">
							<button type="button" id="cancel" class="btn"><?php echo LABEL_BTN_CANCEL; ?></button>
							<button type="button" id="submit" class="btn green"><?php echo LABEL_BTN_SUBMIT; ?>&nbsp;
								<i id="submit-icon" class="m-icon-swapright m-icon-white"></i>
								<i id="loading-icon" class="icon-spinner icon-mirrored icon-spin" style="display:none;"></i>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END PAGE CONTENT-->
</div>
<script src="assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script src="assets/scripts/pages/schedule-events.min.js"></script>
<script src="assets/scripts/pages/schedule-new.min.js"></script>