<!-- ADD DIALOG -->
<div id="modal-add" class="modal hide fade" role="dialog" aria-hidden="true">
   <div class="modal-header">
      <h3 style="color:#3a87ad;"><span id="modal-message"></span></h3>
   </div>
   <div class="modal-body">
		<div class="control-group">
			<div class="controls">
				<select id="lst-entity-add" class="select2"></select>
			</div>
		</div>
   </div>
   <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
      <button id="btn-entity-add" data-dismiss="modal" class="btn green" disabled="true">OK</button>
   </div>
</div>

<!-- DELETE DIALOG -->
<div id="modal-remove" class="modal hide fade" role="dialog" aria-hidden="true">
	<div class="modal-body">
		<div class="alert alert-error"> 
			<i class="icon-exclamation-sign "></i>&nbsp;<span style="font-size:115%;font-weight:bold">&nbsp;You are about to modifiy live data</span>
		</div>		
		<p style="font-size:110%;"><span id="modal-message"></span><br>
		Please enter the word "<b>DELETE</b>" to confirm.</p>
		<input type="text" id="input-confirm-remove" autocomplete="on" autofocus="autofocus"></input>
	</div>
	<div class="modal-footer">
	  <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  <button id="btn-entity-remove" class="btn green" disabled="true"><i class="icon-ok m-icon-white"></i>&nbsp;OK</button>
	</div>
</div>

<!-- UNLINK DIALOG -->
<div id="modal-unlink" class="modal hide fade" role="dialog" aria-hidden="true">
	<div class="modal-body">
		<div class="alert alert-error"> 
			<i class="icon-exclamation-sign "></i>&nbsp;<span style="font-size:115%;font-weight:bold">&nbsp;You are about to modifiy live data</span>
		</div>		
		<p style="font-size:110%;"><span id="modal-message"></span><br>
		Please enter the word "<b>UNLINK</b>" to confirm.</p>
		<input type="text" id="input-confirm-unlink" autocomplete="on" autofocus="autofocus"></input>
	</div>
	<div class="modal-footer">
	  <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  <button id="btn-entity-unlink" class="btn green" disabled="true"><i class="icon-ok m-icon-white"></i>&nbsp;OK</button>
	</div>
</div>

<!-- ACTIVATE-DEACTIVATE DIALOG -->
<div id="modal-activate" class="modal hide fade" role="dialog" aria-hidden="true">
	<div class="modal-body">
		<div class="alert alert-error"> 
			<i class="icon-exclamation-sign "></i>&nbsp;<span style="font-size:115%;font-weight:bold">&nbsp;You are about to modify live data</span>
		</div>		
		<p style="font-size:110%;"><span id="activate-message"></span>?<br>
			Please enter the word "<span style="text-transform:uppercase;" id="activate-action"></span>" to confirm.</p>
		<input type="text" id="input-confirm-activate" autofocus="autofocus"></input>
	</div>
	<div class="modal-footer">
	  <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  <button id="btn-entity-activate" class="btn green" disabled="true"><i class="icon-ok m-icon-white"></i>&nbsp;OK</button>
	</div>
</div>

<!-- MOVE DIALOG -->
<div id="modal-move" class="modal hide fade" role="dialog" aria-hidden="true">
	<div class="modal-body">
		<div class="alert alert-error"> 
			<i class="icon-exclamation-sign "></i>&nbsp;<span style="font-size:115%;font-weight:bold">&nbsp;You are about to modify live data</span>
		</div>		
		<p style="font-size:110%;"><span id="move-message"></span><br>
			Please enter the word "<b>MOVE</b>" to confirm.</p>
		<input type="text" id="input-confirm-move" autofocus="autofocus"></input>
	</div>
	<div class="modal-footer">
	  <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  <button id="btn-entity-move" class="btn green" disabled="true"><i class="icon-ok m-icon-white"></i>&nbsp;OK</button>
	</div>
</div>


<!-- UNREGISTER DIALOG -->
<div id="modal-unregister" class="modal hide fade" role="dialog" aria-hidden="true">
	<div class="modal-body">
		<div class="alert alert-error"> 
			<i class="icon-exclamation-sign "></i>&nbsp;<span style="font-size:115%;font-weight:bold">&nbsp;You are about to modify live data</span>
		</div>		
		<p style="font-size:110%;">Please enter the registration code from <span id="entity-description"></span>?<p>
		<input type="text" id="input-confirm-code" autofocus="autofocus" placeholder="Registration Code">
	</div>
	<div class="modal-footer">
	  <button class="btn red" data-dismiss="modal" aria-hidden="true">Cancel</button>
	  <button id="btn-entity-remove" class="btn green" disabled="true"><i class="icon-ok m-icon-white"></i>&nbsp;OK</button>
	</div>
</div>