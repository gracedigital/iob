<!-- BEGIN CORE PLUGINS -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script src="assets/plugins/jquery.blockui.min.js"></script>	
<script src="assets/plugins/jquery.cookie.min.js"></script>
<script src="assets/plugins/uniform/jquery.uniform.min.js" ></script>
<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/plugins/jquery.pulsate.min.js"></script>
<script src="assets/plugins/spin/spin.min.js"></script>
<script src="assets/plugins/spin/jquery.spin.min.js"></script>
<script src="assets/plugins/data-tables/jquery.dataTables.min.js"></script>
<script src="assets/plugins/data-tables/DT_bootstrap.min.js"></script>
<script src="assets/plugins/select2/select2.min.js"></script>
<script src="assets/plugins/jquery-nestable/jquery.nestable.min.js"></script>
<script src="assets/plugins/underscore.min.js"></script>
<script src="assets/plugins/moment.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- BEGIN SITE LEVEL SCRIPTS -->
<script src="assets/scripts/app.min.js"></script>
<script src="assets/scripts/ajax.min.js"></script>
<script src="assets/scripts/webapp.min.js"></script>
<script src="assets/scripts/ui-nestable.min.js"></script>
<script src="assets/scripts/group.min.js"></script>
<script src="assets/scripts/radio.min.js"></script>
<script src="assets/scripts/folder.min.js"></script>
<script src="assets/scripts/channel.min.js"></script>
<script src="assets/scripts/schedule.min.js"></script>
<script src="assets/scripts/dialogs.min.js"></script>
<script src="assets/plugins/date.min.js"></script>
<!-- END SITE LEVEL SCRIPTS -->



