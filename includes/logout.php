<?php
	if(!isset($_SESSION)) session_start();
	// Unset all session values
	$_SESSION = array();
	// Destroy session
	if(session_destroy()) {
		exit("<script>location.href = '/index.php';</script>");
	}
?>