<?php
	// Check for a session user
	if(empty($_SESSION['username'])) {
		header("Location: login.php");
	}	
	// Check the last activity time
	if (isset($_SESSION['last_activity']) && (time() - $_SESSION['last_activity'] > 1800)) {
		// last request was more than 30 minutes ago
		include 'logout.php';
	}

	// Update last activity time stamp
	$_SESSION['last_activity'] = time(); 

	if (!isset($_SESSION['created'])) {
		include 'logout.php'; 
	} else if (time() - $_SESSION['created'] > 1800) {
		// Session started more than 30 minutes ago
		if (!headers_sent()) {
			// Change session ID for the current session an invalidate old session ID
			session_regenerate_id(true);    
		}
		// Update creation time
		$_SESSION['created'] = time();  
	}
?>