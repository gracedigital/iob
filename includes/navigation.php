<!-- BEGIN SIDEBAR -->
<div class="page-sidebar nav-collapse collapse">
	<!-- BEGIN SIDEBAR MENU -->        
	<ul class="page-sidebar-menu">
		<li><div style="height:30px;"</li>
		<!-- DASHBOARD -->
		<li class="start active top">
			<a href="dashboard/view">
				<i class="icon-dashboard"></i> 
				<span class="title"><?php echo LABEL_DASHBOARD; ?></span>
				<span class="selected"></span>
			</a>
		</li>

		<!-- GROUPS -->
		<?php if(in_array(RG_ALL, $_SESSION['privileges'])): ?>
		<li class="has-sub top">
			<a href="javascript:;">
			<i class="icon-sitemap"></i> 
			<span class="title"><?php echo LABEL_GROUPS; ?></span>
			<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="group/new">
						<i class="icon-plus"></i><span>&nbsp;<?php echo LABEL_ADD; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="group/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>
			</ul>
		</li>
		<?php endif; ?>
				
		<!-- CREDENTIALS -->	
		<?php if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) || in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])): ?>	
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-key"></i> 
				<span class="title"><?php echo LABEL_CREDENTIALS; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="credential/provision">
						<i class="icon-magic"></i><span>&nbsp;<?php echo LABEL_PROVISION; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="credential/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="credential/view_move">
						<i class="icon-truck icon-mirrored"></i><span>&nbsp;<?php echo LABEL_MOVE; ?>&nbsp;</span>
					</a>
				</li>												
			</ul>
		</li>
		<?php endif; ?>


		<!-- LINEUPS -->	
		<?php if(!in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) && !in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])): ?>
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-random"></i> 
				<span class="title"><?php echo LABEL_LINEUPS; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">				
				<li>
					<a href="folder/new">
						<i class="icon-plus"></i><span>&nbsp;<?php echo LABEL_ADD; ?>&nbsp;</span>
					</a>
				</li>	
				<li>
					<a href="folder/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>
			</ul>
		</li>
		<?php endif; ?>

		<!-- RADIOS -->
		<?php if(!in_array(SIRIUSUSERS_ALL, $_SESSION['privileges']) && !in_array(SIRIUSUSERS_LIST, $_SESSION['privileges'])): ?>
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-tasks"></i> 
				<span class="title"><?php echo LABEL_RADIOS; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="radio/new">
						<i class="icon-magic"></i><span>&nbsp;<?php echo LABEL_PROVISION; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="radio/move">
						<i class="icon-truck icon-mirrored"></i><span>&nbsp;<?php echo LABEL_MOVE; ?>&nbsp;</span>
					</a>
				</li>				
				<li>
					<a href="radio/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>								
			</ul>
		</li>
		<?php endif; ?>

		<!-- SCHEDULES -->
		<?php if(in_array(SCH_ALL, $_SESSION['privileges'])): ?>
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-calendar"></i> 
				<span class="title"><?php echo LABEL_SCHEDULES; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="schedule/new">
						<i class="icon-plus"></i><span>&nbsp;<?php echo LABEL_ADD; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="schedule/edit">
						<i class="icon-bolt"></i><span>&nbsp;<?php echo LABEL_EDIT; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="schedule/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>				
			</ul>
		</li>
		<?php endif; ?>

		<!-- USERS -->
		<?php if(in_array(USERS_ALL, $_SESSION['privileges'])): ?>	
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-group"></i> 
				<span class="title">&nbsp;<?php echo LABEL_USERS; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="user/manage">
						<i class="icon-cogs"></i><span>&nbsp;<?php echo LABEL_MANAGE; ?>&nbsp;</span>
					</a>
				</li>			
			</ul>
		</li>	
		<?php endif; ?>

		<!-- HELP -->
		<li class="has-sub top">
			<a href="javascript:;">
				<i class="icon-question"></i> 
				<span class="title">&nbsp;<?php echo LABEL_HELP; ?></span>
				<span class="arrow"></span>
			</a>
			<ul class="sub-menu">
				<li>
					<a href="help/overview">
						<i class="icon-flag-alt"></i><span>&nbsp;<?php echo LABEL_START; ?>&nbsp;</span>
					</a>
				</li>				
				<!-- <li>
					<a href="group/help">
						<i class="icon-sitemap"></i><span>&nbsp;<?php echo LABEL_GROUPS; ?>&nbsp;</span>
					</a>
				</li>
				<?php if(in_array(SIRIUSUSERS_ALL, $_SESSION['privileges'])): ?>
				<li>
					<a href="credential/help">
						<i class="icon-key"></i><span>&nbsp;<?php echo LABEL_CREDENTIALS; ?>&nbsp;</span>
					</a>
				</li>
				<?php else: ?>
				<li>
					<a href="radio/help">
						<i class="icon-tasks"></i><span>&nbsp;<?php echo LABEL_RADIO; ?>&nbsp;</span>
					</a>
				</li>
				<li>
					<a href="folder/help">
						<i class="icon-random"></i><span>&nbsp;<?php echo LABEL_LINEUPS; ?>&nbsp;</span>
					</a>
				</li>				
				<?php endif; ?>				
				<li>
					<a href="user/help">
						<i class="icon-group"></i><span>&nbsp;<?php echo LABEL_USERS; ?>&nbsp;</span>
					</a>
				</li> -->										
			</ul>
		</li>	
	</ul>				
</div>

<script src="assets/plugins/jquery.history.js"></script>
<script src="assets/scripts/pages/navigation.min.js"></script>		

</body>
</html>