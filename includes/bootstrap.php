<?php

	/*** include the applicaton configurations ***/
	include $_SERVER['DOCUMENT_ROOT'].'/application/configs/app-config.php';

	/*** include the site configurations ***/
	include CONFIGS . '/site-config.php';

	/*** include the api configurations ***/
	include CONFIGS . '/api-config.php';

	/*** include the controller class ***/
	include APP_ROOT . '/controller.class.php';

	/*** include the registry class ***/
	include APP_ROOT . '/registry.class.php';

	/*** include the router class ***/
	include APP_ROOT . '/router.class.php';

	/*** include the template class ***/
	include APP_ROOT . '/template.class.php';

	/*** auto load model classes ***/
	function __autoload($class_name) {
		$filename = strtolower($class_name) . '.class.php';
		$file = MODELS . $filename;

		if (file_exists($file) == false) {
		    return false;
		}

		include ($file);
	}

	/*** a new registry object ***/
	$registry = new registry;

	/*** create the database registry object ***/
	// $registry->db = db::getInstance();
	
?>
